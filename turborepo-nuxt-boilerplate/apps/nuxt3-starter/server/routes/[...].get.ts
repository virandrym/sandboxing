
export default defineEventHandler(async (event) => {
return serveStatic(event, {
    encodings: undefined, fallthrough: false, getContents(id: string): unknown | Promise<unknown> {
        return undefined;
    }, getMeta(id: string): StaticAssetMeta | Promise<StaticAssetMeta | undefined> | undefined {
        return undefined;
    }, indexNames: []

})
})
