import { StateCustomerUsers } from '~/store/customers/users'

export interface VuexModuleCustomerUsers {
  customers: {
    users: StateCustomerUsers
  }
}

export interface CustomerUser {
  id: string
  name: string
  email: string
}

export interface PayloadCustomerUser extends Omit<CustomerUser, 'id'> {
  password: string
  changePassword?: { newPassword: string; oldPassword: string }
}

export interface ActionCustomerUser extends PayloadCustomerUser {
  passwordConfirmation: string
  passwordOld: string
}
