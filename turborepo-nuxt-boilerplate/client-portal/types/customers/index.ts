import { StateCustomers } from '~/store/customers'

export interface VuexModuleCustomers {
  customers: StateCustomers
}

export interface Customer {
  id: string
  name: string
  code?: string
}

export interface ActionCustomer {
  name: string
  code?: string
  logo?: string
}

export type CustomerOrderCodePrefixed = 'DISABLED' | 'DEFAULT'
