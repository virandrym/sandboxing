import { b2bDocumentCategories } from '~/composables/clients/orders/b2b/form'
import { StateB2BOrders } from '~/store/orders/b2b'

export type B2BShipmentMode = 'land' | 'air' | 'sea'
export type B2BScope = 'EXW' | 'FOB' | 'DDP' | 'DAP' | 'CIP' | 'ALL-IN'
export type B2BPackagingType = 'Cartons' | 'Pallets' | 'Gunny Bags'

// b2b on order details
export interface B2BOrderDetails {
  id: string
  scope: B2BScope
  shipmentMode: B2BShipmentMode
  dangerousGoods: boolean
  cargoReadinessDate: Date | string
  quantity: number
}

export interface B2BOrderParcel {
  id: string
  orderID: string
  packagingType: string
  height: number
  width: number
  length: number
  weight: number
}
// end

export interface B2BBasicInfo {
  shipmentMode: B2BShipmentMode
  scope: B2BScope
  originCountry: string
  destinationCountry: string
  originPortID: string
  destinationPortID: string
  cargoReadinessDate: Date | string
}

export interface B2BSenderDetails {
  senderName: string
  senderPhoneNumber: string
  senderEmail: string
  senderAddress: string
  senderProvince: string
  senderDistrict: string
  senderCity: string
  senderPostal: string
}

export interface B2BConsigneeDetails {
  consigneeAddress: string
  consigneeCity: string
  consigneeDistrict: string
  consigneeEmail: string
  consigneeName: string
  consigneePhoneNumber: string
  consigneePostal: string
  consigneeProvince: string
}

export interface B2BParcelItems {
  length: number
  width: number
  height: number
  weight: number
  packagingType: string
}

export interface B2BPackageDetails {
  quantity: number
  parcels: B2BParcelItems[]
}

export interface B2BOrderItems {
  category: string
  productDescription: string
  quantity: number
}

export interface B2BItemDetails {
  dangerousGoods: boolean
  items: B2BOrderItems[]
}

// b2b API payload model
export interface B2BPostPayload
  extends B2BBasicInfo,
    B2BSenderDetails,
    B2BConsigneeDetails,
    B2BPackageDetails,
    B2BItemDetails {}

export interface B2BPostResponse {
  b2bID: string
}

export interface B2BAttachDocuments {
  [category: typeof b2bDocumentCategories[number]['value']]: string[]
}
export interface B2BQuotationFormData {
  shipmentMode: string
  originPortID: string
  destinationPortID: string
  incoterm: string
  estimatedWeight: number
}
export interface B2BQuotationResponse {
  rateID: string
  rate: number | null
  currency: string
  quotationID: string
  totalEstimatedPrice: number | null
}
export interface B2BOrdersRfq {
  b2b: StateB2BOrders
}
export interface VuexModuleB2bOrders {
  orders: B2BOrdersRfq
}

export interface B2BAttachRfqToOrder {
  b2bOrderID: string
}
