// upload order submission
import type { WorkSheet } from 'xlsx'
import type {
  COD,
  Consignee,
  Dimension,
  Order,
  OrderItem,
  Pickup,
  Sender,
} from '~/types/orders'

export interface OrderItemUpload extends Omit<OrderItem, 'id' | 'orderId'> {}

export interface OrderUpload
  extends Required<
    Pick<Order, 'orderCode' | 'paymentType' | 'insurance'> &
      Consignee &
      Pickup &
      Sender &
      Dimension &
      COD
  > {
  items: OrderItemUpload[]
}

export interface OrderDomestic extends OrderUpload {
  domesticPartnerCode: string
}

export interface OrderCrossBorder extends OrderUpload {
  firstMile: boolean
  lastMile: boolean
  freightForwarder: boolean
  customs: boolean
  isSelfCollectDropOff: boolean
  incoterm: string
  lmPartnerCode: string
}

export interface OrderCrossBorderSingleSheet
  extends Omit<OrderCrossBorder, 'items'>,
    Omit<OrderItemUpload, 'orderCode'> {}

export type OrdersMultipleSheet = OrderCrossBorder | OrderDomestic
export type OrdersSingleSheet = OrderCrossBorderSingleSheet
export type UploadOrdersScopeType = 'cross border' | 'domestic'
export type UploadSheetType = 'multiple' | 'single'

export interface OrderSubmissionWorksheet {
  orders: WorkSheet
  items: WorkSheet
}
