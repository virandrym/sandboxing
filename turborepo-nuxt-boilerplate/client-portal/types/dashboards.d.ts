import { RootStateDashboards } from '~/store/dashboards'

export interface VuexModuleOrders {
  dashboards: RootStateDashboards
}
