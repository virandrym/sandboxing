import { RootStateMarketplaces } from '~/store/marketplaces/marketplaces'

export interface VuexModuleDetailMarketplace {
  marketplaces: RootStateMarketplaces
}

export interface PartnerServiceType {
  id: string
  name: string
}

export interface PartnerSubzoneCapability {
  domestic: boolean
}

export interface PartnerServiceZone {
  cod: boolean
  id: string
  sla_from: number
  sla_to: number
  types: string | null
  zone_country: string
  zone_id: string
  subzoneCapability: PartnerSubzoneCapability
}

export interface PartnerGallery {
  id: string
  name: string
  partnerId: string
  path: string
  size: string
  type: string
}

export interface DetailPartner {
  code: string
  codFee: string
  companyBrief: string
  clientId: string
  contactPerson: string
  currency: string
  description: string
  emailAddress: string
  id: string
  logo: string
  maxCodValue: number
  maximumDimension: string
  maximumWeight: number
  minFee: string
  name: string
  slug: string
  status: 'connected' | 'pending' | 'none'
  sla: string
  partnerSOPPath: string
  phoneNumber: string
  prohibitedItem: string
  partnerServiceTypes: PartnerServiceType[]
  partnerServiceZones: PartnerServiceZone[]
  partnerGallery: PartnerGallery[]
}

export interface Gallery {
  id: string
  path: string
  partnerId: string
  logo: string
  src: string
}
export interface DataGallery {
  description: string
  gallery: Gallery[]
  logo: string
  name: string
  slug: string
}
export interface DetailProfile
  extends Omit<
    DetailPartner,
    'serviceType' | 'serviceZone' | 'partnerGallery'
  > {
  serviceType: PartnerServiceType[]
  serviceZone: PartnerServiceZone[]
  gallery: Gallery[]
  pikupAndDropOff: boolean
}

// partner ratings
export type RatingType =
  | 'Overall'
  | 'ClientSatisfactionRate'
  | 'ConsigneeSatisfaction'
  | 'LastMileFirstAttemptSuccessRate'
  | 'LastMileOnTimeDeliveryRate'
  | 'LastMileNoLossAndDamageGoods'
  | 'FirstMileOnTimePickup'
  | 'FirstMileFirstAttemptSuccess'
  | 'FirstMileNoLossAndDamageGoods'
  | 'CustomsClearanceTimeRate'
  | 'CustomsRejectionRate'

export interface Rating {
  id: string
  partnerId: string
  type: RatingType
  rating: string
}
