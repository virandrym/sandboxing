import { RootStatePartnerComparison } from '~/store/marketplaces/partnerComparison'

export interface VuexModuleCompare {
  marketplaces: {
    partnerComparison: RootStatePartnerComparison
  }
}

export interface RatesParams {
  zone: string
  port: string
  serviceType: string
  partners: string[]
}

export interface RatesLMFM {
  weight: string
  cost: string
  currency: string
}

export interface RatesCustoms {
  costPerWeight: number
  costPerParcel: number
  costMinimum: number
  currency: string
}

export interface RatesPayload {
  CODMinFee: string
  codRate: string
  name: string
  serviceType: string
  sla: string
  zone: string
  rates: RatesLMFM[]
}

export interface UserSelection extends RatesParams {
  country: string
  submit: boolean
}
