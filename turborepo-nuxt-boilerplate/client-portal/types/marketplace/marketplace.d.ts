import { RootStateMarketplaces } from '~/store/marketplaces/marketplaces'

export interface VuexModuleMarketplaces {
  marketplaces: {
    marketplaces: RootStateMarketplaces
  }
}

export interface FilterDetails {
  page: number
  itemsPerPage: number
  search: string
  country: string
  service: string | string[]
  port: string
}

export interface FilterMarketplaces {
  search: string
}

export interface PaginationMarketplaces {
  page: Number
  itemsPerPage: Number
}

export type PageView = 'marketplaces' | 'networkPartners'

export interface ParamsGetMarketplace {
  page: string | number
  perPage: string | number
  search?: string
  country?: string
  service?: string[]
  zone?: string
  port?: string
  service_zone_type?: boolean
}
