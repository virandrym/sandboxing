import type { Statuses } from './filters.d'
import type { B2BOrderDetails, B2BOrderParcel } from './orders/b2b.d'
import type { StateOrders } from '~/store/orders'

export interface VuexModuleOrders {
  orders: StateOrders
}

export interface LatestUpdate {
  id: string
  status: string
  updateTimestamp: Date
  comments: string
  podReceiverContact: string
  podReceiverName: string
  podURI: string
  rawPayload: string
}

export interface UpdatesOrderAllocation {
  comments: string
  id: string
  podReceiverContact: string
  podReceiverName: string
  podURI: string
  rawPayload: string
  status: string
  updateTimestamp: string
}

export interface OrderAllocationData {
  id: string
  externalTrackingNumber: string
  updates: UpdatesOrderAllocation[]
  mappedUpdates: UpdatesOrderAllocation[]
  partnerID: string
  partnerName: string
  serviceType: string
}

export interface BatchOrders {
  id: string
  code: string
  createdAt: Date
  updatedAt: Date
  clientId: string
  totalOrder: number
}

export interface PartnerUpdates {
  comments: string
  id: string
  podReceiverContact: string
  podReceiverName: string
  podURI: string
  rawPayload: string
  status: string
  updateTimestamp: Date
}

export interface OrderAllocationUpdate {
  id: string
  orderID: string
  orderCode: string
  partnerID: string
  partnerName: string
  serviceType: string
  externalTrackingNumber: string
  externalTracking: {
    id: string
    partnerUpdates: PartnerUpdates[]
    mapPartnerUpdates: PartnerUpdates[]
  }
  updates: PartnerUpdates[]
  mappedUpdates: PartnerUpdates[]
  labelURL: string
}

interface PUDO {
  City: string
  Country: string
  CreatedAt: Date
  DeletedAt: Date
  FirstLine: string
  ID: string
  OrderAllocationID: string
  PostalCode: string
  Province: string
  SecondLine: string
  UpdatedAt: Date
}

interface SellingRates {
  rate: string
  currency: string
  weight: string
  createdAt: Date
}

interface Consignee {
  consigneeName: string
  consigneeNumber: string
  consigneeAddress: string
  consigneePostal: string
  consigneeCountry: string
  consigneeCity: string
  consigneeState: string
  consigneeDistrict?: string
  consigneeProvince: string
  consigneeEmail: string
  consigneeTaxId: string
}

interface Pickup {
  pickupContactName: string
  pickupContactNumber: string
  pickupState: string
  pickupCity: string
  pickupProvince: string
  pickupPostal: string
  pickupCountry: string
  pickupAddress: string
  destPort: { code: string }
  destPortId: string
}

interface Sender {
  senderName: string
  senderNumber: string
  senderContactName: string
  senderContactNumber: string
  senderState: string
  senderDistrict: string
  senderCity: string
  senderProvince: string
  senderPostal: string
  senderCountry: string
  senderAddress: string
  senderPort: { code: string }
  senderPortId: string
}

interface COD {
  codCurrency?: string
  codValue?: number
}

interface Dimension {
  length: number
  width: number
  height: number
  weight: number
}

interface OrderCancellation {
  status: string
  remark: string
}

export interface OrderItem {
  id: string
  orderId: string
  orderCode: string
  description: string
  quantity: number
  productCode: string
  sku: string
  category: string
  price: number
  currency: string
}

export interface Order extends Consignee, Pickup, Sender, Dimension, COD {
  id: string
  orderCode: string
  batchId: string
  batchCode: string
  clientId: string
  refID: number
  labelPath: string
  requestedServices: string[]
  orderAllocations: OrderAllocationUpdate[]
  createdAt?: Date
  mawb?: { mawbId: string; mawbNo: string }
  newOrderId: string
  originalOrderId: string
  customerName?: string
  customerId?: string
  latestLuwjistikStatus?: string
  latestPartnerUpdate?: string
  selfCollectedDropOff: PUDO
  sellingRates?: SellingRates[]
  paymentType: string
  b2bDetails?: B2BOrderDetails | null
  parcels: B2BOrderParcel[] | null
  isE2ESmartRouting: boolean
  insurance: boolean
  cancellation: OrderCancellation | null
}

export interface FilterOrders {
  orderCode: string | null
  batchCode?: string | null
  // mawbId?: string
  createdFrom: string | null
  createdTo: string | null
  originCountry: string | null
  destinationCountry: string | null
  serviceType: string[]
  originPortId: string | null
  destinationPortId: string | null
  status: Statuses
  customerId?: string | null
  clientId?: string[] | null
  terminalStatus?: string | boolean | null
}

export interface FilterBatch {
  batchCode: string
}

export interface FilterMawb {
  mawbId: string
}

// end upload order submission

export interface NodeCalculator {
  [key: string]: {
    bobCost: String | Number
    bobTransmissionFee: String | Number
    ccCost: String | Number
    ccTransmissionFee: String | Number
    codCost: String | Number
    codMinFee: String | Number
    codRate: String | Number
    createdAt: String
    currencyBOB: String
    currencyCC: String
    currencyDomestic: String
    currencyDuties: String
    currencyFM: String
    currencyLM: String
    currencyTax: String
    domesticCost: String | Number
    domesticTransmissionFee: String | Number
    dutiesFee: String | Number
    fmCost: String | Number
    fmTransmissionFee: String | Number
    id: String
    lmCost: String | Number
    lmTransmissionFee: String | Number
    taxFee: String | Number
  }
}

export interface ExportOrdersParams {
  data: string[]
  forPartner: boolean
  partnerId: string
}

export interface OrderDutiesTaxes {
  calculatedTaxes: String | Number
  calculatedDuties: String | Number
  appliedTaxes: String | Number
  appliedDuties: String | Number
  handlingFee: String | Number
  currency: String
}

export interface OrderFlightSchedule {
  carrierCode: string
  carrierNumber: string
  departureAt: Date
  arrivalAt: Date
}
export interface OrderDetails {
  order: Order
  orderItems: OrderItem[]
  orderDutiesTaxes: OrderDutiesTaxes
  orderAllocationUpdates: OrderAllocationUpdate[]
  nodeCalc: NodeCalculator
}

// MAWB
export interface SetAwb {
  orderIds: string[]
  awbNo: string
}

export interface MawbFile {
  file: File[]
}

export interface MawbDocuments {
  awbUrl: string
  hawbUrl: string
  packingListUrl: string
}
