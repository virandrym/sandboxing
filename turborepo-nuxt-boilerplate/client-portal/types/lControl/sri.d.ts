export interface SRI {
  originCountry: string
  destinationCountry: string
}

export interface SRIPostPayload extends SRI {}
export interface SRIDeletePayload extends SRI {}
