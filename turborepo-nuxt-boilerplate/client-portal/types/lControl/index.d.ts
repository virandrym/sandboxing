import { CountryCode, PortData, ServiceType, ZoneData } from '../filters'
import { StateLControls } from '~/store/lControls'

export interface VuexModuleLControls {
  lControls: StateLControls
}

export interface LControlFeatureFlags {
  serviceTypes: string[]
  sri: Record<string, string[]>
}

export interface LControlDetail {
  partnerID: string | null
  volume?: string | number | null
}

export interface LTimizer {
  CUSTOMS: boolean
  DOMESTIC: boolean
  FIRST_MILE: boolean
  FREIGHT_FORWARDER: boolean
  LAST_MILE: boolean
}

export type LControlType = 'modular' | 'sri'

export interface UserSelectedLControl {
  useBOB: boolean
  useBOBModular: LTimizer
  country: CountryCode
  serviceType: ServiceType
  zone: ZoneData
  port: PortData
  defaultPartner: LControlDetail
  codPartner: LControlDetail
  primary: LControlDetail
  secondary: LControlDetail
  tertiary: LControlDetail
}

export interface LControl {
  useBOB?: boolean
  country: string
  serviceType: string
  zoneID: string
  portID: string
  defaultPartner?: LControlDetail
  codPartner?: LControlDetail
  primaryPartner?: LControlDetail
  secondaryPartner?: LControlDetail
  tertiaryPartner?: LControlDetail
}
