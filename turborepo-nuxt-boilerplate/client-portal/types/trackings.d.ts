import { PartnerUpdates } from '@/types/orders'
import { RootStateTracking } from '@/store/trackings'

export interface VuexModuleTracking {
  trackings: RootStateTracking
}

export interface TrackingElement {
  id: string
  serviceType: string
  trackingNumber: string
  partnerName: string
  updates: PartnerUpdates[]
  mappedUpdates: PartnerUpdates[]
}

export interface Tracking {
  orderCode: string
  clientName: string
  clientLogo: string
  isSR: boolean
  createdAt: Date
  trackings: TrackingElement[]
  client: {
    supportWidget: boolean
  }
}
