import { StateStatusMappings } from '~/store/statusMappings'

export interface VuexModuleStatusMappings {
  statusMappings: StateStatusMappings
}

export interface ClientStatusMapping {
  id: string
  clientStatus: string
}

export interface StatusMapping {
  serviceTypeStatusMapping: string
  code: string
  luwjistikStatus: string
  clientStatusMapping: ClientStatusMapping
}

export interface StatusChange {
  clientStatus?: string
  luwjistikStatusCode: string
}
