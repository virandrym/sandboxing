export interface MFA {
  isActive: boolean
  secret: string
  otpAuthURL: string
  base64QRCode: string
}

export interface ParsedOTPAuthURL {
  protocol: string
  issuer: string | null
  account: string
  algorithm: string
  digits: string
  period: string
  secret: string
}

// response - start
export interface MFAResponse {
  secret: string
  otp_auth_url: string
}

export interface MFAStatusResponse {
  otpEnabled: boolean
}

export interface MFALoginResponse
  extends Pick<MFAStatusResponse, 'otpEnabled'> {
  otpEnabled: boolean
  userId: string
}
// response - end

// payload - start
export interface MFAActivationPayload {
  token: string
}

export interface LoginOTPPayload {
  userId: string
  token: string
}
// payload - end
