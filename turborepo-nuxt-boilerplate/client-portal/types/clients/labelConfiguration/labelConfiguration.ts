import { RootStateLabelConfigurations } from '~/store/clients/labelConfiguration/labelConfiguration'

export interface VuexModuleLabelConfig {
  clients: {
    labelConfiguration: RootStateLabelConfigurations
  }
}

export interface LogoConfiguration {
  logo: string
  logo1Preference: string
  logo2Preference: string
  logo3Preference: string
}
