import { RootStateChat } from '~/store/sendbird'

export interface VuexModuleChat {
  sendbird: RootStateChat
}

export interface ChatUser {
  user_id: string
  nickname: string
}
export interface ChatChannel {
  name: string
  channel_url: string
  logo?: string
  client_name: string
}

export interface ChatRoom {
  id: string
  name: string
  sendbirdChannelURL: string
  partner?: {
    name?: string
    logo?: string
  }
}

export interface ActionChat {
  allocationID: string
  orderID: string
  topic: string
  networkPartner: string
  serviceType: string
  orderCode: string
}
