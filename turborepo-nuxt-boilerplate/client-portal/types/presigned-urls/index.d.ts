export interface PresignedURLsFeatureType {
  id: string
  name: string
}

export interface PresignedURLsPayload {
  fileName: string
  category: string
}

export interface PresignedURLs {
  url: string
  path: string
  fileName: string
}

export interface UploadDocuments {
  url: string
  file: File
}
