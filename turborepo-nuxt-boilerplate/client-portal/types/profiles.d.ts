import { RootStateProfiles } from '~/store/profiles'

export interface VuexModuleProfiles {
  profiles: RootStateProfiles
}

// credentials - start
export interface Credential {
  id: string
  clientId: string
  token: string
  keyName: string
  refId: string
  revokedAt: null
  createdAt: string
}

export interface GenerateCredential {
  keyName: string
}
// credentials - end

export interface ResetPasswordParams {
  newPassword: string
  oldPassword: string
}

// subscription - start
export interface Subscription {
  id: string
  clientId: string
  eventType: string
  endpoint: string
  appKey: string
  createdAt: Date
  updatedAt: Date
}

export interface SubscriptionGetParams {
  clientId: string
}

export interface SubscriptionPayload {
  clientId: string
  eventType: string
  url: string
}
// subscription - end
