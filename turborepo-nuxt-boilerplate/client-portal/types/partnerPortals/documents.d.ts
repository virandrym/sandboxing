import { StateMawbDocuments } from '~/store/partnerPortals/documents'
import { Bagged } from '~/types/bagging/bagging'

export interface VuexModuleMawbDocuments {
  documents: StateMawbDocuments
}

export interface DocumentsDetail {
  bags: Bagged[]
  flightDate: string
  flightNo: string
  manifestStatus: string
  manifestUrl: string
  mawbID: string
  mawbNumber: string
  reweight: boolean
}

export interface MawbAttachDocuments {
  category: string
  path: string
}

export interface MawbPostPayload {
  documents: MawbAttachDocuments[]
  serviceType: string
}

export interface MawbDocumentsResponse extends MawbAttachDocuments {
  id: string
}
