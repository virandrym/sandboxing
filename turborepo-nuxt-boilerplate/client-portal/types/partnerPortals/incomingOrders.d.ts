import { Order, OrderAllocationUpdate, OrderItem } from '../orders'
import { Statuses } from '../filters'
import { StateIncomingOrders } from '~/store/partnerPortals/incomingOrders'

export interface VuexModuleIncomingOrders {
  partnerPortals: {
    incomingOrders: StateIncomingOrders
  }
}

export interface IncomingOrder extends Order {
  items: OrderItem[]
  clientName: string
}

export interface FilterOrders {
  orderCode: string
  batchCode: string
  createdFrom: string
  createdTo: string
  originCountry: string
  destinationCountry: string
  serviceType: string[]
  originPortId: string
  destinationPortId: string
  status: Statuses
  clientId: string[]
}

export interface InputUpdateStatus {
  orderIDs: string[]
  status: string
  timestamp: string
  service_type: string
  remarks: string
}

export interface IncomingOrderDetails {
  order: IncomingOrder
  allocation: OrderAllocationUpdate[]
}

export interface UpdateStatusBulkPayload {
  file: File[]
}
