import { Meta, PaginationPayload } from '../applications'
import { RootStateBagging } from '~/store/bagging/bagging'
import { Order as OrderGeneral } from '~/types/orders'

export interface VuexModuleDetailBagging {
  bagging: { bagging: RootStateBagging }
}

export interface TabState {
  orderView: {
    [key: string]: number
  }
  step: number
}

export interface ContentGenerateManifest {
  groupName?: string
  length: string | number
  width: string | number
  height: string | number
  weight: string | number
  weightFromData: string | number
  needReweight: Boolean
}
export interface GenerateManifest {
  [key: string | number]: ContentGenerateManifest
}
export interface InputManifest {
  manifest: boolean
  mawb: string
  generateManifest: GenerateManifest
  originPortCode: string
  destinationPortCode: string
}

export interface dataInputReweight {
  [key: string | number]: number | string
}
export interface InputReweight {
  reweight: boolean
  total: number
  input: dataInputReweight | {}
}
export interface BagManifest {
  bag_id: string
  length: number
  width: number
  height: number
  weight: number
  originPortCode: string
  destinationPortCode: string
}

export interface InputManifestAPI {
  mawb: string
  bag_manifest: BagManifest[]
}
export interface InputLabelBags {
  bag_id: string
  group_name: string
  partnerID?: string
}

export interface InputDeleteBag {
  partner_id: string
  bag_id: string
  order_id: string
  reason: string
  comments: string
  id: string
}

export interface InputPostBag {
  bag_name: string
  order_ids: string[]
}
export interface InputPostScannableBagOrders {
  // can be order code or bag code
  code: string
  // deprecated
  bag_id: string
  order_id: string
}

export interface ScanBagOrderResponse {
  error?: string
  orderCode?: string
  bagCode?: string
  scanType?: string
  partnerOrdinal?: number
}

export interface Order {
  id: string
  order_code?: string
  weight?: number
  measured_weight?: number
  orderScanStatus?: number
  isHighValue?: boolean
  partnerOrdinal?: number
  transhipmentStatus?: string
}

export interface Bagged {
  id: string
  group_name: string
  dest_port: string
  dest_country: string
  departure: string
  arrival?: string
  flight: string
  flight_date: string
  origin_port: string
  status: string
  orders: Order[]
  mawb: string
  label_url: string
  bags?: Bagged[] | null
  is_owner?: boolean
  weight: number
  is_editable?: boolean
}

export interface Unbagged {
  dest_port: string
  dest_country: string
  order_group: Bagged[]
}

export interface BagUpdate {
  id: string
  bag_id: string
  status: string
  comments: string
  update_timestamp: string
}

export interface BagData {
  bagged: Bagged[]
  unbagged: Unbagged[]
}

export interface BagDataPartner {
  bagged: Unbagged[]
  unbagged: Unbagged[]
}

export interface CompletedBag {
  id: string
  group_name: string
  dest_port: string
  dest_country: string
  origin_port: string
  status: string
  flight: string
  flight_date: string
  departure: string
  arrival: string
  orders: Order[] | null
  mawb: string
  label_url: string
  bags: Bagged[] | null
  is_owner: boolean
  is_sealed: boolean
  weight: number
  width: number
  height: number
  length: number
  manifest_status: string
  is_detail_bag_tab?: boolean
}

export interface FilterBagging {
  createdFrom: string
  createdTo: string
  client: string
  destination: string
}

// bagged/orders
export interface ParamsGetBaggedOrders
  extends PaginationPayload,
    FilterBagging {}

export interface BaggedOrders extends Meta {
  data: Bagged[]
}

// unbagged
export interface ParamsGetUnbaggedOrders extends PaginationPayload {
  origin: string
  destination: string
  cc_partner: string
  lm_partner: string
}

export interface UnbaggedPartner {
  CcCode?: string
  CcPartner?: string
  Count?: number
  LmCode?: string
  LmPartner?: string
}

export interface UnbaggedCount {
  ConsigneeCountry: string
  Count: number
  DestPortName: string
  DestinationPortId: string
  OrigPortName: string
  SenderPortId: string
  Partners: UnbaggedPartner[]
}

export interface UnbaggedOrder extends OrderGeneral {}

export interface IncomingFFOrder {
  orderId: string
  orderCode: string
  portOriginCode: string
  portDestinationCode: string
  clientName: string
  bagId: string
  bagCode: string
  partnerOrdinal: number
}

export interface ScannableOrders {
  [x: string]: any
  orderId: string
  orderCode: string
  orderScanStatus: number
  bagId?: string
  bagCode?: string
  bagScanStatus?: number
  isHighValue: boolean
  partnerOrdinal: number
}
export interface ScannableBags {
  bagCode?: string
  bagId?: string
  orders: ScannableOrders[]
  scanStatus: number
}

export interface ScannableBagOrders {
  bags: ScannableBags[]
  orders: ScannableOrders[]
}

export interface FilterIncoming {
  orderId?: string
  originPortId?: string
  destinationPortId?: string
  clientId?: string
  bagId?: string
}

export interface ParamsGetIncomingOrders
  extends PaginationPayload,
    FilterIncoming {}

export interface InputRemoveOrder {
  bagId: string
  orderId: string
}
