import { PaginationPayload } from '../applications'
import { RootStateLFreight } from '~/store/lFreight/lFreight'

export interface VuexModuleLFreight {
  lFreight: RootStateLFreight
}

export interface Lane {
  time: string
  port: string
  airport: string
  // diffDay?: string
  // date?: Date
}

export interface Plane {
  code: string
  name: string
}

export interface Milestone {
  from: Lane
  to: Lane
  plane: Plane
}

export interface Flight {
  id: string
  date: string
  is_confirmed: boolean
  confirmed_rate: number
  total_weight: number
  max_weight: number
  owned_weight: number
  flight_status: string
  milestone: Milestone
  airlineName: string
}

export interface FlightList {
  tab_name: string
  content: Flight[]
}

export interface PostReservePayload {
  flightId: string
  weight: number
}

export interface ActiveLane {
  id: string
  bookingConfirmed: number
  bookingRequested: number
  destinationPortCode: string
  destinationPortId: string
  destinationPortName: string
  originPortCode: string
  originPortId: string
  originPortName: string
}

export interface ActiveList {
  content: ActiveLane[]
}

export interface PostRequestPayload {
  originPortID: string
  destinationPortID: string
  bookingDate: Date
  weight: number
}

export interface Booking {
  originPortID: string
  destinationPortID: string
  bookingDate: Date
  status: string
}

export interface BookingList {
  content: Booking[]
}

export interface ParamsGetClientBookings extends PaginationPayload, Booking {}
