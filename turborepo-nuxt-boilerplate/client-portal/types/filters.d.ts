import { CancelToken } from 'axios'
import { Meta, PaginationPayload } from './applications'
import { StateFilters } from '~/store/filters'
// import { Definition } from '~/types/lControl/lControl'

export interface VuexModuleFilters {
  filters: StateFilters
}

export interface GetZonesPayload {
  country: string
  is_pickup_origin?: boolean
}
export interface Zone {
  id: string
  country: string
  zoneName: string
  // definitions?: Definition[]
  partnerID?: string
  priority?: number | null
  ruleGroupID?: string
  ruleID?: string
  serviceType?: string
}

export interface ZoneData {
  code: string
  countryCode: string
  id: string
  zoneName: string
}

export type ColorServiceType =
  | 'purple darken-1'
  | 'secondary darken-2'
  | 'brown darken-1'
  | 'warning'
  | 'cyan darken-1'
  | 'pink darken-1'
  | 'success darken-2'
  | 'primary darken-1'
  | 'purple'
  | 'secondary'
  | 'brown'
  | 'warning'
  | 'cyan'
  | 'pink'
  | 'success'
  | 'primary'

export interface ServiceType {
  id: string
  name: string
}

export interface GetCountriesPayload {
  isActive: boolean
}

export interface CountryCode {
  name: string
  value: string
}

export interface GetPortsPayload {
  params: PaginationPayload
  country?: string
  cancelToken?: CancelToken
}

export interface PortData {
  code: string
  countryCode: string
  id: string
  name: string
}

export interface Ports {
  data: PortData[]
  page: number
  totalCount: number
  totalPage: number
}

export interface Statuses {
  Remarks: string
  ServiceType: string
  Status: string
}

// customer
export interface Customer {
  customerName: string
  customerId: string
}
export interface CustomerPaginated extends Meta {
  data: Customer[]
}

// clients on partner
export interface Clients {
  id: string
  name: string
  slug: string
}

export interface ClientsPaginated extends Meta {
  data: Clients[]
}

export interface TerminalStatus {
  text: string
  value: string
}

export interface GetServiceableLanesPayload {
  incoterm: string
}

interface ServiceableB2BPort extends PortData {
  countryName: string
}
export interface ServiceableB2BLanes {
  id: string
  incoterm: string
  originPort: ServiceableB2BPort
  destinationPort: ServiceableB2BPort
}
