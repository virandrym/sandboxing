export interface Login {
  email: string
  password: string
}

export interface Client {
  ID: string
  CreatedAt: string
  UpdatedAt: string
  DeletedAt: string | null
  slug: string
  name: string
  useLogoInLabel: boolean
  logo: string | null
  defaultCurrency: string | null
  useDms: boolean
  companyWebsite: string
  entityName: string
  companyRegistrationNo: string
  companyRegisteredAddress: string
  companyBillingAddress: string
  gstOrVat: string
  billingEmail: string
  financeContactNumber: string
  financeEmailContact: string
  commercialEmailContact: string
  commercialContactNumber: string
  omsLite: boolean
  insurance: boolean
  cod: boolean
  consigneeEmailNotification: boolean
  onAftership: boolean
  suspensionStartDate: string
}

interface PartnerServiceDetail {
  CreatedAt: string
  DeletedAt: string | null
  ID: string
  Name: string
  UpdatedAt: string
}
export interface PartnerServiceTypes {
  CreatedAt: string
  DeletedAt: string | null
  ID: string
  IsActive: boolean
  PartnerID: string
  Service: PartnerServiceDetail
  ServiceID: string
  UpdatedAt: string
}
export interface PartnerProfiles {
  client: Client
  partnerId: string
  isFreight: boolean
  partnerServiceType: PartnerServiceTypes[]
}

export interface SuspensionAllowedActions {
  addCustomer: boolean
  connectPartner: boolean
  login: boolean
  submitOrder: boolean
  uploadDocuments: boolean
}

export type RoleType = 'CLIENT_CUSTOMER' | 'CLIENT_COMPANY_ADMIN'

export interface User {
  email: string
  role: RoleType
  clientId: string
  partnerProfiles: PartnerProfiles[]
  suspensionAllowedActions: SuspensionAllowedActions
  initial?: string
}

export interface LoginResponse extends Omit<User, 'initial'> {
  token: string
}
