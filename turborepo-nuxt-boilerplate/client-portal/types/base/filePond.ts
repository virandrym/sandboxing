// file pond error type
export interface ErrorFilePond {
  main: string
  sub: string
}

export interface FileCategorized {
  category: string
  files: File[]
}
