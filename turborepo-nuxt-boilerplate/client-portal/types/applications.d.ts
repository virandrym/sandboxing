import { DataTableHeader, DataOptions } from 'vuetify'
import { RootStateApplications } from '~/store/applications'
export interface VuexModuleApplications {
  applications: RootStateApplications
}

//  for side navigation list menu - start
export interface NavigationLinks {
  title: string
  to: string
  icon?: string
}
//  for side navigation list menu - end

//  for v-alert types - start
export type AlertType = '' | 'success' | 'warning' | 'error'

export interface Alert {
  isShow: boolean
  type: AlertType
  message: string | string[]
}
//  for v-alert types - end

//  for v-form types - start
export type VForm = Vue & {
  validate: () => boolean
  reset: () => boolean
}
//  for v-form types - end

//  for v-modal types - start
export interface ModalConfirm {
  loading: boolean
  title: string
  content: string
  cancelText: string
  submitText: string
  submitColor: string
  type?: string
  avatar?: string
  subtitle?: string
  disabledSubmit?: boolean
}
//  for v-modal types - end

//  for v-breadcrumbs types - start
export interface Breadcrumbs {
  text: string
  disabled: boolean
  to: string
  exact?: boolean
}
//  for v-breadcrumbs types - end

//  **for v-data-table - start
//  use this for types v-data-table options (table pagination)
export interface Pagination
  extends Partial<Omit<DataOptions, 'page' | 'itemsPerPage'>> {
  page: number
  itemsPerPage: number
}

export interface Header extends DataTableHeader {}

export interface ActionsTable {
  export?: boolean
  edit?: boolean
  downloadSingle?: boolean
  downloadSelection?: boolean
  downloadBagging?: boolean
  updates?: boolean
  manageUser?: boolean
  editUser?: boolean
  reweight?: boolean
  manifestBag?: boolean
  mawbBag?: boolean
  detail?: boolean
  delete?: boolean
}
//  **for v-data-table - end

//  **fetch types - start
//  use this for pagination payload to the API
export interface PaginationPayload extends Omit<Pagination, 'itemsPerPage'> {
  perPage: number
}

//  meta-data pagination from the API
export interface Meta {
  page: number
  totalPage: number
  totalCount: number
}

export interface PaginatedResponse<T> extends Meta {
  data: T
}

export interface FetchState {
  error: Error | null
  pending: boolean
  timestamp: number
}

// types standard for error from API
export interface ErrorDetails {
  field: string
  reason: string
  note: string
}

export interface ErrorAPI {
  error?: string
  ErrorDetails?: ErrorDetails[]
  Message?: string
}
//  **fetch types - end

//  for v-form - start
export interface SelectItem<T = string | number> {
  text: string
  value: T
}
//  for v-form - end
