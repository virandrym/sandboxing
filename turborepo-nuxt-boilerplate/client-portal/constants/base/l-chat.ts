export const ZENDESK_LINK = 'https://static.zdassets.com/ekr/snippet.js'

export const VUE_ADVANCED_CUSTOM_STYLES = `.avatar-custom {
        position: absolute;
        left: 0;
        top: 0;
        transform: translateX(-50%) translateY(-50%);
      }
      .user-custom {
        position: absolute;
        left: 25px;
        top: -5px;
        transform: translateY(-100%);
        color: grey;
        font-weight: 600;
        font-size: 13px;
      }
      .timestamp-custom {
        font-size: 8px;
        justify-content: right;
        display: flex;
        position: absolute;
        color: grey;
        right: 5px;
        bottom: -2px;
        transform: translateY(100%);
      }
      .custom-card-chat {
        box-shadow: 0px 2px 1px -1px rgb(0 0 0 / 9%),
          0px 1px 5px 0px rgb(0 0 0 / 10%), 0px 1px 12px 0px rgb(0 0 0 / 8%) !important;
      }
      .vac-files-box div:not(:nth-child(1)) .vac-room-file-container {
        display: none !important;
      }
      .vac-files-box .vac-svg-button {
        display: none !important;
      }
      .vac-media-preview {
        width: 100% !important;
      }
      .message-header-custom,
      .message-header-no-messages {
        padding: 7.5px 0px 7.5px 14px;
        font-size: 14px;
        font-weight: 500;
        cursor: auto;
      }

      .message-header-no-messages {
        color: grey;
        text-align: center;
        margin: 20px 0;
        font-size: 12px;
      }

      .vac-textarea {
        min-height: 60px !important;
      }

      .vac-message-wrapper .vac-message-current {
        background-color: #1961e4 !important;
        color: #fff;
      }

      .vac-message-wrapper .vac-message-current .vac-text-timestamp {
        color: #fff !important;
      }

      .vac-message-wrapper .vac-offset-current {
        margin-left: 20%;
      }

      .vac-message-wrapper .vac-message-box {
        max-width: 80%;
      }

      .vac-file-container .vac-text-ellipsis {
        color: #1961e4 !important;
      }

      .vac-message-files-container .vac-file-wrapper .vac-file-container {
        width: 160px !important;
      }`
