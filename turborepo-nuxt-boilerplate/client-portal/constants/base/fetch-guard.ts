export const enum USER_TYPE {
  clients = 'clients',
  customers = 'customers',
  guest = 'guest',
}

export const enum BASE_URL {
  clients = '/api/clients',
  customers = '/api/clients/customers',
  guest = '/api',
}
