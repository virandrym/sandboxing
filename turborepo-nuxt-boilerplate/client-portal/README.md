# Client Portal

The client portal is the front end that Luwjistik clients use to interact with the Luwjistik platform. It may include basic functionalities like uploading orders, connecting to partners (and confirming connection as a partner), managing their credentials, controlling the order allocation rules, etc. ([Client Portal Overview](https://luwjistik.slab.com/posts/systems-overview-7210vab3#ha76d-clients-portal))

<!-- DEMO -->

## Demo

[dev/staging](https://dev.luwjistik.io)

[beta/sandbox](https://sandbox.luwjistik.io)

[release/prod](https://v2.luwjistik.io)

<!-- END DEMO -->

<!-- ENV VAR -->

## Environment Variables

To start the local development server, simply run `npm run dev`. This will start the development server based on environtment variabel `PORT` in `.env` file.

All API calls are sent to the same origin as the frontend server. On the production deployment, the server will be setup to redirect the traffic accordingly.
On local environment, the development server will proxy any unknown requests to a backend server as well.

By default, this will proxy the requests to `http://localhost:8080`, assuming you have the backend server running on your local environment on port `8080` as well.
To change this behaviour, copy the `.env.example` file to `.env`, and change the `PROXY` line to point to the backend server that you'd like to use (e.g. `https://dev.luwjistik.io` for the staging server).

<!-- END ENV VAR -->

<!-- RUN -->

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/luwjistik1/client-portal.git

  # or with ssh
  git clone git@gitlab.com:luwjistik1/client-portal.git
```

Go to the project directory

```bash
  cd client-portal
```

Install dependencies

```bash
  npm install
  # or
  npm install --legacy-peer-deps
```

Start the server

```bash
  npm run dev

  # build for production and launch server
  $ npm run build
  $ npm run start
```

<!-- END RUN -->

<!-- CONTRIBUTE -->

## Contributing

1. Pull latest `main` branch

```shell
$ git checkout main

$ git pull origin main
```

2. Create new feature/bug branch

```shell
$ git checkout -b <new-feature>
```

3. Push feature branch to remote repository

```shell
# please runt lint then fix them
$ npm run lint:fix

# before push don't forget to pull from main
$ git fetch--all && git pull origin main
$ git push origin --set-upstream origin <new-feature>
```

4. Create merge request, on gitlab

5. Ask for approval by one or more engineers

6. Merge and delete `<new-feature>` branch
<!-- END CONTRIBUTE -->

<!-- CONVENTION -->

## Coding Convention

### Function

- Use camelCase for function name;
- If the function is an action, it uses the prefix "do". Example: doSubmit, doDelete, etc;
- If the function is computation, it uses the prefix "compute". Example: computeProgress, computeTotalScore, etc (flexible);

### Variable

- Use camelCase for everything variable name;
- Use kebab-case for everything in the `pages` folder;
- use kebab-case for folders under the `components` folder but use PascalCase for all component names;
- For variable naming, if the type is boolean, use the prefix "is". example: isLoading, isDataExist, etc;
- Global Constants use UPPERCASE and use `_`. Example: IMAGE_PATH, BG_PATH, etc (flexible);

### Comments

DON'T FORGET TO USE THE COMMENTS IF THE THING THAT YOU DO IS COMPLEX. Example for: Complicated methods, complicated calculation operations, logical flow.

<!-- END CONVENTION -->

<!-- DOCUMENTATION -->

## Related README

[About Store](./store/README.md)

[About Fetch Engine](./composables/base/fetch-engine/README.md)

## Documentation

[Nuxt.js](https://nuxtjs.org/)

[Vuetify](https://vuetifyjs.com/en/)

[Nuxt Composition API](https://composition-api.nuxtjs.org/)

[Nuxt Typescript](https://typescript.nuxtjs.org/)

[Nuxt Auth](https://auth.nuxtjs.org/)

[Nuxt Axios](https://axios.nuxtjs.org/)

<!-- END DOCUMENTATION -->

<!-- TECH STACK -->

## Tech Stack

**Client:** Nuxt.js, Vuex, Vuetify

**Server:** Node

<!-- END TECH STACK -->

<!-- TEST -->

## Test runner

The `test.Dockerfile` is the base image used to run the tests in CI. It also pre-caches the build dependencies.

```
docker build -t registry.gitlab.com/luwjistik1/client-portal/baseimage -f test.Dockerfile .

docker push registry.gitlab.com/luwjistik1/client-portal/baseimage
```

<!-- END TEST -->
