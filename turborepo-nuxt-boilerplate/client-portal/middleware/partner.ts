import { defineNuxtMiddleware } from '@nuxtjs/composition-api'

// types - start
import { User } from '~/types/base/applications/login'
// types - end

export default defineNuxtMiddleware(({ $auth, redirect }) => {
  const user = $auth.$storage.getLocalStorage('user') as User
  const isPartnerAccount = !!user?.partnerProfiles?.length

  if (!isPartnerAccount) {
    redirect('/')
  }
})
