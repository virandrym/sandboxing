import { defineNuxtMiddleware } from '@nuxtjs/composition-api'
import { useSessionStorage } from '@vueuse/core'

export default defineNuxtMiddleware(({ redirect }) => {
  const userIdSession = useSessionStorage('userId', '')
  const otpEnabledSession = useSessionStorage('otpEnabled', false)

  if (!userIdSession.value || !otpEnabledSession.value) {
    return redirect('/login')
  }
})
