import { defineNuxtMiddleware } from '@nuxtjs/composition-api'

// types - start
import type { User } from '~/types/base/applications/login'
// types - end

export default defineNuxtMiddleware((context) => {
  const { from, route, store, $auth } = context

  if (!from.path.includes(route.path) && !route.path.includes(from.path)) {
    store.commit('applications/RESET_ALERT')
    store.commit('applications/RESET_PAGINATION')

    if (route.name?.includes('orders')) {
      store.commit('orders/RESET_FILTER_ORDERS')
      store.commit('orders/RESET_FILTER_BATCH')
      store.commit('orders/RESET_ORDER_TAB_VIEW')
      store.commit('partnerPortals/incomingOrders/RESET_FILTER')
    } else if (route.name?.includes('marketplace')) {
      store.commit('marketplaces/marketplaces/RESET_FILTER')
      store.commit('marketplaces/marketplaces/RESET_DETAIL_MARKETPLACE')
      store.commit('marketplaces/marketplaces/RESET_PAGE_VIEW')
    }
  }

  if ((context as any)?.$ga) {
    const user = $auth.$storage.getLocalStorage('user') as User
    ;(context as any).$ga.set({ user })
  }
})
