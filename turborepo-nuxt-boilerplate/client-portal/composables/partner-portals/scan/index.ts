import * as dateFns from 'date-fns'

// interface and types
import { Bagged, Unbagged, Order, InputPostBag } from '~/types/bagging/bagging'
import { CountryCode } from '~/types/filters'
// composables
import { parsingBagged } from '~/composables/partner-portals/bagging/index'

export function searchName(
  barcode: string,
  unbaggedData: any,
  isBagsTabPartner: any,
  params?: { isTranshipment: boolean; transhipmentGroupName: string | boolean }
) {
  let dataFilter = [...unbaggedData.value].map((x: Unbagged, _i: number) => {
    return x.order_group
  }) as any

  dataFilter = [].concat.apply([], dataFilter)

  if (isBagsTabPartner.value) {
    // filter array of bagged orders to check if barcode exists in array
    dataFilter = dataFilter.filter((x: Bagged) => x.group_name === barcode)
  } else {
    dataFilter = dataFilter.filter(
      (x: Bagged) =>
        x.orders && x.orders.some((y: Order) => y.order_code === barcode)
    )
  }

  if (dataFilter && dataFilter.length > 0) {
    const mainData = dataFilter[0] as Bagged
    let id = ''
    if (isBagsTabPartner.value) {
      id = mainData.id
    } else {
      const parsingOrders = mainData.orders.filter(
        (x: Order) => x.order_code === barcode
      )[0]
      id = parsingOrders.id
    }
    return {
      dest_country: mainData.dest_country,
      dest_port: mainData.dest_port,
      group_name:
        params && params.isTranshipment && params.transhipmentGroupName
          ? params.transhipmentGroupName
          : mainData.group_name,
      id,
    }
  } else {
    return {}
  }
}

export function indexPayloadArr(
  allScanned: any,
  params: { data: any; isNew?: boolean }
) {
  const parsingNewOnlyAllScanned = [...allScanned.value].filter(
    (x: any) => x.new
  )
  const lastNewAllScanned =
    parsingNewOnlyAllScanned[parsingNewOnlyAllScanned.length - 1]

  const indexPayloadArr = params.data.findIndex((x: any) =>
    x.order_codes?.includes(lastNewAllScanned.order_code)
  )

  return indexPayloadArr
}

export function setPayloadForAPI(
  isBagsTabPartner: string | boolean,
  isHighValue: boolean,
  payloadArr: any,
  allScanned: any,
  params?: { isTranshipment: boolean; transhipmentGroupName: string | boolean }
) {
  let payloadForAPI = {} as {
    bag_name: string
    bag_ids?: string[]
    order_ids?: string[]
  }
  if (isBagsTabPartner) {
    payloadForAPI = {
      bag_name:
        payloadArr[indexPayloadArr(allScanned, { data: payloadArr })].bag_name,
      bag_ids:
        payloadArr[indexPayloadArr(allScanned, { data: payloadArr })].order_ids,
    }
  } else {
    // do not include transhipment partner in bag name if is transhipment order
    payloadForAPI = {
      bag_name:
        params && params.isTranshipment && params.transhipmentGroupName
          ? params.transhipmentGroupName +
            '-' +
            dateFns.format(new Date(), 'yyyyMMddHHmmss')
          : payloadArr[indexPayloadArr(allScanned, { data: payloadArr })]
              .bag_name,
      order_ids:
        payloadArr[indexPayloadArr(allScanned, { data: payloadArr })].order_ids,
    }

    if (isHighValue) {
      payloadForAPI.bag_name = payloadForAPI.bag_name + '-H'
    }
  }

  return payloadForAPI
}

export function searchCountry(countryCode: string, countryCodes: any) {
  const filterCountry = [...countryCodes.value].filter(
    (x: CountryCode) => x.value === countryCode
  )[0]
  return filterCountry?.name ?? 'No Data'
}

export function parseInput(
  isBagsTabPartner: any,
  unbaggedData: any,
  allScannedNotVerif: any,
  countryCodes: any,
  params?: { isNew?: boolean }
) {
  // const $dateFns = inject('$dateFns', dateFns);
  let orderGroups = []
  let payload = [] as any

  if (isBagsTabPartner.value) {
    orderGroups = parsingBagged(unbaggedData.value)
      .map((x: Unbagged) => x.order_group && x.order_group)
      .filter((z: any) => z && z.length > 0)
    // returns all bagged bags in an array by bagname/order_groups
    orderGroups = [].concat.apply([], orderGroups)

    // return array of bag that has been scanned
    payload = parsingBagged(unbaggedData.value)
      .map((x: any) => {
        return {
          country: searchCountry(x.dest_country, countryCodes),
          groupName: x.group_name,
          order_codes: (
            x.order_group &&
            x.order_group.filter((y: any) =>
              allScannedNotVerif.value.some(
                (z: { order_code: string; new: boolean }) => {
                  // z.order_code === y.group_name && z.new
                  if (params?.isNew)
                    return z.order_code === y.group_name && z.new
                  else return z.order_code === y.group_name
                }
              )
            )
          )?.map((k: any) => k.group_name),
          bag_name:
            x.group_name +
            '-' +
            dateFns.format(
              new Date(),
              'yyyyMMddHHmmss'
              // 'yyyy-MM-dd\'T\'HH:mm:ss.SSS'
            ),
          order_ids: (
            x.order_group &&
            x.order_group.filter((y: any) =>
              allScannedNotVerif.value.some(
                (z: { order_code: string; new: boolean }) => {
                  // z.order_code === y.group_name && z.new
                  if (params?.isNew)
                    return z.order_code === y.group_name && z.new
                  else return z.order_code === y.group_name
                }
              )
            )
          )?.map((k: any) => k.id),
        }
      })
      .filter((q: InputPostBag) => q.order_ids && q.order_ids.length)
  } else {
    orderGroups = unbaggedData.value
      .map((x: Unbagged) => x.order_group && x.order_group)
      .filter((z: any) => z && z.length > 0)
    orderGroups = [].concat.apply([], orderGroups)

    payload = orderGroups
      .map((x: Bagged) => {
        return {
          country: searchCountry(x.dest_country, countryCodes),
          groupName: x.group_name,
          order_codes: (
            x.orders &&
            x.orders.filter((y) =>
              allScannedNotVerif.value.some(
                (z: { order_code: string; new: boolean }) => {
                  // z.order_code === y.order_code && z.new
                  if (params?.isNew)
                    return z.order_code === y.order_code && z.new
                  else return z.order_code === y.order_code
                }
              )
            )
          )?.map((k) => k.order_code),
          bag_name:
            x.group_name +
            '-' +
            dateFns.format(
              new Date(),
              'yyyyMMddHHmmss'
              // 'yyyy-MM-dd\'T\'HH:mm:ss.SSS'
            ),
          order_ids: (
            x.orders &&
            x.orders.filter((y) =>
              allScannedNotVerif.value.some(
                (z: { order_code: string; new: boolean }) => {
                  // z.order_code === y.order_code && z.new
                  if (params?.isNew)
                    return z.order_code === y.order_code && z.new
                  else return z.order_code === y.order_code
                }
              )
            )
          )?.map((k) => k.id),
        }
      })
      .filter((q: InputPostBag) => q.order_ids && q.order_ids.length)
  }

  return payload
}

export function checkHighValue(order: Order) {
  if (order.isHighValue) {
    // hide if unassigned or if is transhipment order and FF 1
    if (
      order.transhipmentStatus === 'unassigned' ||
      (order.isHighValue &&
        order.transhipmentStatus !== 'direct' &&
        order.partnerOrdinal &&
        order.partnerOrdinal < 2)
    ) {
      return false
    }
    return true
  }
}
