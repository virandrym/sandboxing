import type { PartnerServiceTypes } from '~/types/base/applications/login'

export const isFreightPartner = (data: PartnerServiceTypes[]) => {
  return data.some(
    (serviceType: PartnerServiceTypes) =>
      serviceType.Service.Name === 'FREIGHT_FORWARDER'
  )
}

export const isCustomsPartner = (data: PartnerServiceTypes[]) => {
  return data.some(
    (serviceType: PartnerServiceTypes) => serviceType.Service.Name === 'CUSTOMS'
  )
}
