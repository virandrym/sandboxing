import { ref } from '@nuxtjs/composition-api'
import { Unbagged, Bagged, Order } from '~/types/bagging/bagging'

export interface BaggedTranshipment extends Bagged {
  group_name: string
  orders: Order[]
}
export interface UnbaggedTranshipment {
  dest_port: string
  dest_country: string
  order_group: BaggedTranshipment[]
}

// for old orders, transhipmentStatus '' === 'direct'
// ** for unbagged tab - start
function getUnassignedOrders(data: Unbagged[]) {
  // extract all orders into a single array
  const combinedArr =
    (data
      .map((x: Unbagged) => x.order_group.map((y: Bagged) => y.orders))
      .flat(2) as Bagged[]) || []
  // identify orders that are unassigned
  const unassigned = [
    {
      dest_port: '',
      dest_country: 'Unassigned',
      order_group: combinedArr.filter(
        (x: Order) => x.transhipmentStatus === 'unassigned'
      ),
    },
  ] as Unbagged[]
  return unassigned
}

export function getTranshipmentOrders(data: Unbagged[]) {
  // identify orders that are transhipment and FF 1
  const transhipmentFF1 = data.map((x) => ({
    ...x,
    order_group: x.order_group
      ?.map((y) => ({
        ...y,
        group_name: y.origin_port + y.dest_port,
        orders: y.orders.filter(
          (z) =>
            z.transhipmentStatus !== 'direct' &&
            z.transhipmentStatus !== 'unassigned' &&
            z.transhipmentStatus !== '' &&
            z.partnerOrdinal &&
            z.partnerOrdinal <= 1
        ),
      }))
      .filter((x) => x.orders && x.orders.length),
  }))

  // for transhipment orders that are going from same origin to destination, group them together for display
  const combined = transhipmentFF1
    .map((x) => {
      const orderGroups: { [key: string]: BaggedTranshipment } = {}

      x.order_group.forEach((y) => {
        const groupName = y.group_name
        if (!orderGroups[groupName]) {
          orderGroups[groupName] = { ...y }
        } else {
          orderGroups[groupName].orders.push(...y.orders)
        }
      })

      return {
        ...x,
        order_group: Object.values(orderGroups),
      }
    })
    .filter((z) => z.order_group && z.order_group.length)

  return combined
}

// filter for orders that are direct
function getDirectOrders(data: Unbagged[]) {
  const direct = data
    .map((x) => {
      return {
        ...x,
        order_group: x.order_group
          .map((y) => {
            return {
              ...y,
              orders: y.orders.filter(
                (z: Order) =>
                  z.transhipmentStatus === 'direct' ||
                  z.transhipmentStatus === ''
              ),
            }
          })
          .filter((y) => y.orders && y.orders.length),
      }
    })
    .filter((x) => x.order_group && x.order_group.length)

  return direct
}

// filter for orders that are FF 2
function getTranshipmentOrdersFF2(data: Unbagged[]) {
  const transhipmentFF2 = data
    .map((x) => {
      return {
        ...x,
        order_group: x.order_group.map((y) => {
          return {
            ...y,
            // replace origin port for FF2
            group_name: y.origin_port + y.group_name.slice(3),
            orders: y.orders.filter(
              (z: Order) =>
                z.transhipmentStatus !== 'direct' &&
                z.transhipmentStatus !== '' &&
                z.partnerOrdinal &&
                z.partnerOrdinal >= 2
            ),
          }
        }),
      }
    })
    .map((x) => {
      return {
        ...x,
        order_group: x.order_group.filter((y) => y.orders && y.orders.length),
      }
    })
    .filter((x: Unbagged) => x.order_group && x.order_group.length)
  return transhipmentFF2
}

// separate assigned orders based on direct, FF1, FF2 and return them combined after manipulating the group_name
function assignedAndTranshipmentOrders(data: Unbagged[]) {
  // filter for orders that are direct
  const assigned = getDirectOrders(data)

  // filter for orders that are FF 2
  const transhipmentFF2 = getTranshipmentOrdersFF2(data)

  // filter for orders that are FF 1
  const transhipment = getTranshipmentOrders(data)

  // for transhipment FF 2 orders that have the same group_name (org, dest, cc, lm partners are the same) as direct orders, group them together
  const combined = assigned.map((x: Unbagged) => ({ ...x }))

  combined.forEach((x: Unbagged) => {
    transhipmentFF2.forEach((a: Unbagged) => {
      if (x.dest_port === a.dest_port) {
        x.order_group.forEach((y: Bagged) => {
          a.order_group.forEach((b: Bagged) => {
            if (y.group_name === b.group_name) {
              y.orders.unshift(...b.orders)
            }
          })
        })
      }
    })
  })

  // // re-assign transhipment FF 1 orders to dest_port groupfor display
  combined.forEach((x: Unbagged) => {
    transhipment.forEach((y: Unbagged) => {
      if (x.dest_port === y.dest_port) {
        x.order_group.unshift(...y.order_group)
      } else {
        assigned.push({ ...y })
      }
    })
  })
  return combined
}

// for partner unbagged get unassigned orders
export const getUnassignedTemp = (data: Unbagged[]) => {
  const temp = ref(getUnassignedOrders(data))
  return temp
}

// for partner unbagged get assigned orders
export const getAssignedTemp = (data: Unbagged[]) => {
  const temp = ref(assignedAndTranshipmentOrders(data))
  return temp
}

// for partner unbagged get transhipment orders
export const getTranshipmentTemp = (data: Unbagged[]) => {
  const temp = ref(getTranshipmentOrders(data))
  return temp
}
// ** for unbagged tab - end

// ** for bagged tab - start
function getUnassignedBags(data: Unbagged[]) {
  const unassigned = [] as Bagged[]
  data.forEach((x: Unbagged) => {
    x.order_group.forEach((y: Bagged) => {
      if (y.status === 'Unassigned bag') {
        unassigned.push(y)
      }
    })
  })
  return unassigned
}

function removeUnassignedBags(data: Unbagged[]) {
  const assigned = data.map((w: any) => {
    return {
      ...w,
      bag_group:
        w.bag_group &&
        w.bag_group.map((x: Unbagged) => {
          return {
            ...x,
            order_group: x.order_group.filter(
              (y: Bagged) => y.status !== 'Unassigned bag'
            ),
          }
        }),
    }
  })
  return assigned
}

function getTranshipmentBags(data: Unbagged[]) {
  const transhipment = [] as String[]
  data.forEach((x: Unbagged) => {
    x.order_group.forEach((y: Bagged) => {
      const isTranshipmentBag = y.orders.some(
        (z: Order) =>
          z.transhipmentStatus !== 'direct' &&
          z.transhipmentStatus !== 'unassigned' &&
          z.transhipmentStatus !== ''
      )
      if (isTranshipmentBag) {
        transhipment.push(y.group_name)
      }
    })
  })
  return transhipment
}

// for partner bagged get unassigned bags
export const getUnassignedBagsTemp = (data: Unbagged[]) => {
  const temp = ref(getUnassignedBags(data))
  return temp
}

// for partner bagged get assigned bags
export const getAssignedBagsTemp = (data: Unbagged[]) => {
  const temp = ref(removeUnassignedBags(data))
  return temp
}

// for partner unbagged get transhipment orders
export const getTranshipmentBagsTemp = (data: Unbagged[]) => {
  const temp = ref(getTranshipmentBags(data))
  return temp
}

// ** for bagged tab - end
