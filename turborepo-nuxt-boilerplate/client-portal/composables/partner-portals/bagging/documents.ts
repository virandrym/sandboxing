import { ref, useContext } from '@nuxtjs/composition-api'
import { AxiosResponse } from 'axios'
import { initDocuments } from '~/composables/clients/orders/b2b/form'
import {
  MawbAttachDocuments,
  MawbDocumentsResponse,
} from '~/types/partnerPortals/documents'
import {
  PresignedURLs,
  PresignedURLsFeatureType,
  PresignedURLsPayload,
  UploadDocuments,
} from '@/types/presigned-urls'

import { FileCategorized } from '~/types/base/filePond'

export type MawbDocuments = ReturnType<typeof initDocuments>

export const initUploadDocumentsForm = () => ({
  documents: [] as MawbDocuments[],
})

//  initialization types - start
export type UploadDocumentsForm = ReturnType<typeof initUploadDocumentsForm>

export interface UploadDocumentsFormData {
  uploadDocuments: UploadDocumentsForm
}
//  initialization types - end

//  ref of form data
export const initUploadData = () => {
  return ref<UploadDocumentsFormData>({
    uploadDocuments: initUploadDocumentsForm(),
  })
}

//  for options fields item - start
export const manifestDocumentCategories = [
  {
    text: 'Commercial Invoice & Packing List',
    value: 'CIPL',
  },
  {
    text: 'Master Airway Bill',
    value: 'MAWB',
  },
  {
    text: 'House Airway Bill',
    value: 'HAWB',
  },
  {
    text: 'Duties & Taxes',
    value: 'DNT',
  },
  {
    text: 'Others',
    value: 'OTHERS',
  },
]
//  for options fields item - end

export const doFormatPayload = {
  // for presigned urls mawb docs
  presignedUrlsMawbDocs: (
    id: string,
    documents: MawbDocuments[]
  ): {
    payload: PresignedURLsPayload[]
    featureType: PresignedURLsFeatureType
  } => {
    const featureType = {
      name: 'mawbs',
      id,
    }
    const payload: PresignedURLsPayload[] = documents.flatMap(
      ({ category, files }) =>
        files.map(({ name: fileName }) => ({ category, fileName }))
    )

    return {
      featureType,
      payload,
    }
  },
  // for attach mawb documents
  attachMAWBDocuments: (
    presignedPayloads: PresignedURLsPayload[],
    presignedURLsRes: AxiosResponse<PresignedURLs[], any>,
    files: File[]
  ): {
    attachDocumentsPayload: MawbAttachDocuments[]
    uploadPresignedPayload: UploadDocuments[]
  } => {
    const attachDocumentsPayload: MawbAttachDocuments[] = []
    const uploadPresignedPayload: UploadDocuments[] = []

    presignedPayloads.forEach((itemPayload) => {
      // find the path and url on the response of request presigned URL based on the filename property
      const presignedData = presignedURLsRes.data.find(
        (itemResponse) => itemResponse.fileName === itemPayload.fileName
      )
      const path = presignedData?.path
      const url = presignedData?.url
      if (!path || !url) return

      attachDocumentsPayload.push({ category: itemPayload.category, path })

      // filter the files based on the file name property
      const filesFiltered = files.reduce((accumulator: File, file: File) => {
        if (file.name.includes(itemPayload.fileName)) {
          accumulator = file
        }

        return accumulator
      }, {} as File)
      // add correlated path and file
      uploadPresignedPayload.push({
        url,
        file: filesFiltered,
      })
    })

    return {
      attachDocumentsPayload,
      uploadPresignedPayload,
    }
  },
}

// remap response for display
export const remapDocuments = (documents: MawbDocumentsResponse[]) => {
  if (!documents) return []

  return documents.reduce((acc, cur) => {
    const { category, path } = cur
    const existingCategory = acc.find((item) => item.category === category)

    const pathMeta = useContext().$customUtils.getPathMeta(path)

    const file = { name: pathMeta?.file || '' } as unknown as File

    /** grouping the files
     *
     *  if the files does not grouping yet, push the new one;
     *  else, files already exist, push into the new category
     **/
    if (existingCategory) {
      existingCategory.files.push(file)
    } else {
      acc.push({
        category,
        files: [file],
      })
    }

    return acc
  }, [] as FileCategorized[])
}
