import { checkHighValue } from '../scan'
import { getAssignedTemp } from './orderAssignment'
import {
  Unbagged,
  CompletedBag,
  InputManifest,
  Bagged,
  Order,
} from '~/types/bagging/bagging'

export interface OrderForGroupView extends Order {
  group_name: string
  new?: boolean
}

export function groupScannedUnbaggedOrder(data: Unbagged[], newScanned: any) {
  // get orders by assigned and transhipment with corrected group names
  const assigned = getAssignedTemp(data).value

  // check if order in newScanned matches assigned orders, and if it does, create a temp array containing these orders and the corrected group name
  const tempArr = newScanned.value.reduce((arr: Order[], k: Order) => {
    assigned.forEach((x) => {
      x.order_group.forEach((m) => {
        m.orders.forEach((n) => {
          if (
            k.order_code === n.order_code &&
            !arr.some((order: Order) => order.order_code === k.order_code)
          ) {
            const temp = {
              group_name: m.group_name,
              isHighValue: n.isHighValue ?? false,
              ...n,
              ...k,
            }
            arr.push(temp)
          }
        })
      })
    })
    return arr
  }, []) as OrderForGroupView[]

  // match orders in temp array back to assigned array with all other order details for display
  const groupOrder = assigned.map((x) => ({
    ...x,
    order_group: x.order_group
      .map((y) => ({
        ...y,
        orders: tempArr
          .filter((k) => k.group_name === y.group_name)
          .map((l) => ({
            ...l,
            id: l.id,
            order_code: l.order_code,
            new: l.new ?? false,
            isHighValue: l.isHighValue ?? false,
          })),
      }))
      .filter((b) => b.orders.length),
  }))

  return groupOrder
}

export function parsingBagged(data: Unbagged[]) {
  let temp = [] as any
  data.forEach((x) => {
    const remap = x.order_group.map((y) => {
      return {
        group_name: y.group_name.split('-')[0],
        dest_port: y.dest_port,
        dest_country: y.dest_country,
        origin_port: y.origin_port,
      }
    })
    temp.push(remap)
  })
  temp = [].concat.apply([], temp)
  let finalTemp = [] as any
  temp.forEach((x: any) => {
    if (!finalTemp.some((z: any) => z.group_name === x.group_name)) {
      finalTemp.push(x)
    }
  })
  let mergedAllOrderGroup = data.map((x) => x.order_group) as any
  mergedAllOrderGroup = [].concat.apply([], mergedAllOrderGroup)
  finalTemp = finalTemp.map((x: any) => {
    const temp = [] as any
    mergedAllOrderGroup.forEach((y: any) => {
      if (
        // y.group_name.includes(x.group_name)
        y.group_name.split('-')[0] === x.group_name
      ) {
        temp.push(y)
      }
    })
    return {
      ...x,
      order_group: temp,
    }
  })
  return finalTemp
}

export function processData(data: Unbagged[], newScanned: any) {
  let tempArr = [] as {
    group_name: string
    id: string
    order_code: string
    new?: boolean
  }[]
  data.forEach((x) => {
    newScanned.value.forEach((k: { order_code: string; new?: boolean }) => {
      let temp = {} as {
        group_name: string
        id: string
        order_code: string
        new?: boolean
      }
      x.order_group.forEach((m) => {
        if (k.order_code === m.group_name) {
          temp = {
            group_name: m.group_name,
            id: m.id,
            ...k,
          }
        }
      })
      tempArr.push(temp)
    })
  })
  tempArr = tempArr.filter((x) => Object.keys(x).length)
  const noOrder = [...data].map((x) => {
    return {
      ...x,
      order_group:
        x.order_group &&
        x.order_group
          .filter((k) => {
            return tempArr.some((j) => j.order_code === k.group_name)
          })
          .map((l: any) => {
            return {
              id: l.id,
              order_code: l.group_name,
              new:
                tempArr.some((j) => j.order_code === l.group_name && j.new) ??
                false,
            }
          }),
    }
  })
  return noOrder
}

export function weightCalc({
  data,
  isReweight,
}: {
  data: CompletedBag
  isReweight?: Boolean
}) {
  const keyWeight = isReweight ? 'measured_weight' : 'weight'
  return data.orders
    ? data.orders.reduce((accumulator, a: any) => accumulator + a[keyWeight], 0)
    : data.bags
    ? data.bags
        .map((x) => {
          return x.orders.reduce(
            (accumulator, a: any) => accumulator + a[keyWeight],
            0
          )
        })
        .reduce((accumulator, a) => accumulator + a, 0)
    : 0
}

export function parsingDataInputManifest(data: InputManifest) {
  const bagManifest = Object.entries(data.generateManifest).map((x) => {
    return {
      bag_id: x[0],
      length: Number(x[1].length),
      width: Number(x[1].width),
      height: Number(x[1].height),
      weight: Number(x[1].weight),
    }
  })
  const payload = {
    mawb: data.mawb,
    bag_manifest: bagManifest,
    originPortCode: data.originPortCode || '',
    destinationPortCode: data.destinationPortCode || '',
  }
  return payload
}

// check and store order codes of high value orders
export function findHighValueOrders(data: Unbagged[]) {
  const highValueOrders = [] as any
  data.forEach((x) => {
    x.order_group.forEach((y) => {
      y.orders.forEach((z) => {
        if (checkHighValue(z)) {
          highValueOrders.push(z.order_code)
        }
      })
    })
  })
  localStorage.setItem('highValueOrders', JSON.stringify(highValueOrders))
}

// check data for SIN destination, and split high and low value orders into separate bags
export function splitLowHighValueBags(data: Bagged[]) {
  // // create new temp array for high value orders by mapping all orders then filtering whether order is high value
  const tempHighValue = data.map((x) => ({
    ...x,
    orders: x.orders.filter((order) => checkHighValue(order)),
  })) as Bagged[]
  // create new temp array for low value orders by mapping all orders then filtering whether order is high value
  const tempLowValue = data.map((x) => ({
    ...x,
    orders: x.orders.filter((order) => !checkHighValue(order)),
  })) as Bagged[]

  // combine all arrays
  const tempSplitData = [...tempHighValue, ...tempLowValue]
  // remove bags that do not contain any orders
  const splitData = tempSplitData.filter((x) => x.orders?.length)
  return splitData
}

// for partner bagging generate Manifest
export function checkIfIsTranshipment(data: Bagged[]) {
  if (data && data.length) {
    let frontQuery
    // if there are only orders and no sub bags
    if (data[0].orders && data[0].orders.length) {
      frontQuery = data[0].orders[0]
    } else if (data[0].bags && data[0].bags.length) {
      frontQuery = data[0].bags[0].orders[0]
    }
    return !!(
      frontQuery &&
      // check that is transhipment order
      frontQuery.transhipmentStatus !== 'direct' &&
      frontQuery.transhipmentStatus !== 'unassigned' &&
      frontQuery.transhipmentStatus !== ''
    )
  }
  return false
}

// for bagging of transhipment orders, do not include partner name in bag name
export function setUnbaggedTranshipmentBagName(
  orderCode: String,
  transhipmentTemp: Unbagged[]
) {
  let foundGroupName = false as string | boolean
  transhipmentTemp.some((x: Unbagged) =>
    x.order_group.some((y: Bagged) => {
      const isTranshipment = y.orders.some((z) => z.order_code === orderCode)
      if (isTranshipment) {
        foundGroupName = y.group_name
        return foundGroupName
      }
      return false
    })
  )
  return foundGroupName || false
}
