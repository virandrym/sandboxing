export const initTableHeaders = (postfix?: string) => ({
  clientBagging: [
    {
      text: 'Bag ID',
      value: 'group_name',
      cellClass: 'text-no-wrap',
      width: 150,
    },
    {
      text: 'Orders',
      value: 'orderBagsCountClientBagging',
      width: 150,
    },
    {
      text: 'Status',
      value: 'status',
      sortable: false,
      cellClass: 'text-no-wrap',
    },
    {
      text: 'Origin Port',
      value: 'origin_port',
      sortable: false,
      width: 150,
    },
    {
      text: 'Destination Port',
      value: 'dest_port',
      sortable: false,
      width: 180,
    },
    {
      text: 'Flight',
      value: 'flight',
      sortable: false,
      cellClass: 'text-no-wrap',
    },
    {
      text: 'Date',
      value: 'flight_date',
      sortable: false,
      cellClass: 'text-no-wrap',
    },
    {
      text: 'Departure',
      value: 'departure',
      sortable: false,
      cellClass: 'text-no-wrap',
    },
    {
      text: 'Arrival',
      value: 'arrival',
      sortable: false,
      cellClass: 'text-no-wrap',
    },
    {
      text: '',
      value: 'actions',
      sortable: false,
    },
  ],
  partnerBagging: {
    completeTab: [
      {
        text: 'Bag ID',
        value: 'group_name',
        cellClass: 'text-no-wrap',
        width: 150,
      },
      {
        text: 'Orders',
        value: 'orderBagsCount' + postfix,
        width: 150,
      },
      {
        text: 'Sub-Bags',
        value: 'subBagsCount' + postfix,
        width: 150,
      },
      {
        text: 'Origin Port',
        value: 'origin_port',
        sortable: false,
        width: 150,
      },
      {
        text: 'Destination Port',
        value: 'dest_port',
        sortable: false,
        width: 180,
      },
      {
        text: 'Weight',
        value: 'weightComputed' + postfix,
        sortable: false,
        width: 150,
      },
      {
        text: '',
        value: 'sealBags',
        sortable: false,
      },
      {
        text: '',
        value: 'actions',
        sortable: false,
      },
    ],
    manifestTab: {
      default: [
        {
          text: 'MAWB No.',
          value: 'mawb',
          cellClass: 'text-no-wrap',
          width: 150,
        },
        {
          text: 'Flight No.',
          value: 'flight_no',
          cellClass: 'text-no-wrap',
          width: 150,
        },
        {
          text: 'Date',
          value: 'flightDate' + postfix,
          sortable: false,
          cellClass: 'text-no-wrap',
        },
        {
          text: 'Origin Port',
          value: 'originPort' + postfix,
          sortable: false,
          width: 150,
        },
        {
          text: 'Destination Port',
          value: 'destPort' + postfix,
          sortable: false,
          width: 180,
        },
        {
          text: 'Bags',
          value: 'bagsCount' + postfix,
          sortable: false,
          width: 150,
        },
        {
          text: 'Sub-Bags',
          value: 'subBagsCount' + postfix,
          sortable: false,
          width: 150,
        },
        {
          text: 'Orders',
          value: 'subBagsOrderCount' + postfix,
          sortable: false,
          width: 150,
        },
        {
          text: 'Weight',
          value: 'bagsWeight' + postfix,
          sortable: false,
          width: 150,
        },
        {
          text: 'Status',
          value: 'manifest_status',
          sortable: false,
        },
        {
          text: '',
          value: 'actions',
          sortable: false,
        },
      ],
      uploadDocumentsFlag: [
        {
          text: 'MAWB No.',
          value: 'mawbNumber',
          cellClass: 'text-no-wrap',
          width: 150,
        },
        {
          text: 'Flight No.',
          value: 'flightNo',
          cellClass: 'text-no-wrap',
          width: 150,
        },
        {
          text: 'Date',
          value: 'flightDate' + postfix,
          sortable: false,
          cellClass: 'text-no-wrap',
        },
        {
          text: 'Origin Port',
          value: 'originPort' + postfix,
          sortable: false,
          width: 150,
        },
        {
          text: 'Destination Port',
          value: 'destPort' + postfix,
          sortable: false,
          width: 180,
        },
        {
          text: 'Bags',
          value: 'bagsCount' + postfix,
          sortable: false,
          width: 150,
        },
        {
          text: 'Sub-Bags',
          value: 'subBagsCount' + postfix,
          sortable: false,
          width: 150,
        },
        {
          text: 'Orders',
          value: 'subBagsOrderCount' + postfix,
          sortable: false,
          width: 150,
        },
        {
          text: 'Weight',
          value: 'bagsWeight' + postfix,
          sortable: false,
          width: 150,
        },
        {
          text: 'Status',
          value: 'manifestStatus',
          sortable: false,
        },
        {
          text: '',
          value: 'actions',
          sortable: false,
        },
      ],
    },
  },
})
