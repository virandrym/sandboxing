// types
import { PortData } from '~/types/filters'

/** list of the available ports in the destination ports */
export const availableDestPortsB2B = {
  // based on the origin port :
  SIN: [
    'CGK',
    'KUL',
    'MNL',
    'BKK',
    'HKG',
    'SGN',
    'SYD',
    'TPE',
    'AKL',
    'LHR',
    'LAX',
    'AMS',
    'BWN',
    'ICN',
    'NRT',
    'RUH',
  ],
  HKG: [
    'CGK',
    'KUL',
    'MNL',
    'BKK',
    'SIN',
    'SGN',
    'LHR',
    'JFK',
    'LAX',
    'SYD',
    'MEL',
    'AKL',
    'NRT',
    'RUH',
    'ICN',
    'AMS',
    'TPE',
  ],
  KUL: [
    'TPE',
    'SIN',
    'BKK',
    'HKG',
    'CGK',
    'SGN',
    'MNL',
    'SYD',
    'BWN',
    'LAX',
    'NRT',
    'ICN',
    'RUH',
    'AMS',
    'LHR',
  ],
  CGK: [
    'TPE',
    'SIN',
    'BKK',
    'HKG',
    'KUL',
    'SGN',
    'MNL',
    'SYD',
    'BWN',
    'LAX',
    'NRT',
    'ICN',
    'RUH',
    'AMS',
    'LHR',
  ],
  // based on the incoterm
  CIP: [
    'CGK',
    'KUL',
    'MNL',
    'BKK',
    'HKG',
    'SGN',
    'SYD',
    'TPE',
    'AKL',
    'LHR',
    'LAX',
    'AMS',
    'BWN',
    'ICN',
    'NRT',
    'RUH',
    'SIN',
    'JFK',
    'MEL',
  ],
}
export type DestPortRef = keyof typeof availableDestPortsB2B

/**
 * @function restrictDestPort : restricted the destination ports
 * @param ports the list of ports from API
 * @param references the string for references to get the @availableDestPortsB2B object
 */
export const restrictDestPort = (
  ports: PortData[],
  references: DestPortRef
): PortData[] => {
  const isValidDestRef =
    // based on the origin port
    references === 'SIN' ||
    references === 'HKG' ||
    references === 'KUL' ||
    references === 'CGK' ||
    // based on the incoterm
    references === 'CIP'

  if (!isValidDestRef) return [...ports]

  return (
    ports?.filter((port) =>
      availableDestPortsB2B[references].some(
        (item) => item.toLowerCase() === port.code.toLowerCase()
      )
    ) || []
  )
}
