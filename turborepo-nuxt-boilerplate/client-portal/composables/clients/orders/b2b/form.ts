import { ref } from '@nuxtjs/composition-api'
import { AxiosResponse } from 'axios'
import type {
  B2BAttachDocuments,
  B2BPackagingType,
  B2BPostPayload,
  B2BScope,
  B2BShipmentMode,
  B2BQuotationFormData,
  B2BAttachRfqToOrder,
} from '~/types/orders/b2b'
import type {
  PresignedURLs,
  PresignedURLsFeatureType,
  PresignedURLsPayload,
  UploadDocuments,
} from '@/types/presigned-urls'
import type {
  ServiceableB2BLanes,
  CountryCode,
  PortData,
} from '~/types/filters'

//  **initialization variable - start
//  for options fields item - start
export const b2bIncotermInit = [
  {
    code: 'EXW',
    description: 'Origin Pickup + Freight + Destination Customs + Delivery',
  },
  {
    code: 'FOB',
    description: 'Freight + Destination Customs + Delivery',
  },
  {
    code: 'DDP',
    description: 'Origin Pickup + Freight + Destination Customs + Delivery',
  },
  {
    code: 'DAP',
    description: 'Origin Pickup + Freight + Delivery',
  },
  {
    code: 'CIP',
    description: 'Destination Customs + Delivery only',
  },
  {
    code: 'ALL-IN',
    description: 'Freight + Destination Customs + Delivery',
  },
]
export const b2bPackagingTypeInit = ['Cartons', 'Pallets', 'Gunny Bags']
export const b2bDocumentCategories = [
  {
    text: 'Commercial Invoice',
    value: 'COMMERCIAL_INVOICE',
  },
  {
    text: 'Packing List',
    value: 'PACKING_LIST',
  },
  {
    text: 'Material Safety Data Sheet (MSDS)',
    value: 'MATERIAL_SAFETY_DATA_SHEET',
  },
  {
    text: 'Surveyor Report',
    value: 'SURVEYOR_REPORT',
  },
  {
    text: 'Health Permit',
    value: 'HEALTH_PERMIT',
  },
  {
    text: 'Others',
    value: 'OTHERS',
  },
]
//  for options fields item - end

//  for basic info - start
const initBasicInfo = () => ({
  destinationCountry: '',
  destinationPort: {} as PortData,
  originCountry: '',
  originPort: {} as PortData,
  shipmentMode: 'air' as B2BShipmentMode,
  scope: '' as B2BScope,
  cargoReadinessDate: '',
})
//  for basic info - end

//  for sender details - start
const initSenderDetails = () => ({
  senderName: '',
  senderPhoneNumber: null as number | null,
  senderEmail: '',
  senderDistrict: '',
  senderProvince: '',
  senderCity: '',
  senderPostal: null as number | null,
  senderAddress: '',
})
//  for sender details - end

//  for consignee details - start
const initConsigneeDetails = () => ({
  consigneeName: '',
  consigneePhoneNumber: null as number | null,
  consigneeEmail: '',
  consigneeDistrict: '',
  consigneeProvince: '',
  consigneeCity: '',
  consigneePostal: null as number | null,
  consigneeAddress: '',
})
//  for consignee details - end

//  for package details - start
export const initParcelItems = (packagingType = '' as B2BPackagingType) => ({
  packagingType,
  length: null as number | null,
  width: null as number | null,
  height: null as number | null,
  weight: null as number | null,
})
const initPackageDetails = () => ({
  quantity: null as number | null,
  parcels: [{ ...initParcelItems() }],
})
//  for package details - end

//  for items details - start
export const initOrderItems = () => ({
  category: '',
  productDescription: '',
  quantity: null as number | null,
  price: null as number | null,
  currency: '',
})
export const initItemsDetails = () => ({
  dangerousGoods: false,
  items: [{ ...initOrderItems() }],
})
//  for items details - end

//  for additional form - start
export const initDocuments = (category = '') => ({
  category,
  files: [] as File[],
})
export type B2BDocumentsForm = ReturnType<typeof initDocuments>
export const initAdditionalsForm = () => ({
  documents: [] as B2BDocumentsForm[],
})
//  for additional form - end

//  for estimated price - start
export const initEstimatedPriceForm = () => ({
  estimatedShipmentLoad: null as number | null,
})
export const initEstimatedPriceData = () => ({
  getPriceEstimate: false as Boolean,
  estimatedPrice: null as number | null,
  rate: null as number | null,
  currency: '' as string,
})
//  for estimated price - end

//  **initialization variable - end

//  initialization types - start
export type B2BBasicInfoForm = ReturnType<typeof initBasicInfo>
export type B2BSenderForm = ReturnType<typeof initSenderDetails>
export type B2BConsigneeForm = ReturnType<typeof initConsigneeDetails>
export type B2BPackageDetailsForm = ReturnType<typeof initPackageDetails>
export type B2BItemDetailsForm = ReturnType<typeof initItemsDetails>
export type B2BAdditionalsForm = ReturnType<typeof initAdditionalsForm>
export type B2BEstimatedPriceForm = ReturnType<typeof initEstimatedPriceForm>
export type B2BEstimatedPriceData = ReturnType<typeof initEstimatedPriceData>
export interface B2BFormData {
  basicInfo: B2BBasicInfoForm
  senderDetails: B2BSenderForm
  consigneeDetails: B2BConsigneeForm
  packageDetails: B2BPackageDetailsForm
  b2bItems: B2BItemDetailsForm
  b2bAdditionals: B2BAdditionalsForm
}
//  initialization types - end

//  ref of form data
export const initFormData = () => {
  return ref<B2BFormData>({
    basicInfo: { ...initBasicInfo() },
    senderDetails: { ...initSenderDetails() },
    consigneeDetails: { ...initConsigneeDetails() },
    packageDetails: { ...initPackageDetails() },
    b2bItems: { ...initItemsDetails() },
    b2bAdditionals: { ...initAdditionalsForm() },
  })
}

export const doFormatPayload = {
  // for submit b2b orders
  b2bSubmission: (data: B2BFormData): B2BPostPayload => {
    const {
      basicInfo,
      senderDetails,
      consigneeDetails,
      packageDetails,
      b2bItems,
    } = data

    // *format the basic info payload
    const {
      originPort,
      destinationPort,
      cargoReadinessDate,
      ...restBasicInfo
    } = basicInfo
    const formattedBasicInfo = {
      ...restBasicInfo,
      originPortID: originPort?.id || '',
      destinationPortID: destinationPort.id,
      cargoReadinessDate: new Date(cargoReadinessDate),
    }
    // *format the consignee details
    const { consigneePhoneNumber, consigneePostal, ...restConsigneeDetails } =
      consigneeDetails
    const formattedConsigneeDetails = {
      ...restConsigneeDetails,
      consigneePhoneNumber: consigneePhoneNumber?.toString() || '',
      consigneePostal: consigneePostal?.toString() || '0',
    }
    // *format the sender details
    const { senderPhoneNumber, senderPostal, ...restSenderDetails } =
      senderDetails
    const formattedSenderDetails = {
      ...restSenderDetails,
      senderPhoneNumber: senderPhoneNumber?.toString() || '',
      senderPostal: senderPostal?.toString() || '0',
    }
    // *format the package details
    const {
      parcels,
      quantity: quantityPackage,
      ...restPackageDetails
    } = packageDetails
    const formattedParcels = parcels.map(
      ({ height, length, weight, width, packagingType }) => ({
        packagingType,
        height: Number(height) || 0,
        length: Number(length) || 0,
        weight: Number(weight) || 0,
        width: Number(width) || 0,
      })
    )
    const formattedPackageDetails = {
      ...restPackageDetails,
      quantity: Number(quantityPackage) || 0,
      parcels: [...formattedParcels],
    }
    // *format the b2b order items
    const { items: orderItems, ...restOrderItems } = b2bItems
    const orderItem = orderItems.map(({ quantity, ...item }) => ({
      ...item,
      quantity: Number(quantity) || 0,
    }))
    const formattedItemDetails = {
      ...restOrderItems,
      items: [...orderItem],
    }

    /** merge the formatted payload */
    return {
      ...formattedBasicInfo,
      ...formattedConsigneeDetails,
      ...formattedSenderDetails,
      ...formattedPackageDetails,
      ...formattedItemDetails,
    }
  },
  // for presigned urls b2b docs
  presignedUrlsB2BDocs: (
    id: string,
    documents: B2BDocumentsForm[]
  ): {
    payload: PresignedURLsPayload[]
    featureType: PresignedURLsFeatureType
  } => {
    const featureType = {
      name: 'b2bs',
      id,
    }
    const payload: PresignedURLsPayload[] = documents.flatMap(
      ({ category, files }) =>
        files.map(({ name: fileName }) => ({ category, fileName }))
    )

    return {
      featureType,
      payload,
    }
  },
  // for attach b2b documents
  attachB2BDocuments: (
    presignedPayloads: PresignedURLsPayload[],
    presignedURLsRes: AxiosResponse<PresignedURLs[], any>,
    files: File[]
  ): {
    attachDocumentsPayload: B2BAttachDocuments
    uploadPresignedPayload: UploadDocuments[]
  } => {
    const attachDocumentsPayload: B2BAttachDocuments = {}
    const uploadPresignedPayload: UploadDocuments[] = []

    presignedPayloads.forEach((itemPayload) => {
      // find the path and url on the response of request presigned URL based on the filename property
      const presignedData = presignedURLsRes.data.find(
        (itemResponse) => itemResponse.fileName === itemPayload.fileName
      )
      const path = presignedData?.path
      const url = presignedData?.url
      if (!path || !url) return

      // if the category property is not yet in the attachDocumentsPayload object, create an empty array for it
      if (!attachDocumentsPayload[itemPayload.category]) {
        attachDocumentsPayload[itemPayload.category] = []
      }
      // when category property are exist, add the path to the array associated with the category property
      attachDocumentsPayload[itemPayload.category].push(path)

      // filter the files based on the file name property
      const filesFiltered = files.reduce((accumulator: File, file: File) => {
        if (file.name.includes(itemPayload.fileName)) {
          accumulator = file
        }

        return accumulator
      }, {} as File)
      // add correlated path and file
      uploadPresignedPayload.push({
        url,
        file: filesFiltered,
      })
    })

    return {
      attachDocumentsPayload,
      uploadPresignedPayload,
    }
  },
  // b2b estimated price

  // for submit rfq
  rfqSubmission: (
    estimatedPrice: B2BEstimatedPriceForm,
    basicInfo: B2BBasicInfoForm
  ): B2BQuotationFormData => {
    return {
      shipmentMode: basicInfo.shipmentMode.toUpperCase(),
      originPortID: basicInfo.originPort.id,
      destinationPortID: basicInfo.destinationPort.id,
      incoterm: basicInfo.scope.toUpperCase(),
      estimatedWeight: estimatedPrice.estimatedShipmentLoad ?? 0,
    }
  },

  // for attach rfq to order
  attachRfqToOrder: (id: string): B2BAttachRfqToOrder => {
    return {
      b2bOrderID: id,
    }
  },
}

export const assignServiceableB2BCountriesAndPorts = (
  serviceableLanes: ServiceableB2BLanes[],
  formDataOrigin?: PortData | 'CIP'
) => {
  // if incoterm is CIP, originPort in serviceable lanes will be null
  return serviceableLanes.reduce(
    (result, lane) => {
      result.originCountry.push({
        name: lane.originPort?.countryName,
        value: lane.originPort?.countryCode,
      })

      const originPortData = {
        code: lane.originPort?.code,
        countryCode: lane.originPort?.countryCode,
        id: lane.originPort?.id,
        name: lane.originPort?.name,
      }

      result.originPort.push(originPortData)

      // filter destination based on origin input
      if (
        formDataOrigin &&
        (formDataOrigin === 'CIP' ||
          JSON.stringify(formDataOrigin) === JSON.stringify(originPortData))
      ) {
        result.destinationCountry.push({
          name: lane.destinationPort.countryName,
          value: lane.destinationPort.countryCode,
        })
        result.destinationPort.push({
          code: lane.destinationPort.code,
          countryCode: lane.destinationPort.countryCode,
          id: lane.destinationPort.id,
          name: lane.destinationPort.name,
        })
      }

      return result
    },
    {
      originCountry: [] as CountryCode[],
      destinationCountry: [] as CountryCode[],
      originPort: [] as PortData[],
      destinationPort: [] as PortData[],
    }
  )
}
