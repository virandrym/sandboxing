// types
import { CountryCode } from '~/types/filters'

/** list of origin & destination countries available
 * @const availableCountriesOriginB2B for origin
 * @const availableCountriesDestinationB2B for destination
 */
export const availableCountriesOriginB2B = {
  default: ['SINGAPORE', 'MALAYSIA', 'INDONESIA', 'HONG KONG'],
  'ALL-IN': [
    'SINGAPORE',
    'MALAYSIA',
    'INDONESIA',
    'THAILAND',
    'TAIWAN',
    'HONG KONG',
    'CHINA',
    'SOUTH KOREA',
    'JAPAN',
    'UNITED STATES',
  ],
}
export const availableCountriesDestinationB2B: { [key: string]: string[] } = {
  // based on the origin country :
  SINGAPORE: [
    'INDONESIA',
    'MALAYSIA',
    'PHILIPPINES',
    'THAILAND',
    'HONG KONG',
    'VIETNAM',
    'AUSTRALIA',
    'TAIWAN',
    'NEW ZEALAND',
    'UNITED KINGDOM',
    'UNITED STATES',
    'EUROPE',
    'BRUNEI DARUSSALAM',
    'KOREA',
    'JAPAN',
    'SAUDI ARABIA',
  ],
  'HONG KONG': [
    'INDONESIA',
    'MALAYSIA',
    'PHILIPPINES',
    'THAILAND',
    'SINGAPORE',
    'VIETNAM',
    'UNITED KINGDOM',
    'UNITED STATES',
    'AUSTRALIA',
    'NEW ZEALAND',
    'JAPAN',
    'SAUDI ARABIA',
    'KOREA',
    'EUROPE',
    'TAIWAN',
  ],
  MALAYSIA: [
    'TAIWAN',
    'SINGAPORE',
    'THAILAND',
    'HONG KONG',
    'INDONESIA',
    'VIETNAM',
    'PHILIPPINES',
    'AUSTRALIA',
    'BRUNEI DARUSSALAM',
    'UNITED STATES',
    'JAPAN',
    'KOREA',
    'SAUDI ARABIA',
    'EUROPE',
    'UNITED KINGDOM',
  ],
  INDONESIA: [
    'TAIWAN',
    'SINGAPORE',
    'THAILAND',
    'HONG KONG',
    'MALAYSIA',
    'VIETNAM',
    'PHILIPPINES',
    'AUSTRALIA',
    'BRUNEI DARUSSALAM',
    'UNITED STATES',
    'JAPAN',
    'KOREA',
    'SAUDI ARABIA',
    'EUROPE',
    'UNITED KINGDOM',
  ],
  // based on the incoterm
  CIP: [
    'INDONESIA',
    'MALAYSIA',
    'PHILIPPINES',
    'THAILAND',
    'HONG KONG',
    'VIETNAM',
    'AUSTRALIA',
    'TAIWAN',
    'NEW ZEALAND',
    'UNITED KINGDOM',
    'UNITED STATES',
    'EUROPE',
    'BRUNEI DARUSSALAM',
    'KOREA',
    'JAPAN',
    'SAUDI ARABIA',
    'SINGAPORE',
    'UNITED STATES',
    'AUSTRALIA',
  ],
  'ALL-IN-SINGAPORE': ['INDONESIA', 'PHILIPPINES', 'TAIWAN'],
  'ALL-IN-MALAYSIA': ['INDONESIA', 'PHILIPPINES', 'TAIWAN'],
  'ALL-IN-INDONESIA': ['PHILIPPINES', 'TAIWAN'],
  'ALL-IN-THAILAND': ['INDONESIA'],
  'ALL-IN-TAIWAN': ['INDONESIA', 'PHILIPPINES', 'THAILAND', 'VIETNAM'],
  'ALL-IN-HONG KONG': [
    'INDONESIA',
    'PHILIPPINES',
    'TAIWAN',
    'THAILAND',
    'VIETNAM',
  ],
  'ALL-IN-CHINA': ['INDONESIA'],
  'ALL-IN-SOUTH KOREA': ['INDONESIA'],
  'ALL-IN-JAPAN': ['INDONESIA'],
  'ALL-IN-UNITED STATES': ['INDONESIA'],
}

export type DestCountryRef = keyof typeof availableCountriesDestinationB2B

/**
 * @function restrictCountries : restricted the origin & destination countries
 * for origin will return the restricted origin countries
 * for destination will return the restricted destination countries based on origin country selected
 * @param type set the city that want to restrict, can be origin or destination country
 * @param countries the list of countries from API
 * @param destReferences the string for references to get the @availableCountriesDestinationB2B object
 */
export const restrictCountries = (
  type: 'origin' | 'destination',
  countries: CountryCode[],
  originReference: 'ALL-IN' | '',
  destReferences: DestCountryRef
): CountryCode[] => {
  if (!type) return []

  const isValidDestRef =
    // based on the incoterm
    destReferences === 'CIP' ||
    // based on the origin countries
    destReferences === 'INDONESIA' ||
    destReferences === 'MALAYSIA' ||
    destReferences === 'HONG KONG' ||
    destReferences === 'SINGAPORE' ||
    (originReference === 'ALL-IN' && destReferences === 'THAILAND') ||
    (originReference === 'ALL-IN' && destReferences === 'TAIWAN') ||
    (originReference === 'ALL-IN' && destReferences === 'CHINA') ||
    (originReference === 'ALL-IN' && destReferences === 'SOUTH KOREA') ||
    (originReference === 'ALL-IN' && destReferences === 'JAPAN') ||
    (originReference === 'ALL-IN' && destReferences === 'UNITED STATES')

  /**
   * return the default value
   * when it's for ori country & the incoterm is CIP,
   * and when it's for the dest country & isValidDestRef
   **/
  if (
    (type === 'origin' && destReferences === 'CIP') ||
    (type === 'destination' && !isValidDestRef)
  ) {
    return [...countries]
  }

  // get the list of the available countries as origin
  let availableCountries = [...availableCountriesOriginB2B.default]

  // if incoterm is ALL-IN, filter selected origin countries only
  if (type === 'origin' && originReference === 'ALL-IN') {
    availableCountries = [...availableCountriesOriginB2B[originReference]]
  }

  // if incoterm is ALL-IN, filter dest country by origin country, otherwise filter by dest references
  if (type === 'destination') {
    originReference === 'ALL-IN'
      ? (availableCountries =
          availableCountriesDestinationB2B['ALL-IN-' + destReferences])
      : (availableCountries = [
          ...availableCountriesDestinationB2B[destReferences],
        ])
  }
  // filter the countries from API based on the available list
  return [
    ...countries.filter((country) =>
      availableCountries.some(
        (availCountry) =>
          availCountry.toLowerCase() === country.name.toLowerCase()
      )
    ),
  ]
}
