import { ref } from '@nuxtjs/composition-api'
import { useChangeCase } from '@vueuse/integrations/useChangeCase'
import { doValidate } from '~/composables/clients/orders/upload-orders/validation'

// types - start
import type {
  OrderCrossBorder,
  OrderDomestic,
  OrderItemUpload,
  OrdersMultipleSheet,
  OrdersSingleSheet,
  UploadOrdersScopeType,
} from '~/types/orders/upload-submission'
// types - end

export const errorMessages = ref<string[]>([])
export const doFormat = {
  /**
   * To format the cell header from .xlsx file to the object key name as 'camelCase'
   *
   * @param data {OrdersMultipleSheet | OrdersSingleSheet | OrderItemUpload}
   *
   * @return {OrdersMultipleSheet | OrdersSingleSheet | OrderItemUpload}
   */
  payloadKey: <
    T extends OrdersMultipleSheet | OrdersSingleSheet | OrderItemUpload
  >(
    data: T
  ): T => {
    const payload = {} as T

    for (const [key, value] of Object.entries(data)) {
      const formatKey = useChangeCase(key, 'camelCase')

      /** format some cell header to custom value **/
      // cross-border orders file
      if (formatKey.value === 'customBrokerages') {
        formatKey.value = 'customs'
      }
      if (formatKey.value === 'lmLControlBypass') {
        formatKey.value = 'lmPartnerCode'
      }
      if (formatKey.value === 'selfCollectPudo') {
        formatKey.value = 'isSelfCollectDropOff'
      }
      if (formatKey.value === 'shipmentIncoterm') {
        formatKey.value = 'incoterm'
      }
      // domestic orders file
      if (formatKey.value === 'partnerCode') {
        formatKey.value = 'domesticPartnerCode'
      }

      payload[formatKey.value as keyof T] = value
    }

    return payload
  },
  /**
   * formatting the payload for order items
   *
   * @param order {OrdersMultipleSheet | OrdersSingleSheet}
   * @param orderItems {OrderItemUpload[]}
   *
   * @returns {OrderItemUpload[]}
   */
  orderItems: <T extends OrdersMultipleSheet | OrdersSingleSheet>(
    order: T,
    orderItems: OrderItemUpload[]
  ): OrderItemUpload[] =>
    orderItems
      .filter((item) => order.orderCode === item.orderCode)
      .map((item) => {
        const { orderCode, sku, productCode, quantity, price } = item

        return {
          ...item,
          orderCode: String(orderCode || ''),
          sku: String(sku || ''),
          productCode: String(productCode || ''),
          quantity: parseInt(String(quantity)) || 0,
          price: parseFloat(String(price)) || 0,
        }
      }),
  /**
   * formatting the payload for orders
   *
   * @param order {OrdersMultipleSheet | OrdersSingleSheet}
   * @param orderItems {OrderItemUpload[]}
   * @param uploadOrdersScope {UploadOrdersScopeType}
   *
   * @returns {OrdersMultipleSheet | OrdersSingleSheet}
   */
  orders: <T extends OrdersMultipleSheet | OrdersSingleSheet>(
    order: T,
    orderItems: OrderItemUpload[],
    uploadOrdersScope: UploadOrdersScopeType
  ): T => {
    const strToBool = window.$nuxt.$customUtils.strToBool
    const {
      orderCode,
      codValue,
      codCurrency,
      paymentType,
      length,
      width,
      weight,
      height,
      consigneePostal,
      consigneeNumber,
      consigneeTaxId,
      consigneeDistrict,
      senderPostal,
      senderContactNumber,
      senderDistrict,
      insurance,
    } = order
    const isCOD = paymentType === 'cod'
    let data: T = {
      ...order,
      orderCode: String(orderCode || ''),
      items: orderItems,
      codValue: isCOD && codValue ? parseFloat(String(codValue)) : 0,
      codCurrency: isCOD ? codCurrency : '',
      length: parseFloat(String(length)) || 0,
      width: parseFloat(String(width)) || 0,
      height: parseFloat(String(height)) || 0,
      weight: parseFloat(String(weight)) || 0,
      consigneePostal: String(consigneePostal || ''),
      consigneeNumber: String(consigneeNumber || ''),
      consigneeTaxId: String(consigneeTaxId || ''),
      consigneeState: String(consigneeDistrict || ''),
      senderPostal: String(senderPostal || ''),
      senderContactNumber: String(senderContactNumber || ''),
      senderState: String(senderDistrict || ''),
      insurance: strToBool(insurance),
    }

    // additional payload for domestic orders
    if (uploadOrdersScope === 'domestic') {
      const { domesticPartnerCode } = order as OrderDomestic

      data = {
        ...data,
        domesticPartnerCode: String(domesticPartnerCode || ''),
      }
    }

    // additional payload for cross-borders orders
    if (uploadOrdersScope === 'cross border') {
      const {
        firstMile,
        lastMile,
        freightForwarder,
        customs,
        isSelfCollectDropOff,
      } = order as OrderCrossBorder

      data = {
        ...data,
        firstMile: strToBool(firstMile),
        lastMile: strToBool(lastMile),
        freightForwarder: strToBool(freightForwarder),
        customs: strToBool(customs),
        isSelfCollectDropOff: strToBool(isSelfCollectDropOff),
      }
    }

    return data
  },
}
/**
 * Merging the of orders to include the order items
 *
 * @param orders {(OrdersMultipleSheet | OrdersSingleSheet)[]}
 * @param orderItems {OrderItemUpload[]}
 * @param uploadOrdersScope {UploadOrdersScopeType}
 *
 * @return {
 *  { isError: boolean;orders: (OrderCrossBorder | OrderDomestic)[] }
 * }
 */
export const doMergeOrders = ({
  orders,
  orderItems,
  uploadOrdersScope,
}: {
  orders: OrdersMultipleSheet[]
  orderItems: OrderItemUpload[]
  uploadOrdersScope: UploadOrdersScopeType
}): { isError: boolean; orders: OrdersMultipleSheet[] } => {
  const ordersMerged: OrdersMultipleSheet[] = []
  orders.forEach((order) => {
    const formattedOrderItems = doFormat.orderItems(order, orderItems)
    const formattedOrders = doFormat.orders(
      order,
      formattedOrderItems,
      uploadOrdersScope
    )

    // validate the orders based on the data types
    errorMessages.value = [
      ...errorMessages.value,
      ...doValidate.dataTypes(formattedOrders, uploadOrdersScope),
    ]

    ordersMerged.push(formattedOrders)
  })

  return {
    isError: !!errorMessages.value.length,
    orders: ordersMerged,
  }
}
/**
 * On single sheet file,
 * the orders data need to be separated first into orders and order items
 * the goal is to make the data look the same as the raw data on multiple sheets
 *
 * @param orders {OrdersSingleSheet[]}
 *
 * @returns {{ orders: OrdersMultipleSheet[], orderItems: OrderItemUpload[]}}
 */
export const formatToMultipleSheet = (orders: OrdersSingleSheet[]) => {
  const handleSeparateOrders = (type: 'orders' | 'items') =>
    orders.map(
      ({
        orderCode,
        description,
        quantity,
        productCode,
        sku,
        category,
        price,
        currency,
        ...order
      }) => {
        if (type === 'orders')
          return { orderCode, items: [], ...order } as OrdersMultipleSheet

        return {
          orderCode,
          description,
          quantity,
          productCode,
          sku,
          category,
          price,
          currency,
        } as OrderItemUpload
      }
    )
  const multipleSheetOrders = [
    ...new Map(
      handleSeparateOrders('orders').map((order) => [order.orderCode, order])
    ).values(),
  ] as OrdersMultipleSheet[]
  const multipleSheetOrderItems = handleSeparateOrders(
    'items'
  ) as OrderItemUpload[]

  return {
    orders: multipleSheetOrders,
    orderItems: multipleSheetOrderItems,
  }
}
