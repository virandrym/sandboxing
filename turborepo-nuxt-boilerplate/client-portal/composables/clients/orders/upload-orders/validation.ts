import { errorMessages } from '~/composables/clients/orders/upload-orders/merge'

// types - start
import type {
  OrderItemUpload,
  OrdersMultipleSheet,
  OrdersSingleSheet,
  UploadOrdersScopeType,
  UploadSheetType,
} from '~/types/orders/upload-submission'
// types - end

// *getting the error messages based on data types validation
export const getErrorMessage = {
  duplicateOrderCodes: <T extends OrdersMultipleSheet | OrdersSingleSheet>(
    orders: T[]
  ) => {
    const orderCodes = [
      ...new Set(orders.map((order) => order.orderCode)),
    ].join(', ')

    return [`order code ${orderCodes} is duplicated!`]
  },
  isRequired: (
    source: OrdersMultipleSheet,
    key: string,
    data: OrdersMultipleSheet | OrderItemUpload,
    onOrderItems = false
  ) => {
    if ((data as unknown as Record<string, string>)[key]) return ''

    return `${key.charAt(0).toUpperCase() + key.slice(1)} ${
      onOrderItems ? 'order item' : ''
    } on ${source.orderCode}  is required!`
  },
  isValidDataType: (
    source: OrdersMultipleSheet,
    key: string,
    data: OrdersMultipleSheet | OrderItemUpload,
    type: 'string' | 'number',
    onOrderItems = false
  ) => {
    if (!type) return ''
    if (
      !(data as unknown as Record<string, string>)[key] ||
      // eslint-disable-next-line valid-typeof
      typeof (data as unknown as Record<string, string>)[key] === type
    )
      return ''

    const messageType =
      type === 'string' ? 'alphabetical' : type === 'number' ? 'numeric' : ''

    return `${key.charAt(0).toUpperCase() + key.slice(1)} ${
      onOrderItems ? 'order item' : ''
    } on ${source.orderCode} must be ${messageType}!`
  },
  isValidEmail: (
    source: OrdersMultipleSheet,
    key: string,
    data: OrdersMultipleSheet,
    onOrderItems = false
  ) => {
    /**
     * The email validations patterns :
     *
     * - The email must start with one or more word characters, dots, or hyphens.
     * - Followed by the "@" symbol.
     * - Followed by one or more word characters, dots, or hyphens.
     * - Followed by a dot and one or more word characters.
     *
     */
    if (
      /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(
        (data as unknown as Record<string, string>)[key]
      )
    )
      return ''

    return `${key.charAt(0).toUpperCase() + key.slice(1)} ${
      onOrderItems ? 'order item' : ''
    } on ${source.orderCode} must be a valid email!`
  },
}

export const doValidate = {
  /**
   * validate the duplicate order codes
   * will return :
   *
   * isDuplicated : boolean from duplicated data of the orders
   * duplicatedData : the orders data that is duplicated
   * uniqueData : the unique data from orders data
   *
   * @returns { isDuplicated: boolean; duplicatedData: T[]; uniqueData: T[] }
   */
  duplicateOrderCodes: <T extends OrdersMultipleSheet | OrdersSingleSheet>(
    data: T[],
    sheetType: UploadSheetType
  ): { isDuplicated: boolean; duplicatedData: T[] } => {
    // get the duplicated orders
    const uniqueData: T[] = []
    const duplicatedData: T[] = []
    data.forEach((item) => {
      // check if the order is duplicated
      const isOrderDuplicated = uniqueData.some((unique) => {
        // for single sheet
        if (sheetType === 'single') {
          const propertiesToCompare = [
            'consigneeName',
            'consigneeNumber',
            'consigneeEmail',
            'consigneeAddress',
            'consigneePostal',
            'consigneeCity',
            'consigneeCountry',
            'consigneeProvince',
            'consigneeDistrict',
            'destPort',
            'consigneeTaxId',
            'length',
            'width',
            'height',
            'weight',
            'paymentType',
            'codValue',
            'codCurrency',
            'senderContactName',
            'senderContactNumber',
            'senderCity',
            'senderCountry',
            'senderProvince',
            'senderDistrict',
            'senderPort',
            'senderPostal',
            'senderAddress',
          ]

          // for single sheets,
          // check if there are any required properties that have different values (single sheets basically have the same order details but different order items.
          return (
            unique.orderCode === item.orderCode &&
            propertiesToCompare.some(
              // @ts-ignore
              (property) => unique[property] !== item[property]
            )
          )
        }

        // for multiple sheet, just check if the order code is the same
        return unique.orderCode === item.orderCode
      })

      // if the order is duplicated, add it to the duplicatedData array
      // otherwise, add it to the uniqueData array
      if (isOrderDuplicated) {
        duplicatedData.push(item)
      } else {
        uniqueData.push(item)
      }
    })

    // set the error messages for duplicated orders
    if (duplicatedData?.length) {
      errorMessages.value = [
        ...errorMessages.value,
        ...getErrorMessage.duplicateOrderCodes(duplicatedData),
      ]
    }

    // return the result with the isDuplicated flag and the unique/duplicated data arrays
    return {
      isDuplicated: duplicatedData.length > 0,
      duplicatedData,
    }
  },
  /**
   * validate the file data for order, and order item details,
   * if validation failed will be show an error messages
   *
   * @param order {OrdersMultipleSheet}
   * @param uploadType {number} - 0 = domestic, 1 = crossborder
   *
   * @return {string[]}
   */
  dataTypes: (
    order: OrdersMultipleSheet,
    uploadType: UploadOrdersScopeType
  ): string[] => {
    const errorValidation: string[] = []
    const { isValidDataType, isValidEmail, isRequired } = getErrorMessage
    const orderItems = order?.items || []
    // function to push error message to the validation array
    const doPushErrorMessage = (message: string) => {
      if (!message) return

      errorValidation.push(message)
    }

    // check if order code is empty
    if (!order.orderCode) {
      errorValidation.push(`Order code cannot be empty!`)

      return errorValidation
    }
    // check if order items are empty
    if (orderItems.length === 0) {
      errorValidation.push(`Order items on ${order.orderCode} is empty!`)
    }

    // define orders validation rules
    const ordersValidation = {
      alphanumeric: [
        'consigneeName',
        'consigneeEmail',
        'consigneeAddress',
        'consigneeCity',
        'consigneeCountry',
        'consigneeState',
        'consigneeProvince',
        'senderContactName',
        'senderCountry',
        'senderCity',
        'senderProvince',
        'senderState',
        'senderAddress',
      ],
      numeric: ['width', 'height', 'weight', 'length'],
      required: [
        'consigneeName',
        'consigneeNumber',
        'consigneeEmail',
        'consigneeAddress',
        'consigneePostal',
        'consigneeCity',
        'consigneeCountry',
        'consigneeProvince',
        'length',
        'width',
        'height',
        'weight',
        'paymentType',
        'senderCountry',
        'senderCity',
        'senderPostal',
        'senderAddress',
      ],
      email: ['consigneeEmail'],
    }
    const requiredCellOrdersType =
      uploadType === 'cross border'
        ? [...ordersValidation.required, 'incoterm', 'destinationPort']
        : [...ordersValidation.required]

    ordersValidation.alphanumeric.forEach((key) =>
      doPushErrorMessage(isValidDataType(order, key, order, 'string'))
    )
    ordersValidation.numeric.forEach((key) =>
      doPushErrorMessage(isValidDataType(order, key, order, 'number'))
    )
    ordersValidation.email.forEach((key) =>
      doPushErrorMessage(isValidEmail(order, key, order))
    )
    requiredCellOrdersType.forEach((key) =>
      doPushErrorMessage(isRequired(order, key, order))
    )

    // define order item validation rules
    const orderItemsValidation = {
      alphanumeric: ['category', 'currency', 'description'],
      numeric: ['quantity', 'price'],
      required: ['description', 'quantity', 'category', 'price', 'currency'],
    }

    orderItems.forEach((item) => {
      orderItemsValidation.alphanumeric.forEach((key) =>
        doPushErrorMessage(isValidDataType(order, key, item, 'string', true))
      )
      orderItemsValidation.numeric.forEach((key) =>
        doPushErrorMessage(isValidDataType(order, key, item, 'number', true))
      )
      orderItemsValidation.required.forEach((key) =>
        doPushErrorMessage(isRequired(order, key, item, true))
      )
    })

    return errorValidation
  },
}
