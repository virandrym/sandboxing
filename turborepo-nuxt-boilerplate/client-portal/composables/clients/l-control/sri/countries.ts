//  composable for L-Control SRI to get the pairing destination countries based on the origin selected

//  types - start
import { CountryCode } from '~/types/filters'

export type SRICountryType = 'origin' | 'destination' | 'all'
//  types - end

export const restrictSRICountries = (
  type: SRICountryType,
  countriesFromState: CountryCode[],
  destReferences: string[],
  countriesFromFlag: Record<string, string[]>
): CountryCode[] => {
  const countriesFS = countriesFromState
  const countriesFF = countriesFromFlag
  //  prevent empty type
  if (!type) return []

  /** check the valid destination references
   *  if the references is not valid return the default value
   */
  const isValidDestRef =
    destReferences.includes('ID') ||
    destReferences.includes('MY') ||
    destReferences.includes('SG')
  if (type === 'destination' && !isValidDestRef) {
    return [...countriesFS]
  }

  /** getting the list of the available countries
   *
   * @type origin : just get all data from @const availableCountriesOriginSRI
   * @type destination : not just get the list, but do filter for the duplicated value, and also filter the same country with the selected origin
   */
  const availableCountries =
    type === 'origin'
      ? [...Object.keys(countriesFF)]
      : [
          ...new Set(
            destReferences.map((reference) => countriesFF[reference]).flat()
          ),
        ]

  // filter the countries from API based on the available list
  return countriesFS.filter((country) =>
    availableCountries.some(
      (availCountry) =>
        availCountry.toLowerCase() === country.value.toLowerCase()
    )
  )
}
