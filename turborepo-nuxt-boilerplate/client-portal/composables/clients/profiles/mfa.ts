// types - start
import type { ParsedOTPAuthURL } from '~/types/clients/profiles/mfa'
// types - end

export const useParsedOTPAuthURL = (url: string): ParsedOTPAuthURL => {
  // parse the otp auth URL
  const parsedUrl = new URL(url)

  // extract the params
  const protocol = parsedUrl.protocol.slice(0, -1)
  const issuer = parsedUrl.searchParams.get('issuer')
  const account = parsedUrl.pathname.slice(1).split(':')[1]

  // Convert URLSearchParams to an object
  const parameters: { [key: string]: string } = {}
  parsedUrl.searchParams.forEach((value, key) => {
    parameters[key] = value
  })
  const algorithm = parameters?.algorithm || ''
  const digits = parameters?.digits || ''
  const period = parameters?.period || ''
  const secret = parameters?.secret || ''

  return {
    protocol,
    issuer,
    account,
    algorithm,
    digits,
    period,
    secret,
  }
}
