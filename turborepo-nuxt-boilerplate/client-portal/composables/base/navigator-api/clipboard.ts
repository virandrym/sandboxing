export const useClipboard = {
  paste: async (options = { isOTP: false }) => {
    // check is browser support the clipboard API
    const isSupported = navigator && 'clipboard' in navigator
    if (!isSupported) {
      throw new Error('Your browser not support the clipboard feature!')
    }

    const pasteText = await navigator?.clipboard?.readText()

    // check validity for OTP feature
    if (options.isOTP && isNaN(parseInt(pasteText))) {
      throw new Error('Data must be a number!')
    }

    return pasteText
  },
  isDeniedPermission: (error: string) => error.includes('permission denied'),
}
