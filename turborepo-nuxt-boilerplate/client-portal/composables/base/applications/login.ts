import { useLoginStore } from '~/store/base/applications/login'

// types - start
import type {
  SuspensionAllowedActions,
  RoleType,
} from '@/types/base/applications/login'
import { LoginResponse } from '@/types/base/applications/login'
// types - end

const validateUserRoles = async (role: RoleType) => {
  const loginStore = useLoginStore()
  const { logout } = loginStore

  // Restrict client customer role login action
  if (role === 'CLIENT_CUSTOMER') {
    await logout()

    throw new Error('must be a client account')
  }
}

const getUserDetails = (data: LoginResponse) => {
  const { clientId, email, partnerProfiles, suspensionAllowedActions, role } =
    data
  const suspensionAllowedFormatted =
    !!suspensionAllowedActions && Object.keys(suspensionAllowedActions)?.length
      ? suspensionAllowedActions
      : ({
          login: true,
          submitOrder: true,
          addCustomer: true,
          connectPartner: true,
          uploadDocuments: true,
        } as SuspensionAllowedActions)

  return {
    clientId,
    role,
    email,
    partnerProfiles,
    suspensionAllowedActions: suspensionAllowedFormatted,
  }
}

export const storeLoginCredentials = async (userLogged: LoginResponse) => {
  const auth = window?.$nuxt?.$auth

  // validate the user based on the role
  await validateUserRoles(userLogged.role)

  // generate the user details
  const userDetails = getUserDetails(userLogged)

  // Set user data to local storage
  auth?.setUser(userDetails)
  auth?.setUserToken(userLogged?.token)
  auth?.$storage.setLocalStorage('user', userDetails)
}
