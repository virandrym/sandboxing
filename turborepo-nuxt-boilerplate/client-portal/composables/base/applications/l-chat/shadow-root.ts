import { VUE_ADVANCED_CUSTOM_STYLES } from '~/constants/base/l-chat'

// types - start
type AdoptShadowRoot = ShadowRoot & {
  adoptedStyleSheets: CSSStyleSheet[]
}
// types - end

export const customizeVueAdvancedChatStyles = () => {
  const sheet: any = new CSSStyleSheet()
  sheet.replaceSync(VUE_ADVANCED_CUSTOM_STYLES)

  const chatElement = document.querySelector('vue-advanced-chat')

  if (chatElement) {
    const shadowRoot = chatElement.shadowRoot as AdoptShadowRoot

    if (shadowRoot) {
      // Check if the browser supports adoptedStyleSheets
      if ('adoptedStyleSheets' in shadowRoot) {
        shadowRoot.adoptedStyleSheets = [sheet]
        return
      }

      // Fallback for older browsers
      const styleElement = document.createElement('style')
      styleElement.textContent = VUE_ADVANCED_CUSTOM_STYLES
      ;(shadowRoot as AdoptShadowRoot).appendChild(styleElement)
    } else {
      // eslint-disable-next-line no-console
      console.error('Shadow root not found')
    }
  } else {
    // eslint-disable-next-line no-console
    console.error('vue-advanced-chat element not found')
  }
}
