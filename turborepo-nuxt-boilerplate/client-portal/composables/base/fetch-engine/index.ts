import { SetupContext } from '@nuxtjs/composition-api'
import { useDebounceFn } from '@vueuse/shared'

// types - start
import type { UseFetchReturn } from '@vueuse/core'
import type { AxiosResponse } from 'axios'
import type { AlertType, ErrorAPI } from '~/types/applications'
// types - end

/**
 * The options type for fetch engine
 *
 * @property messages {Object} - the default messages (success & error)
 * @property customLoadingFn {Function} - toggle custom loading function
 * @property successSubmissionFn {Function} - for define success submission function
 * @property failedSubmissionFn {Function} - for define failed submission function
 * @property finalSubmissionFn {Function} - for define final submission function
 */
export type FetchEngineOptions<
  T = unknown,
  P = Promise<UseFetchReturn<T> | T> | unknown
> = {
  messages?: {
    success: string
    failed: string
  }
  customLoadingFn?: () => void
  successSubmissionFn?: () => P | T
  failedSubmissionFn?: (messages: string | string[]) => P | T
  finalSubmissionFn?: () => P | T
}

const defineOptions = <T>(
  context: SetupContext,
  options?: FetchEngineOptions<T>
) => {
  // define alert function
  const setAlert = async (type: AlertType, messages: string | string[]) => {
    await context.root.$store.commit('applications/SET_ALERT', {
      isShow: true,
      type,
      message: messages,
    })
  }
  const resetAlert = async () => {
    await context.root.$store.commit('applications/RESET_ALERT')
  }

  /** define the options **/
  // for custom loading
  const customLoadingFn = options?.customLoadingFn || null
  // for default messages
  const defaultMessages = options?.messages || null
  // for success submission function
  const successSubmissionFn = options?.successSubmissionFn || null
  // for failed submission function
  const failedSubmissionFn = options?.failedSubmissionFn || null
  // for final submission function
  const finalSubmissionFn = options?.finalSubmissionFn || null

  return {
    setAlert,
    resetAlert,
    customLoadingFn,
    defaultMessages,
    successSubmissionFn,
    failedSubmissionFn,
    finalSubmissionFn,
  }
}

const isValidOptions = (options: Function | null) =>
  typeof options !== 'undefined' && typeof options === 'function' && options

const checkFailedResponse = <T>(responseFn: UseFetchReturn<T>) => {
  const statusCode = (responseFn as UseFetchReturn<T>)?.statusCode?.value
  const errors = { response: { data: responseFn?.data?.value } }

  if (statusCode && statusCode >= 400) {
    throw errors
  }

  return false
}

/**
 * The function for handling data fetching (with axios token management)
 *
 * @param context {SetupContext} - setup context object
 * @param fn {Function} - fetching function
 * @param options {FetchEngineOptions} - the fetch engine options
 *
 * @returns Promise<UseFetchReturn<T>>
 */
const engine = async <T extends unknown>(
  context: SetupContext,
  fn: () => Promise<UseFetchReturn<T> | T> | T,
  options?: FetchEngineOptions<T>
) => {
  const {
    setAlert,
    resetAlert,
    customLoadingFn,
    defaultMessages,
    failedSubmissionFn,
    successSubmissionFn,
    finalSubmissionFn,
  } = defineOptions<T>(context, options)

  try {
    await resetAlert()

    // set the loading
    if (isValidOptions(customLoadingFn) && customLoadingFn) {
      customLoadingFn()
    }

    // call the main function
    const responseFn = await fn()

    // check the success/failed response
    checkFailedResponse(responseFn as UseFetchReturn<T>)

    // get the success messages
    if (defaultMessages?.success) {
      await setAlert('success', defaultMessages.success)
    }

    // call the success submission function
    if (isValidOptions(successSubmissionFn) && successSubmissionFn) {
      setTimeout(async () => {
        await successSubmissionFn()
      }, 250)
    }

    return responseFn
  } catch (errors) {
    // get the failed messages
    const messages = context.root.$customUtils.getErrorMessages(
      errors as { response: AxiosResponse<ErrorAPI> },
      defaultMessages?.failed || 'Error on the fetch engine!'
    )
    await setAlert('error', messages)

    // call the failed submission function
    if (isValidOptions(failedSubmissionFn) && failedSubmissionFn) {
      await failedSubmissionFn(messages)
    }

    /* eslint-disable no-console */
    console.error(messages)
    return messages
  } finally {
    // call the final submission function
    if (isValidOptions(finalSubmissionFn) && finalSubmissionFn) {
      await finalSubmissionFn()
    }

    context.root.$nextTick(() => {
      setTimeout(() => {
        // set the loading
        if (isValidOptions(customLoadingFn) && customLoadingFn) {
          customLoadingFn()
        }
      }, 500)
    })
  }
}

export const useFetchEngine = engine
export const useDebounceFetchEngine = useDebounceFn(engine, 250)
