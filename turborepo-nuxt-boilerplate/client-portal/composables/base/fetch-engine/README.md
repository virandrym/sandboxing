# Fetch Engine Documentation

The `fetch engine` is a **powerful** tool that can be used to fetch data from an API. It is easy to use and can be customized to meet the specific needs of your application. With this function, you will be able to get a message about the success or failure of the fetch performed retrieval which you can use to display like a notification. For failed status, it will also be logged to the web console.

There are also the `debounce` function is used to `debounce` the `engine` function. This means that the `engine` function will only be called after a certain amount of time has passed since the last time it was called. This can be useful to prevent the `engine` function from being called too frequently.

The function takes three arguments:

| Parameter                                        | Type           | Description                                                                                                                   |
| ------------------------------------------------ | -------------- | ----------------------------------------------------------------------------------------------------------------------------- |
| `context`<br/> **required**                      | `SetupContext` | The setup context object for the Nuxt application.                                                                            |
| `fn` <br/> **required**                          | `Function`     | The function that will be used to fetch the data.                                                                             |
| `options` <br/> **optional**                     | `Object`       | An object that can be used to configure the fetch operation.                                                                  |
| `options.messages` <br/> **optional**            | `Object`       | This is an object that can be used to specify the success and error messages that will be displayed.                          |
| `options.messages.success` <br/> **required**    | `string`       | The success messages that will be displayed.                                                                                  |
| `options.messages.error` <br/> **required**      | `string`       | The error messages that will be displayed.                                                                                    |
| `options.customLoadingFn` <br/> **optional**     | `Function`     | This is a function that can be used to customize the loading indicator.                                                       |
| `options.successSubmissionFn` <br/> **optional** | `Function`     | This is a function that will be called when the fetch operation is successful.                                                |
| `options.failedSubmissionFn` <br/> **optional**  | `Function`     | This is a function that will be called when the fetch operation fails.                                                        |
| `options.finalSubmissionFn` <br/> **optional**   | `Function`     | This is a function that will be called after the fetch operation is complete, regardless of whether it was successful or not. |

## How It Works?

The `engine` function first checks to see if there is a previous request that is still pending. If there is, it cancels the previous request. Then, it creates a new cancellation token and calls the `fn` function. The `fn` function is responsible for making the actual HTTP request. Once the `fn` function has completed, the `engine` function checks the status of the request. If the request was successful, the `engine` function calls the `successSubmissionFn` function. If the request failed, the `engine` function calls the `failedSubmissionFn` function. Finally, the engine function calls the `finalSubmissionFn` function.

## Example Usage

Here is an example of how the `engine` function can be used to fetch data from an API:

```ts
const doFetchData = async () => {
  const options = {
    messages: {
      success: 'Successfully fetched the data!',
      failed: 'Failed to fetching the data!',
    },
    successSubmissionFn: () => {
      console.log('Data fetched successfully')
    },
    failedSubmissionFn: (error) => {
      console.log('Error fetching data!', error)
    },
    // another options will be here...
  }
  const fetchDataFn = async () => {
    console.log('Fetch the data...')
  }

  await useFetchEngine(context, fetchDataFn, options)
}
```
