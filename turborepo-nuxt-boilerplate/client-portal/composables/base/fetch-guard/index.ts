import { createFetch } from '@vueuse/core'
import { useCookies } from '@vueuse/integrations/useCookies'
import { BASE_URL, USER_TYPE } from '@/constants/base/fetch-guard'

// types - start
type UserType = keyof typeof USER_TYPE
// types - end

// define the base URL based on the user type
const defineBaseUrl = (userType: UserType) => {
  switch (userType) {
    case 'clients':
      return BASE_URL.clients
    case 'customers':
      return BASE_URL.customers
    case 'guest':
      return BASE_URL.guest
    default:
      throw new Error(`Invalid user type: ${userType}`)
  }
}

const guardEngine = (userType: UserType) => {
  const baseUrl = defineBaseUrl(userType)

  return createFetch({
    baseUrl,
    options: {
      beforeFetch({ options }) {
        options.headers = {
          ...options.headers,
          Accept: 'application/json, text/plain, */*',
        }

        // define authorization headers
        if (userType !== 'guest') {
          const { get } = useCookies()
          const strategy = get('auth.strategy')
          const authToken = get(`auth._token.${strategy}`)

          options.headers = {
            ...options.headers,
            Authorization: authToken,
          }
        }

        return { options }
      },
    },
  })
}

export const useFetchGuard = {
  clients: guardEngine('clients'),
  customers: guardEngine('customers'),
  guest: guardEngine('guest'),
}
