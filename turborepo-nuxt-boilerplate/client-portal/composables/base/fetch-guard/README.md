# Fetch Guard

Fetch guard module is designed to handle API requests with different user types. It allows you to define a base URL and apply necessary headers based on the user type.

## Example Usage

You can use the `useFetchGuard` function in your code, and there are 3 types of users that can be used (_this is customized based on the question of who is the feature for?_):

| Types of Users | Base URL                 |
| -------------- | ------------------------ |
| `clients`      | `/api/clients`           |
| `customers`    | `/api/clients/customers` |
| `guest`        | `/api`                   |

Please note that this usage is similar to the useFetch utility provided by `VueUse Core`. You can refer to the documentation of [VueUse Core's useFetch](https://vueuse.org/core/useFetch/) for more information on the available methods and options.

```ts
import { useFetchGuard } from '~/composables/base/fetch-guard'

const callAPIFunc = async () => {
  await useFetchGuard.clients('/url').post({ payload: {} }).json<GenericTypes>()
}
```

The `useFetchGuard` function can also be used together with `useFetchEngine` (because they are very different). Here's an example of the implementation of both.

```ts
import { callAPIFunc } from '~/store'

const handleAPICall = async () => {
  await useFetchEngine(setupContext, callAPIFunc)
}
```
