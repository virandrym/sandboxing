import colors from 'vuetify/es5/util/colors'
// Icons
import OrderIcon from '@/components/base/Icon/Order.vue'
import Marketplace from '@/components/base/Icon/Marketplace.vue'
import ClientConnections from '@/components/base/Icon/ClientConnections.vue'
import Profile from '@/components/base/Icon/Profile.vue'
import Freight from '@/components/base/Icon/Freight.vue'
import LControl from '@/components/base/Icon/LControl.vue'
import Domestic from '@/components/base/Icon/Domestic.vue'
import Dashboard from '@/components/base/Icon/Dashboard.vue'
import RateFill from '~/components/base/Icon/RateFill.vue'
import RateOutline from '~/components/base/Icon/RateOutline.vue'
import Bagging from '~/components/base/Icon/Bagging.vue'
import LFreight from '~/components/base/Icon/LFreight.vue'
import Partner from '~/components/base/Icon/Partner.vue'
import B2B from '~/components/base/Icon/B2B.vue'
import StatusMapping from '~/components/base/Icon/StatusMapping.vue'
import MawbIcon from '~/components/base/Icon/Mawb.vue'
import LabelConfiguration from '~/components/base/Icon/LabelConfiguration.vue'
export default {
  rtl: false,
  breakpoint: {
    mobileBreakpoint: 'sm',
  },
  theme: {
    dark: false,
    // the custom color themes
    themes: {
      light: {
        primary: {
          base: '#1961E4',
          darken1: '#1457D0',
          darken2: '#0947B7',
        },
        secondary: '#FF3D17',
        base: '#FFFFFF',
        info: colors.teal.lighten1,
        success: colors.green,
      },
    },
  },
  // the custom icon
  icons: {
    // iconfont: 'mdi',
    values: {
      order: { component: OrderIcon },
      marketplace: { component: Marketplace },
      clientConnections: { component: ClientConnections },
      profile: { component: Profile },
      freight: { component: Freight },
      lControl: { component: LControl },
      domestic: { component: Domestic },
      dashboard: { component: Dashboard },
      rateFill: { component: RateFill },
      rateOutline: { component: RateOutline },
      bagging: { component: Bagging },
      lFreight: { component: LFreight },
      partner: { component: Partner },
      b2b: { component: B2B },
      statusMapping: { component: StatusMapping },
      mawb: { component: MawbIcon },
      labelConfiguration: { component: LabelConfiguration },
    },
  },
}
