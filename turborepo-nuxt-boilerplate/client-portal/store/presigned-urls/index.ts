// types - start
import { MutationTree, ActionTree, ActionContext, Store } from 'vuex'
import { AxiosResponse, CancelToken } from 'axios'
import {
  PresignedURLs,
  PresignedURLsFeatureType,
  PresignedURLsPayload,
  UploadDocuments,
} from '~/types/presigned-urls'
// types - end

// store state - start
export const state = () => ({})
export type StatePresigned = ReturnType<typeof state>
// store state - end

// mutations - start
export interface MutationsPresigned extends MutationTree<StatePresigned> {}

export const mutations: MutationsPresigned = {}
// mutations - end

// actions - start
type AC = ActionContext<StatePresigned, StatePresigned>
export interface ActionsPresigned
  extends ActionTree<StatePresigned, StatePresigned> {
  requestPresignedURLsAPI(
    ctx: AC,
    payload: {
      featureType: PresignedURLsFeatureType
      data: PresignedURLsPayload[]
      cancelToken?: CancelToken
    }
  ): Promise<AxiosResponse<PresignedURLs[]>>
  uploadDocumentsAPI(ctx: AC, payload: UploadDocuments[]): void
}

export const actions: ActionsPresigned = {
  async requestPresignedURLsAPI(_store, { featureType, data, cancelToken }) {
    const axios = (this as unknown as Store<AC>).$axios
    const { id, name } = featureType

    return await axios.post<PresignedURLs[], any>(
      `/api/clients/${name}/${id}/presigned-urls`,
      data,
      { cancelToken }
    )
  },
  async uploadDocumentsAPI(_store, payloads) {
    const paths = payloads.map(({ url, file }) => {
      // const formData = new FormData()
      // formData.append('file', file)

      return fetch(url, {
        method: 'PUT',
        mode: 'cors',
        // headers: new Headers({
        //   'Content-Type': 'multipart/form-data',
        // }),
        body: file,
      })
    })

    return await Promise.allSettled(paths)
  },
}
// actions - end
