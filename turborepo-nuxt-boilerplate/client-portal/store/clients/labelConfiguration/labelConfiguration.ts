// Interfaces
import { MutationTree, ActionTree, ActionContext, Store } from 'vuex'
import { AxiosResponse } from 'axios'
import { LogoConfiguration } from '~/types/clients/labelConfiguration/labelConfiguration'

// *state
export const state = () => ({
  labelConfiguration: {} as LogoConfiguration,
})
export type RootStateLabelConfigurations = ReturnType<typeof state>
// end state

// *mutations
type MC = RootStateLabelConfigurations // as mutation context
export interface MutationsLabelConfig
  extends MutationTree<RootStateLabelConfigurations> {
  SET_DATA(state: MC, Ivalue: LogoConfiguration): void
}
export const mutations: MutationsLabelConfig = {
  SET_DATA: (state, value: LogoConfiguration) => {
    state.labelConfiguration = value
  },
}
// end mutations

// *actions
type AC = ActionContext<MutationsLabelConfig, MutationsLabelConfig> // as action context
export interface ActionsLabelConfigs
  extends ActionTree<MutationsLabelConfig, MutationsLabelConfig> {
  getLabelConfiguration(ctx: AC): void
  updateLabelConfiguration(ctx: AC, payload: LogoConfiguration): void
}
export const actions: ActionsLabelConfigs = {
  async getLabelConfiguration({ commit }) {
    const axios = (this as unknown as Store<RootStateLabelConfigurations>)
      .$axios

    try {
      const response = await axios.$get(`/api/clients/logo-preference`)

      if (!response) throw response

      commit('SET_DATA', response)

      return response
    } catch (error) {
      return error
    }
  },

  async updateLabelConfiguration(_store, data) {
    const axios = (this as unknown as Store<RootStateLabelConfigurations>)
      .$axios

    try {
      return (await axios.put)<AxiosResponse<any>>(
        `/api/clients/logo-preference`,
        data
      )
    } catch (error) {
      return error
    }
  },
}
// end actions
