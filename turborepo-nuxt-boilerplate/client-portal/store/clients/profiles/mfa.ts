import { ref } from '@nuxtjs/composition-api'
import { defineStore } from 'pinia'
import { useQRCode } from '@vueuse/integrations/useQRCode'
import { useFetchGuard } from '~/composables/base/fetch-guard'

// types - start
import type {
  MFA,
  MFAResponse,
  MFAActivationPayload,
  MFAStatusResponse,
} from '~/types/clients/profiles/mfa'
// types - end

export const useMFAStore = defineStore('mfa', () => {
  // init state - start
  const mfa = ref({
    isActive: false,
    secret: '',
    otpAuthURL: '',
    base64QRCode: '',
  } as MFA)
  const state = {
    mfa,
  }
  // init state - end

  // init actions - start
  const generateMFAAPI = async () => {
    const generateRes = await useFetchGuard
      .clients('/otp/generate')
      .post()
      .json<MFAResponse>()
    const { data } = generateRes

    if (data?.value) {
      const { secret, otp_auth_url: otpAuthURL } = data.value
      const base64QRCode = useQRCode(otpAuthURL, {
        errorCorrectionLevel: 'H',
        margin: 0,
        width: 150,
        version: 12,
      }) as unknown as string

      mfa.value = {
        ...mfa.value,
        secret,
        otpAuthURL,
        base64QRCode,
      }
    }

    return generateRes
  }
  const verifyMFAAPI = async (payload: MFAActivationPayload) =>
    await useFetchGuard.clients('/otp/verify').post(payload).json()
  const disableMFAAPI = async () =>
    await useFetchGuard.clients('/otp/disable').post().json()
  const getMFAStatusAPI = async () => {
    const statusRes = await useFetchGuard
      .clients('/otp/status')
      .get()
      .json<MFAStatusResponse>()
    const isEnabled = statusRes?.data?.value?.otpEnabled || false

    mfa.value.isActive = isEnabled
    if (isEnabled) {
      mfa.value = {
        ...mfa.value,
        secret: '',
        otpAuthURL: '',
        base64QRCode: '',
      }
    }

    return statusRes
  }

  const actions = {
    generateMFAAPI,
    verifyMFAAPI,
    disableMFAAPI,
    getMFAStatusAPI,
  }
  // init actions - end

  return {
    ...state,
    ...actions,
  }
})
