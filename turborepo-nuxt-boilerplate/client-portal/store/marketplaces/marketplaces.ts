// Interfaces
import { MutationTree, ActionTree, Store, ActionContext } from 'vuex'
import { CancelToken } from 'axios'
import { Meta } from '~/types/applications'
import {
  FilterDetails,
  PageView,
  ParamsGetMarketplace,
} from '~/types/marketplace/marketplace'
import {
  DetailPartner,
  Gallery,
  DetailProfile,
  Rating,
} from '~/types/marketplace/detail'

interface GetMarketplaces {
  params: ParamsGetMarketplace
  cancelToken?: CancelToken
  isLControl?: Boolean
  isConnected?: Boolean
  isCOD?: Boolean
  isChat?: Boolean
  isOnNetworkPartners?: Boolean
}

// *init
export const initMarketplaceFilter = {
  // page: 1,
  // itemsPerPage: 8,
  search: '',
  country: '',
  service: '',
  port: '',
} as FilterDetails
// end init

// *state
export const state = () => ({
  marketplacesAll: [] as DetailPartner[],
  marketplaces: [] as DetailPartner[],
  marketplacesLControl: [] as DetailPartner[],
  marketplacesCOD: [] as DetailPartner[],
  marketplacesConnected: [] as DetailPartner[],
  marketplacesConnectedLength: 0 as number,
  marketplacesChat: [] as DetailPartner[],
  incomingChat: [] as DetailPartner[],
  loadedLControl: false as Boolean,
  meta: {
    page: 1,
    totalPage: 1,
    totalCount: 8,
  } as Meta,
  filter: { ...initMarketplaceFilter },
  detail: {} as DetailPartner,
  galleries: [] as Gallery[],
  detailProfile: {} as DetailProfile | {},
  ratings: [] as Rating[],
  // manage network partners view
  pageView: 'marketplaces' as PageView,
})
export type RootStateMarketplaces = ReturnType<typeof state>
// end state

// *mutations
type MC = RootStateMarketplaces // as mutation context
export interface MutationsMarketplaces
  extends MutationTree<RootStateMarketplaces> {
  SET_MARKETPLACES_ALL(state: MC, value: DetailPartner[]): void
  SET_MARKETPLACES(state: MC, value: DetailPartner[]): void
  SET_MARKETPLACES_LCONTROL(state: MC, value: DetailPartner[]): void
  SET_MARKETPLACES_COD(state: MC, value: DetailPartner[]): void
  SET_MARKETPLACES_CONNECTED(state: MC, value: DetailPartner[]): void
  SET_MARKETPLACES_CHAT(state: MC, value: DetailPartner[]): void
  SET_INCOMING_CHAT(state: MC, value: DetailPartner[]): void
  SET_MARKETPLACES_CONNECTED_LENGTH(state: MC, value: number): void
  SET_LOADED_LCONTROL(state: MC, value: boolean): void
  SET_META(state: MC, value: Meta): void
  SET_FILTER(state: MC, value: FilterDetails): void
  RESET_FILTER(state: MC): void
  SET_DETAIL_MARKETPLACE(state: MC, value: DetailPartner): void
  SET_GALLERY(state: MC, value: Gallery[]): void
  SET_DETAIL_PROFILE(state: MC, value: DetailProfile[]): void
  RESET_DETAIL_MARKETPLACE(state: MC): void
  SET_RATINGS(state: MC, value: Rating[]): void
  SET_PAGE_VIEW(state: MC, value: PageView): void
  RESET_PAGE_VIEW(state: MC): void
}
export const mutations: MutationsMarketplaces = {
  SET_MARKETPLACES_ALL: (state, value) => (state.marketplacesAll = value),
  SET_MARKETPLACES: (state, value) => (state.marketplaces = value),
  SET_MARKETPLACES_LCONTROL: (state, value) =>
    (state.marketplacesLControl = value),
  SET_MARKETPLACES_COD: (state, value) => (state.marketplacesCOD = value),
  SET_MARKETPLACES_CONNECTED: (state, value) =>
    (state.marketplacesConnected = value),
  SET_MARKETPLACES_CHAT: (state, value) => (state.marketplacesChat = value),
  SET_INCOMING_CHAT: (state, value) => (state.incomingChat = value),
  SET_MARKETPLACES_CONNECTED_LENGTH: (state, value) =>
    (state.marketplacesConnectedLength = value),
  SET_LOADED_LCONTROL: (state, value) => (state.loadedLControl = value),
  SET_META: (state, value) => (state.meta = value),
  SET_FILTER: (state, value) => (state.filter = value),
  RESET_FILTER: (state) => (state.filter = { ...initMarketplaceFilter }),
  SET_DETAIL_MARKETPLACE: (state, value) => (state.detail = value),
  SET_GALLERY: (state, value) => (state.galleries = value),
  SET_DETAIL_PROFILE: (state, value) => (state.detailProfile = value),
  RESET_DETAIL_MARKETPLACE: (state) => (state.detail = {} as DetailPartner),
  SET_RATINGS: (state, value) => (state.ratings = value),
  SET_PAGE_VIEW: (state, value) => (state.pageView = value),
  RESET_PAGE_VIEW: (state) => (state.pageView = 'marketplaces'),
}
// end mutations

// *actions
type AC = ActionContext<RootStateMarketplaces, RootStateMarketplaces> // as action context
export interface ActionsMarketplaces
  extends ActionTree<RootStateMarketplaces, RootStateMarketplaces> {
  getMarketplacesAll(ctx: AC): void
  getMarketplaces(ctx: AC, payload: GetMarketplaces): void
  getMarketplacesConnected(ctx: AC, payload: GetMarketplaces): void
  addConnection(ctx: AC, payload: { id: String }): void
  getDetail(ctx: AC, payload: string): void
  getGalleries(ctx: AC, payload: string): void
  getRatesAvailable(ctx: AC, payload: { partnerID: string }): void
  getRatesDownload(ctx: AC, payload: { partnerID: string; name: string }): void
  getRatings(ctx: AC, payload: { partnerId: string }): void
}
export const actions: ActionsMarketplaces = {
  async getMarketplacesAll({ commit }) {
    const axios = (this as unknown as Store<RootStateMarketplaces>).$axios

    try {
      const response = await axios?.$get(
        `/api/clients/partners?page=1&perPage=1000`
      )
      const { data } = response
      // const { data, page, totalPage, totalCount } = response

      if (!data) throw response

      // const meta = {
      //   page,
      //   totalPage,
      //   totalCount,
      // }
      commit('SET_MARKETPLACES_ALL', data)

      return response
    } catch (error) {
      return error
    }
  },
  async getMarketplaces(
    { commit, state },
    { params, cancelToken, isLControl, isCOD }
  ) {
    const axios = (this as unknown as Store<RootStateMarketplaces>).$axios
    const customUtils = (this as unknown as Store<RootStateMarketplaces>)
      .$customUtils

    try {
      const formattedParams = customUtils.setURLParams(
        {
          ...params,
          cod: isCOD,
        },
        true
      )
      const response = await axios?.$get(`/api/clients/partners`, {
        params: formattedParams,
        cancelToken,
      })
      const { data, page, totalPage, totalCount } = response

      if (!data) throw response

      const meta = {
        page,
        totalPage,
        totalCount,
      }
      if (!isLControl) {
        commit('SET_MARKETPLACES', data)
        commit('SET_META', meta)
      }
      if (!state.loadedLControl || isLControl) {
        commit('SET_MARKETPLACES_LCONTROL', data)
        commit('SET_LOADED_LCONTROL', true)
      }

      return response
    } catch (error) {
      return error
    }
  },
  async getMarketplacesConnected(
    { commit, dispatch },
    { params, isCOD, isChat, isOnNetworkPartners = false }
  ) {
    const axios = (this as unknown as Store<RootStateMarketplaces>).$axios
    const customUtils = (this as unknown as Store<RootStateMarketplaces>)
      .$customUtils

    try {
      const formattedParams = customUtils.setURLParams(
        {
          ...params,
          connection: 'connected',
          cod: isCOD,
        },
        true
      )
      const response = await axios?.$get(`/api/clients/partners`, {
        params: formattedParams,
      })
      const { data, page, totalPage, totalCount } = response

      if (!data) throw response

      if (isChat && !isCOD) {
        let listChannel = await Promise.all(
          data.map(async (el: any) => {
            try {
              const channel = await dispatch('sendbird/getChatChannel', el.id, {
                root: true,
              })
              return {
                name: el.name,
                channel_url: channel?.channel_url ?? '',
                logo: el.logo ? 'data:image/png;base64,' + el.logo : '',
              }
            } catch (error) {
              return error
            }
          })
        )
        listChannel = listChannel.filter((x: any) => x.channel_url)

        const incomingChats = await dispatch(
          'sendbird/getChatIncoming',
          {},
          { root: true }
        )
        const allArr = listChannel
        // if (incomingChats) allArr = listChannel.concat(incomingChats);

        commit('SET_MARKETPLACES_CONNECTED_LENGTH', data.length)
        commit('SET_MARKETPLACES_CHAT', allArr)
        commit('SET_INCOMING_CHAT', incomingChats)
      }
      if (!isChat && !isCOD) {
        commit('SET_MARKETPLACES_CONNECTED', data)
      }
      if (isCOD) {
        commit('SET_MARKETPLACES_COD', data)
      }
      // if it's on network partners view set meta to this connected network partners
      if (isOnNetworkPartners) {
        commit('SET_META', {
          page,
          totalPage,
          totalCount,
        })
      }

      return response
    } catch (error) {
      return error
    }
  },
  async addConnection(_store, { id }) {
    const axios = (this as unknown as Store<RootStateMarketplaces>).$axios

    try {
      return await axios?.$post(`/api/clients/connections/${id}`)
    } catch (error: any) {
      return error?.response?.data
    }
  },
  async getDetail({ commit }, id) {
    const axios = (this as unknown as Store<RootStateMarketplaces>).$axios

    try {
      const response = await axios?.$get(
        `/api/clients/partner-details/${id ?? ''}`
      )

      if (!response) throw response
      let temp = response?.partnerGallery
      if (response?.partnerGallery?.length > 0) {
        temp = temp.map((el: Gallery) => {
          return { src: el.path }
        })
      }
      if (response?.partnerServiceTypes) {
        // console.log({ partnerServiceTypes: response?.partnerServiceTypes });
        response.partnerServiceTypes = response.partnerServiceTypes.filter(
          (el: any) => el.name
        )
      }
      commit('SET_DETAIL_MARKETPLACE', response)
      commit('SET_GALLERY', temp)
      // commit('SET_DETAIL_PROFILE', response);

      return response
    } catch (error) {
      return error
    }
  },
  async getGalleries({ commit }, id) {
    const axios = (this as unknown as Store<RootStateMarketplaces>).$axios

    try {
      const response = await axios?.$get(
        `api/clients/partners/${id ?? ''}/profile`
      )

      if (!response) throw response
      let temp = response?.gallery
      if (response?.gallery?.length > 0) {
        temp = temp.map((el: Gallery) => {
          return { src: el.path }
        })
      }

      if (response?.serviceType) {
        response.serviceType = response.serviceType.filter((el: any) => el.name)
      }

      commit('SET_GALLERY', temp)
      commit('SET_DETAIL_PROFILE', response)

      return response
    } catch (error) {
      return error
    }
  },
  async getRatesAvailable(_state, { partnerID }) {
    const axios = (this as unknown as Store<RootStateMarketplaces>).$axios

    try {
      return await axios?.$get(
        `/api/clients/partners/${partnerID ?? ''}/rates/is-rates-available`
      )
    } catch (error) {
      return error
    }
  },
  async getRatesDownload(_state, { partnerID, name }) {
    const axios = (this as unknown as Store<RootStateMarketplaces>).$axios

    try {
      const response = await axios({
        url: `/api/clients/partners/${partnerID ?? ''}/rates/download`,
        method: 'GET',
        responseType: 'blob', // important
      })

      const url = window.URL.createObjectURL(new Blob([response.data]))
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', `${name ?? 'rates-NP'}.xls`)
      document.body.appendChild(link)
      link.click()
      return link
    } catch (error) {
      return error
    }
  },
  async getRatings({ commit }, { partnerId }) {
    const axios = (this as unknown as Store<RootStateMarketplaces>).$axios

    try {
      const data = await axios.$get(`api/clients/partners/${partnerId}/ratings`)

      commit('SET_RATINGS', data)
      return data
    } catch (error) {
      return error
    }
  },
}
