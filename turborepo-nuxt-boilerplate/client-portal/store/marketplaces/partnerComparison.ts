// types
import { MutationTree, ActionTree } from 'vuex'
import {
  RatesParams,
  RatesPayload,
} from '~/types/marketplace/partnerComparison'

export const state = () => ({
  rates: [] as RatesPayload[],
})

export type RootStatePartnerComparison = ReturnType<typeof state>

export const mutations: MutationTree<RootStatePartnerComparison> = {
  SET_RATES: (state, value: RatesPayload[]) => (state.rates = value),
}

export const actions: ActionTree<
  RootStatePartnerComparison,
  RootStatePartnerComparison
> = {
  async getRates({ commit }, { params }: { params: RatesParams }) {
    try {
      const source = this.$axios.CancelToken.source()
      const data =
        (await this.$axios.$get<RatesPayload[]>(
          '/api/clients/rate-comparisons',
          {
            params,
            cancelToken: source.token,
          }
        )) || []

      commit('SET_RATES', data)
      return data
    } catch (error) {
      return error
    }
  },
}
