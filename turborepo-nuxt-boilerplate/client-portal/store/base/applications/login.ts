import { defineStore } from 'pinia'
import { useFetchGuard } from '~/composables/base/fetch-guard'

// types - start
import type {
  LoginOTPPayload,
  MFALoginResponse,
} from '~/types/clients/profiles/mfa'
import type { LoginResponse, Login } from '~/types/base/applications/login'
// types - end

export const useLoginStore = defineStore('login', () => {
  /** init actions - start **/
  const loginAPI = async (payload: Login) =>
    await useFetchGuard
      .guest('/login')
      .post(payload)
      .json<LoginResponse | MFALoginResponse>()

  const logout = async () => {
    const auth = window?.$nuxt?.$auth
    const router = window?.$nuxt?.$router

    // hide zendesk widget
    if (window.zE) window.zE.hide()

    await auth?.logout()
    setTimeout(() => {
      router?.push('/login')
    }, 250)
  }
  const verifyLoginOTPAPI = async (payload: LoginOTPPayload) =>
    await useFetchGuard.guest('/login/otp').post(payload).json<LoginResponse>()

  const manageActions = {
    loginAPI,
    logout,
    verifyLoginOTPAPI,
  }
  /** init actions - end **/

  return {
    ...manageActions,
  }
})
