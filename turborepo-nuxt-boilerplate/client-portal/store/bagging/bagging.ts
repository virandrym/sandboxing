import { AxiosError, CancelToken, AxiosResponse } from 'axios'
// Interfaces
import { saveAs } from 'file-saver'
import { MutationTree, ActionTree } from 'vuex'
import { Meta, PaginatedResponse } from '~/types/applications'
import {
  FilterBagging,
  BagData,
  InputPostBag,
  Bagged,
  Unbagged,
  BagUpdate,
  TabState,
  InputLabelBags,
  CompletedBag,
  InputDeleteBag,
  InputManifestAPI,
  Order,
  UnbaggedCount,
  BaggedOrders,
  ParamsGetBaggedOrders,
  ParamsGetUnbaggedOrders,
  UnbaggedOrder,
  IncomingFFOrder,
  InputPostScannableBagOrders,
  ScannableBagOrders,
  ParamsGetIncomingOrders,
  FilterIncoming,
  InputRemoveOrder,
} from '~/types/bagging/bagging'

interface UnbaggedOrderResponse extends Meta {
  data: UnbaggedOrder[]
}

export const filterBaggingInit = {
  createdFrom: '',
  createdTo: '',
  client: '',
  destination: '',
} as FilterBagging

export const metaCompletedInit = {
  page: 1,
  totalPage: 1,
  totalCount: 10,
} as Meta

export const FilterIncomingInit = {
  orderId: '',
  originPortId: '',
  destinationPortId: '',
  clientId: '',
  bagId: '',
} as FilterIncoming

export const state = () => ({
  incomingFFOrders: [] as IncomingFFOrder[],
  scannableBagOrders: {} as ScannableBagOrders,
  bags: [] as Bagged[],
  unbagged: [] as Unbagged[],
  // *bagged/orders
  baggedOrders: [] as Bagged[],
  unbaggedData: {
    // *unbagged/count
    count: [] as UnbaggedCount[],
    // *unbagged/orders
    order: {
      data: [] as UnbaggedOrder[],
      meta: {
        page: 1,
        totalCount: 10,
        totalPage: 1,
      } as Meta,
    },
  },
  bagUpdates: [] as BagUpdate[],
  metaCompleted: {
    page: 1,
    totalPage: 1,
    totalCount: 10,
  } as Meta,
  meta: {
    page: 1,
    totalPage: 1,
    totalCount: 10,
  } as Meta,
  filter: { ...filterBaggingInit },
  isShowFilter: false,
  tab: {
    orderView: {
      '0': 0,
      '1': 0,
    },
    step: 0,
  } as TabState,
  bagsPartner: [] as Unbagged[],
  unbaggedPartner: [] as Unbagged[],
  completedBags: [] as CompletedBag[],
  manifestedBags: [] as CompletedBag[],
  detailManifest: [] as Order[],
  metaManifested: {
    page: 1,
    totalPage: 1,
    totalCount: 10,
  } as Meta,
  metaIncoming: {
    page: 1,
    totalPage: 1,
    totalCount: 10,
  } as Meta,
})

export type RootStateBagging = ReturnType<typeof state>
// end state

// mutations
type MC = RootStateBagging // as mutation context
export interface MutationsBagging extends MutationTree<RootStateBagging> {
  SET_TAB_VIEW(state: MC, value: TabState): void
}

export const mutations: MutationTree<RootStateBagging> = {
  // set partner baggingtab view
  SET_TAB_VIEW: (state, value) => (state.tab = value),

  // * incoming ff orders
  SET_INCOMING_FF_ORDERS: (state, value: IncomingFFOrder[]) => {
    state.incomingFFOrders = value ?? []
  },
  SET_META_INCOMING: (state, value: Meta) =>
    (state.metaIncoming = { ...state.metaIncoming, ...value }),

  // * incoming scan bag orders
  SET_SCANNABLE_BAG_ORDERS: (state, value: ScannableBagOrders) => {
    state.scannableBagOrders = value ?? {}
  },
  // * bags
  SET_DATA: (state, value: BagData) => {
    state.bags = value.bagged ?? []
    state.unbagged = value.unbagged ?? []
  },
  // end bags

  // * bagged/orders
  SET_BAG_ORDERS: (state, value) => {
    state.baggedOrders = value
  },
  SET_META: (state, value: Meta) => (state.meta = { ...state.meta, ...value }),
  // end bagged/orders

  // * unbagged/count
  SET_UNBAGGED_COUNT: (state, value: UnbaggedCount[]) =>
    (state.unbaggedData.count = value || []),
  RESET_UNBAGGED_COUNT: (state) => (state.unbaggedData.count = []),
  // end unbagged/count

  // * unbagged/orders
  SET_UNBAGGED_ORDERS_DATA: (state, value: UnbaggedOrder[]) =>
    (state.unbaggedData.order.data = value || []),
  SET_UNBAGGED_ORDERS_META: (state, value: Meta) =>
    (state.unbaggedData.order.meta = {
      ...state.unbaggedData.order.meta,
      ...value,
    }),
  RESET_UNBAGGED_ORDERS: (state) =>
    (state.unbaggedData.order = {
      data: [],
      meta: {
        page: 1,
        totalCount: 10,
        totalPage: 1,
      },
    }),
  // end unbagged/orders

  SET_BAG_UPDATE: (state, value: BagUpdate[]) =>
    (state.bagUpdates = value ?? []),
  SET_FILTER: (state, value: FilterBagging) => (state.filter = value),
  SET_FILTER_BTN: (state) => (state.isShowFilter = !state.isShowFilter),
  SET_TAB_BTN: (state, value: TabState) => (state.tab = value),
  RESET_FILTER: (state) => (state.filter = filterBaggingInit),
  RESET_PAGE_PARTNER: (state) => (state.metaCompleted = metaCompletedInit),

  SET_DATA_PARTNER_BAGGED_ORDERS: (state, value: Unbagged[]) => {
    state.bagsPartner = value ?? []
  },
  SET_DATA_PARTNER_UNBAGGED_ORDERS: (state, value: Unbagged[]) => {
    state.unbaggedPartner = value ?? []
  },
  SET_DATA_PARTNER_COMPLETE: (state, value: CompletedBag[]) => {
    state.completedBags = value
  },
  SET_META_COMPLETED: (state, value: Meta) =>
    (state.metaCompleted = { ...state.metaCompleted, ...value }),
  SET_DATA_PARTNER_MANIFESTED: (state, value: CompletedBag[]) => {
    state.manifestedBags = value
  },
  SET_META_MANIFESTED: (state, value: Meta) =>
    (state.metaManifested = { ...state.metaManifested, ...value }),
  SET_DATA_PARTNER_DETAIL_MANIFESTED: (state, value: Order[]) => {
    state.detailManifest = value
  },
}

export const actions: ActionTree<RootStateBagging, RootStateBagging> = {
  // * bags
  async getBags(
    { commit },
    { params, cancelToken }: { params: FilterBagging; cancelToken: CancelToken }
  ) {
    try {
      const response = await this.$axios.$get<{
        bagged: Bagged[]
        unbagged: Unbagged[]
      }>('/api/clients/bags', {
        params,
        cancelToken,
      })
      const { bagged = [], unbagged = [] } = response
      // const { page } = state.meta
      // const totalCount = bagged?.length ?? 10
      // const totalPage =
      //   Math?.ceil(
      //     bagged?.length /
      //       (rootState as any).applications.pagination.itemsPerPage
      //   ) ?? 1

      commit(
        'SET_DATA',
        response || {
          bagged,
          unbagged,
        }
      )
      // commit('SET_META', {
      //   page,
      //   totalCount,
      //   totalPage,
      // } as Meta)
      return response
    } catch (error) {
      return error
    }
  },
  // * bagged/orders
  async getBaggedOrders(
    { commit },
    {
      params,
      cancelToken,
    }: { params: ParamsGetBaggedOrders; cancelToken: CancelToken }
  ) {
    try {
      const response = await this.$axios.$get<BaggedOrders>(
        '/api/clients/bagged/orders',
        {
          params,
          cancelToken,
        }
      )
      const { data = [], page = 1, totalCount = 10, totalPage = 1 } = response
      commit('SET_BAG_ORDERS', data || [])
      commit('SET_META', {
        page,
        totalCount,
        totalPage,
      } as Meta)

      return data
    } catch (error) {
      return error
    }
  },
  // * unbagged/count
  async getUnbaggedCount(
    { commit },
    { cancelToken }: { cancelToken: CancelToken }
  ) {
    try {
      const data = await this.$axios.$get<UnbaggedCount[]>(
        '/api/clients/unbagged/count',
        {
          cancelToken,
        }
      )
      commit('SET_UNBAGGED_COUNT', data || [])

      return data
    } catch (error) {
      return error
    }
  },
  // * unbagged/orders
  async getUnbaggedOrders(
    { commit },
    {
      params,
      cancelToken,
    }: { params: ParamsGetUnbaggedOrders; cancelToken: CancelToken }
  ) {
    try {
      const response = await this.$axios.$get<UnbaggedOrderResponse>(
        '/api/clients/unbagged/orders',
        {
          params,
          cancelToken,
        }
      )
      const { data = [], page = 1, totalCount = 10, totalPage = 1 } = response
      commit('SET_UNBAGGED_ORDERS_DATA', data || [])
      commit('SET_UNBAGGED_ORDERS_META', {
        page,
        totalCount,
        totalPage,
      } as Meta)

      return data
    } catch (error) {
      return error
    }
  },
  async postBags(_store, { payload }: { payload: InputPostBag }) {
    try {
      const response = await this.$axios.$post('/api/clients/bags', payload)
      return response
    } catch (error) {
      return error
    }
  },

  async getBagsUpdate({ commit }, { bagID }: { bagID: string }) {
    try {
      const response = await this.$axios.$get(
        `/api/clients/bags/${bagID}/updates`
      )

      if (!response) throw response

      commit('SET_BAG_UPDATE', response.bag_updates)
      // commit('SET_BAG_UPDATE', tempData.bagging.bag_updates);

      return response
    } catch (error) {
      return error
    }
  },
  async getMAWBUpdate(
    { commit },
    { bagID, payload }: { bagID: string; payload: { mawb: string } }
  ) {
    try {
      const response = await this.$axios.$patch(
        `/api/clients/bags/${bagID}`,
        payload
      )

      if (!response) throw response

      commit('SET_BAG_UPDATE', response.bag_updates)

      return response
    } catch (error) {
      return error
    }
  },
  async getBagsPartner({ commit }, { partnerID }: { partnerID: string }) {
    try {
      const request = [
        this.$axios.$get(`/api/clients/partners/${partnerID}/bagged-orders`),
        this.$axios.$get(`/api/clients/partners/${partnerID}/unbagged-orders`),
      ]

      const [bagged, unbagged] = await Promise.allSettled(request)

      if (bagged.status === 'rejected') throw bagged.reason
      commit('SET_DATA_PARTNER_BAGGED_ORDERS', bagged.value)

      if (unbagged.status === 'rejected') throw unbagged.reason
      commit('SET_DATA_PARTNER_UNBAGGED_ORDERS', unbagged.value)
      return { bagged, unbagged }
    } catch (error) {
      return error
    }
  },

  async getBagsPartnerOld({ commit }, { partnerID }: { partnerID: string }) {
    try {
      const response = await this.$axios.$get(
        `/api/clients/partners/${partnerID}/bags`
      )

      if (!response) throw response

      commit('SET_DATA_PARTNER_BAGGED_ORDERS', response.bagged)
      commit('SET_DATA_PARTNER_UNBAGGED_ORDERS', response.unbagged)
      // commit('SET_META', meta);
      return response
    } catch (error) {
      return error
    }
  },
  async postLabelBags(_store, { payload }: { payload: InputLabelBags }) {
    try {
      const response = await this.$axios.$post(
        '/api/clients/label/bags',
        payload
      )
      return response
    } catch (error) {
      return error
    }
  },
  async getBagsPartnerCompleted(
    { commit },
    {
      id,
      metaCompleted,
    }: { id: String; metaCompleted: { page: Number; perPage: Number } }
  ) {
    try {
      const response = await this.$axios.$get(
        `/api/clients/partners/${id}/completed`,
        { params: metaCompleted }
      )

      const { data, page, totalPage, totalCount } = response
      if (!data) throw response

      const meta = {
        page,
        totalPage,
        totalCount,
      }

      commit('SET_DATA_PARTNER_COMPLETE', data)
      commit('SET_META_COMPLETED', meta)
      return response
    } catch (error) {
      return error
    }
  },
  async postBagsCompleted(_store, { payload }: { payload: InputPostBag }) {
    try {
      const response = await this.$axios.$post(
        '/api/clients/complete/bags',
        payload
      )
      return response
    } catch (error) {
      return error
    }
  },
  async deleteOrderBag(_store, { payload }: { payload: InputDeleteBag }) {
    try {
      const custPayload = {
        partner_id: payload.partner_id,
        reason: payload.reason,
        comments: payload.comments,
      } as any
      if (payload.bag_id) custPayload.bag_id = payload.bag_id
      else custPayload.order_id = payload.order_id

      const response = await this.$axios.$delete(
        `/api/clients/bags/${payload.id}`,
        { data: custPayload }
      )
      return response
    } catch (error) {
      return error
    }
  },
  async postLabelBagsPartnerLayer2(
    _store,
    { payload }: { payload: InputLabelBags }
  ) {
    try {
      const response = await this.$axios.$post(
        `/api/clients/partners/${payload?.partnerID}/label/${payload.bag_id}`,
        payload
      )
      return response
    } catch (error) {
      return error
    }
  },
  async sealBag(_store, { payload }: { payload: InputLabelBags }) {
    try {
      const response = await this.$axios.$post(
        `/api/clients/seal/${payload.bag_id}`
      )
      return response
    } catch (error) {
      return error
    }
  },
  async getBagsPartnerManifest(
    { commit },
    { metaManifest }: { metaManifest: { page: Number; perPage: Number } }
  ) {
    try {
      const response = await this.$axios.$get(`/api/clients/manifest`, {
        params: metaManifest,
      })

      const { data, page, totalPage, totalCount } = response
      if (!data) throw response

      const meta = {
        page,
        totalPage,
        totalCount,
      }

      commit('SET_DATA_PARTNER_MANIFESTED', data)
      commit('SET_META_MANIFESTED', meta)
      return response
    } catch (error) {
      return error
    }
  },
  async postGenerateManifest(
    _store,
    { payload }: { payload: InputManifestAPI }
  ) {
    try {
      const response = (await this.$axios({
        url: `/api/clients/manifest`,
        method: 'POST',
        responseType: 'blob',
        data: payload,
      })) as AxiosResponse<Blob>

      saveAs(response.data, `manifest-${payload.mawb}.xlsx`)

      return true
    } catch (error) {
      return false
    }
  },
  async getDetailManifest({ commit }, { id }: { id: string }) {
    try {
      const response = await this.$axios.$get(
        `/api/clients/manifest/${id}/details`
      )

      const { orders } = response
      if (!orders) throw response

      commit('SET_DATA_PARTNER_DETAIL_MANIFESTED', orders)
      return response
    } catch (error) {
      return error
    }
  },
  async postReweight(
    _store,
    {
      payload,
    }: {
      payload: {
        orders_reweigh: { order_id: string; weight: number }[]
        id: string
      }
    }
  ) {
    try {
      // console.log({ payload });
      const payloadAPI = { orders_reweigh: payload.orders_reweigh }
      // console.log({ payloadAPI });
      const response = await this.$axios.$post(
        `/api/clients/manifest/${payload.id}/reweigh`,
        payloadAPI
      )
      return response
    } catch (error) {
      return error
    }
  },

  async getIncomingFFOrders(
    { commit },
    { id, params }: { id: string; params: ParamsGetIncomingOrders }
  ) {
    try {
      const response = await this.$axios.$get<
        PaginatedResponse<IncomingFFOrder[]>
      >(`/api/clients/partners/${id}/incoming-ff-orders`, { params })
      const { data, page, totalPage, totalCount } = response
      if (!data) throw response

      commit('SET_INCOMING_FF_ORDERS', data)

      // do not use filter response for meta data - i.e. when params.perPage = 'all'
      if (typeof params.perPage !== 'string') {
        const meta = {
          page,
          totalPage,
          totalCount,
        }

        commit('SET_META_INCOMING', meta)
      }

      return response
    } catch (error) {
      return error
    }
  },

  async getScannableBagOrders({ commit }, { id }) {
    try {
      const response = await this.$axios.$get(
        `/api/clients/partners/${id}/scannable-bag-orders`
      )

      if (!response) throw response

      commit('SET_SCANNABLE_BAG_ORDERS', response?.data)

      return response
    } catch (error) {
      return error
    }
  },

  async postScannableBagOrders(
    _store,
    { id, payload }: { id: any; payload: InputPostScannableBagOrders }
  ) {
    try {
      return await this.$axios.post(
        `/api/clients/partners/${id}/scan-bag-orders`,
        payload
      )
    } catch (error) {
      return (error as AxiosError<any>).response
    }
  },
  async removeOrderBag(_store, { payload }: { payload: InputRemoveOrder }) {
    try {
      const response = await this.$axios.$delete(
        `/api/clients/bags/${payload.bagId}/orders/${payload.orderId}`
      )
      return response
    } catch (error) {
      return error
    }
  },
}
