// types -start
import { MutationTree, ActionTree, ActionContext, Store } from 'vuex'
import { AxiosResponse, CancelToken } from 'axios'
import {
  B2BAttachDocuments,
  B2BPostPayload,
  B2BPostResponse,
  B2BQuotationFormData,
  B2BQuotationResponse,
  B2BAttachRfqToOrder,
} from '~/types/orders/b2b'
// types - end

// store state - start
export const state = () => ({
  rfq: {} as B2BQuotationResponse,
})
export type StateB2BOrders = ReturnType<typeof state>
// store state - end

// mutations - start
type MC = StateB2BOrders // as mutation context
export interface MutationsB2BOrders extends MutationTree<StateB2BOrders> {
  SET_RFQ(state: MC, value: B2BQuotationResponse): void
  RESET_RFQ(state: MC): void
}

export const mutations: MutationsB2BOrders = {
  SET_RFQ: (state, value) => (state.rfq = value),
  RESET_RFQ: (state) =>
    (state.rfq = {
      rateID: '',
      rate: null,
      currency: '',
      quotationID: '',
      totalEstimatedPrice: null,
    }),
}
// mutations - end

// actions - start
type AC = ActionContext<StateB2BOrders, StateB2BOrders>
export interface ActionsB2BOrders
  extends ActionTree<StateB2BOrders, StateB2BOrders> {
  postB2BShipmentsAPI(
    ctx: AC,
    payload: { data: B2BPostPayload; cancelToken?: CancelToken }
  ): Promise<AxiosResponse<B2BPostResponse>>
  attachB2BDocumentsAPI(
    ctx: AC,
    payload: {
      data: { id: string; payload: B2BAttachDocuments }
      cancelToken?: CancelToken
    }
  ): void
  postB2BOrderQuotationAPI(
    ctx: AC,
    payload: { data: B2BQuotationFormData; cancelToken?: CancelToken }
  ): void
  attachRfqAPI(
    ctx: AC,
    payload: {
      data: { id: string; payload: B2BAttachRfqToOrder }
      cancelToken?: CancelToken
    }
  ): void
}

export const actions: ActionsB2BOrders = {
  async postB2BShipmentsAPI(_store, { data, cancelToken }) {
    const axios = (this as unknown as Store<AC>).$axios

    return await axios.post<B2BPostResponse, any>(
      `/api/clients/b2b/orders`,
      data,
      { cancelToken }
    )
  },
  async attachB2BDocumentsAPI(_store, { data: { id, payload }, cancelToken }) {
    const axios = (this as unknown as Store<AC>).$axios

    return await axios.post(
      `/api/clients/b2b/${id}/attach-documents`,
      payload,
      {
        cancelToken,
      }
    )
  },
  async postB2BOrderQuotationAPI({ commit }, { data, cancelToken }) {
    const axios = (this as unknown as Store<AC>).$axios

    const response = await axios.post(`/api/clients/b2b/quotations`, data, {
      cancelToken,
    })
    commit('SET_RFQ', response.data || {})
  },
  async attachRfqAPI(_store, { data: { id, payload }, cancelToken }) {
    const axios = (this as unknown as Store<AC>).$axios

    return await axios.patch(`/api/clients/b2b/quotations/${id}`, payload, {
      cancelToken,
    })
  },
}
// actions - end
