// types - start
import type { MutationTree, ActionTree, ActionContext, Store } from 'vuex'
import { initMeta } from '../applications'
import type {
  OrderDetails,
  Order,
  OrderItem,
  OrderAllocationUpdate,
  FilterOrders,
  BatchOrders,
  FilterBatch,
  FilterMawb,
  NodeCalculator,
  ExportOrdersParams,
  OrderDutiesTaxes,
  SetAwb,
  MawbDocuments,
  OrderFlightSchedule,
} from '~/types/orders'
import type {
  Meta,
  PaginationPayload,
  PaginatedResponse,
} from '~/types/applications'
import type {
  OrderCrossBorder,
  OrderDomestic,
} from '~/types/orders/upload-submission'

interface OrderGetPayload extends PaginationPayload, FilterOrders {}
interface BatchGetParams extends PaginationPayload, FilterBatch {}
interface MAWBGetPayload extends PaginationPayload, FilterMawb {}
interface UploadOrdersPayload {
  type: string
  data: OrderDomestic[] | OrderCrossBorder[]
}
interface ButtonBatchViewPayload {
  selectedTransferCost: number
  step: number
}
// types - end

// *init data
export const filterOrderInit = {
  orderCode: null,
  batchCode: null,
  originCountry: null,
  createdFrom: null,
  createdTo: null,
  destinationCountry: null,
  serviceType: [],
  originPortId: null,
  destinationPortId: null,
  status: {
    Remarks: '',
    ServiceType: '',
    Status: '',
  },
  customerId: null,
  terminalStatus: null,
} as FilterOrders
export const filterBatchInit = {
  batchCode: '',
} as FilterBatch
export const filterMawbInit = {
  mawbId: '',
} as FilterMawb
export const mawbDocumentsInit = {
  awbUrl: '',
  hawbUrl: '',
  packingListUrl: '',
}
export const orderCategory = [
  'Fashion & Accessories',
  'Shoes',
  'Bag & Purse',
  'Gadget & Accessories',
  'Health & Beauty',
  'Media, Music & Books',
  'Mother & Baby',
  'Pet Supplies',
  'Sports & Outdoors',
  'Stationery & Craft',
  'Toys & Games',
  'Travel & Luggage',
  'Food & Supplements',
  'Bedding & Bath',
  'Cameras & Drones',
  'Computers & Laptops',
  'Electronics Accessories',
  'Furniture & Organization',
  'Motors',
  'Outdoor & Garden',
  'Documents & Letter',
]

export const availableCurrencies = Object.values({
  AF: 'AFN',
  AL: 'ALL',
  DZ: 'DZD',
  AS: 'USD',
  AD: 'EUR',
  AO: 'AOA',
  AI: 'XCD',
  AG: 'XCD',
  AR: 'ARS',
  AM: 'AMD',
  AW: 'AWG',
  AU: 'AUD',
  AT: 'EUR',
  AZ: 'AZM',
  BS: 'BSD',
  BH: 'BHD',
  BD: 'BDT',
  BB: 'BBD',
  BY: 'BYR',
  BE: 'EUR',
  BZ: 'BZD',
  BJ: 'XOF',
  BM: 'BMD',
  BT: 'BTN',
  BO: 'BOV',
  BA: 'BAM',
  BW: 'BWP',
  BV: 'NOK',
  BR: 'BRL',
  IO: 'USD',
  BN: 'BND',
  BG: 'BGN',
  BF: 'XOF',
  BI: 'BIF',
  KH: 'KHR',
  CM: 'XAF',
  CA: 'CAD',
  CV: 'CVE',
  KY: 'KYD',
  CF: 'XAF',
  TD: 'XAF',
  CL: 'CLF',
  CN: 'CNY',
  CX: 'AUD',
  CC: 'AUD',
  CO: 'COU',
  KM: 'KMF',
  CG: 'XAF',
  CD: 'CDF',
  CK: 'NZD',
  CR: 'CRC',
  CI: 'XOF',
  HR: 'HRK',
  CU: 'CUP',
  CY: 'CYP',
  CZ: 'CZK',
  DK: 'DKK',
  DJ: 'DJF',
  DM: 'XCD',
  DO: 'DOP',
  EC: 'USD',
  EG: 'EGP',
  EU: 'EUR',
  SV: 'USD',
  GQ: 'XAF',
  ER: 'ERN',
  EE: 'EEK',
  ET: 'ETB',
  FK: 'FKP',
  FO: 'DKK',
  FJ: 'FJD',
  FI: 'EUR',
  FR: 'EUR',
  GF: 'EUR',
  PF: 'XPF',
  TF: 'EUR',
  GA: 'XAF',
  GM: 'GMD',
  GE: 'GEL',
  DE: 'EUR',
  GH: 'GHC',
  GI: 'GIP',
  GR: 'EUR',
  GL: 'DKK',
  GD: 'XCD',
  GP: 'EUR',
  GU: 'USD',
  GT: 'GTQ',
  GN: 'GNF',
  GW: 'XOF',
  GY: 'GYD',
  HT: 'USD',
  HM: 'AUD',
  VA: 'EUR',
  HN: 'HNL',
  HK: 'HKD',
  HU: 'HUF',
  IS: 'ISK',
  IN: 'INR',
  ID: 'IDR',
  IR: 'IRR',
  IQ: 'IQD',
  IE: 'EUR',
  IL: 'ILS',
  IT: 'EUR',
  JM: 'JMD',
  JP: 'JPY',
  JO: 'JOD',
  KZ: 'KZT',
  KE: 'KES',
  KI: 'AUD',
  KP: 'KPW',
  KR: 'KRW',
  KW: 'KWD',
  KG: 'KGS',
  LA: 'LAK',
  LV: 'LVL',
  LB: 'LBP',
  LS: 'LSL',
  LR: 'LRD',
  LY: 'LYD',
  LI: 'CHF',
  LT: 'LTL',
  LU: 'EUR',
  MO: 'MOP',
  MK: 'MKD',
  MG: 'MGF',
  MW: 'MWK',
  MY: 'MYR',
  MV: 'MVR',
  ML: 'XOF',
  MT: 'MTL',
  MH: 'USD',
  MQ: 'EUR',
  MR: 'MRO',
  MU: 'MUR',
  YT: 'EUR',
  MX: 'MXV',
  FM: 'USD',
  MD: 'MDL',
  MC: 'EUR',
  MN: 'MNT',
  MS: 'XCD',
  MA: 'MAD',
  MZ: 'MZM',
  MM: 'MMK',
  NA: 'NAD',
  NR: 'AUD',
  NP: 'NPR',
  NL: 'EUR',
  AN: 'ANG',
  NC: 'XPF',
  NZ: 'NZD',
  NI: 'NIO',
  NE: 'XOF',
  NG: 'NGN',
  NU: 'NZD',
  NF: 'AUD',
  MP: 'USD',
  NO: 'NOK',
  OM: 'OMR',
  PK: 'PKR',
  PW: 'USD',
  // "PS": " ", remove blank currency
  PA: 'USD',
  PG: 'PGK',
  PY: 'PYG',
  PE: 'PEN',
  PH: 'PHP',
  PN: 'NZD',
  PL: 'PLN',
  PT: 'EUR',
  PR: 'USD',
  QA: 'QAR',
  RE: 'EUR',
  RO: 'ROL',
  RU: 'RUB',
  RW: 'RWF',
  SH: 'SHP',
  KN: 'XCD',
  LC: 'XCD',
  PM: 'EUR',
  VC: 'XCD',
  WS: 'WST',
  SM: 'EUR',
  ST: 'STD',
  SA: 'SAR',
  SN: 'XOF',
  CS: 'CSD',
  SC: 'SCR',
  SL: 'SLL',
  SG: 'SGD',
  SK: 'SKK',
  SI: 'SIT',
  SB: 'SBD',
  SO: 'SOS',
  ZA: 'ZAR',
  ES: 'EUR',
  LK: 'LKR',
  SD: 'SDD',
  SR: 'SRD',
  SJ: 'NOK',
  SZ: 'SZL',
  SE: 'SEK',
  CH: 'CHF',
  SY: 'SYP',
  TW: 'TWD',
  TJ: 'TJS',
  TZ: 'TZS',
  TH: 'THB',
  TL: 'USD',
  TG: 'XOF',
  TK: 'NZD',
  TO: 'TOP',
  TT: 'TTD',
  TN: 'TND',
  TR: 'TRL',
  TM: 'TMM',
  TC: 'USD',
  TV: 'AUD',
  UG: 'UGX',
  UA: 'UAH',
  AE: 'AED',
  GB: 'GBP',
  US: 'USD', // UNITED STATES
  UM: 'USD', // UNITED STATES MINOR OUTLYING ISLANDS
  UY: 'UYU',
  UZ: 'UZS',
  VU: 'VUV',
  VE: 'VEB',
  VN: 'VND',
  VG: 'USD',
  VI: 'USD',
  WF: 'XPF',
  EH: 'MAD',
  YE: 'YER',
  ZM: 'ZMK',
  ZW: 'ZWD',
})
// end init data

// *state
export const state = () => ({
  orders: [] as Order[],
  ordersBatchView: [] as Order[],
  ordersMawbView: [] as Order[],
  orderDetails: {
    order: {} as Order,
    orderItems: [] as OrderItem[],
    orderDutiesTaxes: {} as OrderDutiesTaxes,
    orderAllocationUpdates: [] as OrderAllocationUpdate[],
    nodeCalc: {} as NodeCalculator,
  } as OrderDetails,
  batchOrders: [] as BatchOrders[],
  meta: { ...initMeta } as Meta,
  orderTabView: 0,
  filterOrder: { ...filterOrderInit } as FilterOrders,
  filterBatch: { ...filterBatchInit } as FilterBatch,
  filterMawb: { ...filterMawbInit } as FilterMawb,
  tabBatchView: {
    selectedTransferCost: 1,
    step: 0,
  },
  mawbDocuments: { ...mawbDocumentsInit } as MawbDocuments,
  orderFlightSchedule: null as OrderFlightSchedule | null,
})
export type StateOrders = ReturnType<typeof state>
// end state

// mutations
type MC = StateOrders // as mutation context
export interface MutationsOrders extends MutationTree<StateOrders> {
  SET_ORDERS(state: MC, value: Order[]): void
  SET_ORDERS_BATCH_VIEW(state: MC, value: Order[]): void
  SET_ORDERS_MAWB_VIEW(state: MC, value: Order[]): void
  SET_ORDER_DETAILS(state: MC, value: OrderDetails): void
  SET_BATCH_ORDERS(state: MC, value: BatchOrders[]): void
  SET_NODE_CALCULATORS(state: MC, value: NodeCalculator): void
  SET_META(state: MC, value: Meta): void
  SET_ORDER_TAB_VIEW(state: MC, value: number): void
  RESET_ORDER_TAB_VIEW(state: MC): void
  SET_TAB_BTN_BATCHVIEW(state: MC, value: ButtonBatchViewPayload): void
  SET_FILTER_MAWB(state: MC, value: FilterMawb): void
  RESET_FILTER_MAWB(state: MC): void
  SET_MAWB_DOCUMENTS(state: MC, value: MawbDocuments): void
  SET_ORDER_FLIGHT_SCHEDULE(state: MC, value: OrderFlightSchedule): void
}
export const mutations: MutationsOrders = {
  SET_ORDERS: (state, value) => (state.orders = value),
  SET_ORDERS_BATCH_VIEW: (state, value) => (state.ordersBatchView = value),
  SET_ORDERS_MAWB_VIEW: (state, value) => (state.ordersMawbView = value),
  SET_ORDER_DETAILS: (state, value) => (state.orderDetails = value),
  SET_BATCH_ORDERS: (state, value) => (state.batchOrders = value),
  SET_NODE_CALCULATORS: (state, value) => (state.orderDetails.nodeCalc = value),
  SET_META: (state, value) => (state.meta = value),
  SET_ORDER_TAB_VIEW: (state, value) => (state.orderTabView = value),
  RESET_ORDER_TAB_VIEW: (state) => (state.orderTabView = 0),
  SET_FILTER_ORDERS: (state, value) => (state.filterOrder = value),
  RESET_FILTER_ORDERS: (state) => (state.filterOrder = { ...filterOrderInit }),
  SET_FILTER_BATCH: (state, value) => (state.filterBatch = value),
  RESET_FILTER_BATCH: (state) => (state.filterBatch = { ...filterBatchInit }),
  SET_TAB_BTN_BATCHVIEW: (state, value) => (state.tabBatchView = value),
  SET_FILTER_MAWB: (state, value) => (state.filterMawb = value),
  RESET_FILTER_MAWB: (state) => (state.filterMawb = { ...filterMawbInit }),
  SET_MAWB_DOCUMENTS: (state, value) => (state.mawbDocuments = value),
  SET_ORDER_FLIGHT_SCHEDULE: (state, value) =>
    (state.orderFlightSchedule = value),
}
// end mutations

// *actions
type AC = ActionContext<StateOrders, StateOrders> // as action context
export interface ActionsOrders extends ActionTree<StateOrders, StateOrders> {
  getOrders(
    ctx: AC,
    payload: { params: OrderGetPayload; isBatchView?: Boolean }
  ): void
  getBatchOrders(ctx: AC, payload: { params: BatchGetParams }): void
  getOrderDetails(ctx: AC, payload: string): void
  uploadOrders(ctx: AC, payload: UploadOrdersPayload): any
  getSelectedLabels(
    ctx: AC,
    payload: {
      data: {
        orderIds: string[]
        serviceType: string
      }
    }
  ): void
  getSelectedExports(ctx: AC, payload: ExportOrdersParams): void
  getNodeCalculators(ctx: AC): void
  setAwb(ctx: AC, payload: SetAwb): void
  getMawbOrders(ctx: AC, payload: { params: MAWBGetPayload }): void
  uploadMawbDocuments(
    ctx: AC,
    payload: { mawbId: string; filename: string; file: File }
  ): void
  getMawbDocuments(ctx: AC, payload: string): void
  postOrderCancellation(
    ctx: AC,
    payload: {
      data: {
        orderCodes: string[]
      }
    }
  ): void
  getOrderFlightSchedule(ctx: AC, payload: string): void
}
export const actions: ActionsOrders = {
  async getOrders({ commit }, { params, isBatchView }) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      const response = await axios.$get<PaginatedResponse<Order[]>>(
        '/api/clients/orders',
        {
          params,
        }
      )
      const { data, page, totalPage, totalCount } = response

      if (!data) throw response

      const meta = {
        page,
        totalPage,
        totalCount,
      } as Meta

      if (isBatchView) {
        commit('SET_ORDERS_BATCH_VIEW', data || [])
      }

      commit('SET_ORDERS', data || [])
      commit('SET_META', meta || { ...initMeta })

      return response
    } catch (error) {
      return error
    }
  },
  async getBatchOrders({ commit }, { params }) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      const response = await axios.$get<PaginatedResponse<Order[]>>(
        '/api/clients/orders/batch',
        {
          params,
        }
      )
      const { data, page, totalPage, totalCount } = response

      if (!data) throw response

      const meta = {
        page,
        totalPage,
        totalCount,
      }

      commit('SET_BATCH_ORDERS', data)
      commit('SET_META', meta)
    } catch (error) {
      return error
    }
  },
  async getOrderDetails({ commit }, id) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      const request = [
        axios.$get(`/api/clients/orders/${id}`),
        axios.$get(`/api/clients/orders/${id}/updates`),
        axios.$get(`api/clients/node-calculators/${id}`),
      ]
      const [
        responseOrderDetails,
        responseOrderUpdates,
        responseNodeCalculator,
      ] = await Promise.all(request)

      // Ensure status update is sorted by timestamp
      const sortedStatusUpdate = responseOrderUpdates?.allocationUpdates ?? []
      if (sortedStatusUpdate && sortedStatusUpdate.length) {
        // Sorting the 'partnerUpdates' array in each object based on the 'updateTimestamp'
        sortedStatusUpdate.forEach((item: any) => {
          item.externalTracking.mapPartnerUpdates?.sort((a: any, b: any) => {
            // Parsing the timestamps to Date objects for comparison
            const timestampA = new Date(a.updateTimestamp)
            const timestampB = new Date(b.updateTimestamp)
            // Sorting in descending order based on the timestamps
            return timestampB.getTime() - timestampA.getTime()
          })
        })

        // Sorting the main status update based on the last timestamp in each 'partnerUpdates' array
        sortedStatusUpdate.sort((a: any, b: any) => {
          const earliestTimestampA = new Date(
            a.externalTracking.mapPartnerUpdates?.[0].updateTimestamp
          )
          const earliestTimestampB = new Date(
            b.externalTracking.mapPartnerUpdates?.[0].updateTimestamp
          )
          return earliestTimestampB.getTime() - earliestTimestampA.getTime()
        })
      }

      const data = {
        order: responseOrderDetails ?? {},
        orderItems: responseOrderDetails?.items ?? [],
        orderDutiesTaxes: responseOrderDetails?.dutiesAndTaxes ?? {},
        orderAllocationUpdates: sortedStatusUpdate,
        nodeCalc: responseNodeCalculator ?? {},
      }

      commit('SET_ORDER_DETAILS', data)

      return data
    } catch (error) {
      return error
    }
  },
  async uploadOrders(_store, { data, type }) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      return await axios.$post(`/api/clients/orders/batch/${type}`, data)
    } catch (error) {
      return error
    }
  },
  async getSelectedLabels(_store, { data }) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      return await axios.$post(`api/clients/orders/labels`, data, {
        responseType: 'blob',
      })
    } catch (error) {
      return error
    }
  },
  async getSelectedExports(_store, { data, partnerId = '' }) {
    const axios = (this as unknown as Store<StateOrders>).$axios
    let url = `api/clients/orders-export`

    if (partnerId) {
      url = `api/clients/partners/${partnerId}/orders-export`
    }

    try {
      return await axios.$post(url, data, {
        responseType: 'blob',
      })
    } catch (error) {
      return error
    }
  },
  async getNodeCalculators({ commit }) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      const response = await axios.$get(`api/clients/node-calculators`)
      commit('SET_NODE_CALCULATORS', response)
      return response
    } catch (error) {
      return error
    }
  },
  // awb related
  async setAwb(_store, data) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      return await axios.$post('api/clients/orders/set-awb', data)
    } catch (error) {
      return error
    }
  },
  async getMawbOrders({ commit }, { params }) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      const response = await axios.$get<PaginatedResponse<Order[]>>(
        '/api/clients/orders',
        {
          params,
        }
      )
      const { data, page, totalPage, totalCount } = response

      if (!data) throw response

      const meta = {
        page,
        totalPage,
        totalCount,
      }

      commit('SET_MAWB_ORDERS', data)
      commit('SET_META', meta)
    } catch (error) {
      return error
    }
  },
  async uploadMawbDocuments(_store, { mawbId, filename, file }) {
    const axios = (this as unknown as Store<StateOrders>).$axios
    const formData = new FormData()

    try {
      formData.append(filename, file)

      return await axios.post(`/api/clients/mawb/${mawbId}`, formData, {
        headers: { 'Content-Type': 'multipart/form-data' },
      })
    } catch (error) {
      return error
    }
  },
  async getMawbDocuments({ commit }, mawbId) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      const response = await axios.$get(`/api/clients/mawb/${mawbId}`)

      const { awbNumber, awbUrl, hawbUrl, packingListUrl } = response.mawb[0]

      if (!awbNumber) throw response

      const mawbDocuments = { awbUrl, hawbUrl, packingListUrl }

      commit('SET_MAWB_DOCUMENTS', mawbDocuments)
    } catch (error) {
      return error
    }
  },
  async postOrderCancellation(_store, { data }) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      return await axios.$post(`/api/clients/orders/cancellations`, data)
    } catch (error) {
      return error
    }
  },
  async getOrderFlightSchedule({ commit }, b2bId) {
    const axios = (this as unknown as Store<StateOrders>).$axios

    try {
      const response = await axios.$get(
        `api/clients/b2b/orders/${b2bId}/freight-schedules`
      )

      commit('SET_ORDER_FLIGHT_SCHEDULE', response || null)
      return response
    } catch (error) {
      return error
    }
  },
}
// end actions
