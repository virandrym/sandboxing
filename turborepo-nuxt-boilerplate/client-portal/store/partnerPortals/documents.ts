// types -start
import { MutationTree, ActionTree, ActionContext, Store } from 'vuex'
import { CancelToken, AxiosResponse } from 'axios'
import {
  MawbPostPayload,
  MawbDocumentsResponse,
} from '~/types/partnerPortals/documents'
// types - end

// store state - start
export const state = () => ({
  freightDocuments: [] as MawbDocumentsResponse[],
  customsDocuments: [] as MawbDocumentsResponse[],
})
export type StateMawbDocuments = ReturnType<typeof state>
// store state - end

// mutations
type MC = StateMawbDocuments // as mutation context
export interface MutationsMawbDocuments
  extends MutationTree<StateMawbDocuments> {
  SET_MAWB_DOCUMENTS_FREIGHT_FORWARDER(
    state: MC,
    value: MawbDocumentsResponse[]
  ): void
  SET_MAWB_DOCUMENTS_CUSTOMS(state: MC, value: MawbDocumentsResponse[]): void
}

export const mutations: MutationsMawbDocuments = {
  SET_MAWB_DOCUMENTS_FREIGHT_FORWARDER: (state, value) =>
    (state.freightDocuments = value),
  SET_MAWB_DOCUMENTS_CUSTOMS: (state, value) =>
    (state.customsDocuments = value),
}

// actions - start
type AC = ActionContext<StateMawbDocuments, StateMawbDocuments>

export interface ActionsMAWBDocuments
  extends ActionTree<StateMawbDocuments, StateMawbDocuments> {
  attachMAWBDocumentsAPI(
    ctx: AC,
    payload: {
      data: { id: string; payload: MawbPostPayload }
      cancelToken?: CancelToken
    }
  ): void
  getMAWBDocuments(
    ctx: AC,
    payload: { mawbId: string; serviceType: string }
  ): void
  getSpecificMAWBDocument(
    ctx: AC,
    payload: { mawbId: string; documentId: string }
  ): Promise<AxiosResponse<any>>
}

export const actions: ActionsMAWBDocuments = {
  async attachMAWBDocumentsAPI(_store, { data: { id, payload }, cancelToken }) {
    const axios = (this as unknown as Store<AC>).$axios

    return await axios.post(`/api/clients/mawbs/${id}/documents`, payload, {
      cancelToken,
    })
  },

  async getMAWBDocuments({ commit }, { mawbId, serviceType }) {
    const axios = (this as unknown as Store<StateMawbDocuments>).$axios

    try {
      const response = await axios.$get(
        `/api/clients/mawbs/${mawbId}/documents`,
        { params: { serviceType } }
      )

      if (!response) throw Error
      const documentsData = response.documents

      commit(`SET_MAWB_DOCUMENTS_${serviceType}`, documentsData || [])
      return documentsData
    } catch (error) {
      return error
    }
  },

  async getSpecificMAWBDocument(_commit, { mawbId, documentId }) {
    const axios = (this as unknown as Store<StateMawbDocuments>).$axios

    try {
      const response = await axios.$get(
        `/api/clients/mawbs/${mawbId}/documents/${documentId}/download`
      )

      const { url } = response
      if (!url) throw response
      return url
    } catch (error) {
      return error
    }
  },
}
