// Interfaces
import { MutationTree, ActionTree, ActionContext, Store } from 'vuex'
import { AxiosResponse, CancelToken } from 'axios'
import { initMeta } from '../applications'
import { Meta, PaginatedResponse } from '~/types/applications'
import {
  IncomingOrder,
  FilterOrders,
  InputUpdateStatus,
  IncomingOrderDetails,
} from '~/types/partnerPortals/incomingOrders'

// *init data
export const filterOrderInit = {
  orderCode: '',
  batchCode: '',
  createdFrom: '',
  createdTo: '',
  destinationCountry: '',
  originCountry: '',
  serviceType: [],
  originPortId: '',
  destinationPortId: '',
  status: {
    Remarks: '',
    ServiceType: '',
    Status: '',
  },
  clientId: [],
} as FilterOrders
// end init data

// *state
export const state = () => ({
  incomingOrders: [] as IncomingOrder[] | [],
  meta: { ...initMeta } as Meta,
  filterOrders: filterOrderInit as FilterOrders,
  incomingOrderDetails: {} as IncomingOrderDetails,
})
export type StateIncomingOrders = ReturnType<typeof state>
// end state

// *mutations
type MC = StateIncomingOrders // as mutation context
export interface MutationsIncomingOrders
  extends MutationTree<StateIncomingOrders> {
  SET_CLIENT_CONNECTIONS(state: MC, Ivalue: IncomingOrder[]): void
  SET_META(state: MC, value: Meta): void
  SET_FILTER(state: MC, value: FilterOrders): void
  RESET_FILTER(state: MC): void
  SET_INCOMING_ORDER_DETAILS(state: MC, value: IncomingOrderDetails): void
}
export const mutations: MutationsIncomingOrders = {
  SET_CLIENT_CONNECTIONS: (state, value) => (state.incomingOrders = value),
  SET_META: (state, value) => (state.meta = value),
  SET_FILTER: (state, value) => (state.filterOrders = value),
  RESET_FILTER: (state) => (state.filterOrders = filterOrderInit),
  SET_INCOMING_ORDER_DETAILS: (state, value) =>
    (state.incomingOrderDetails = value),
}
// end mutations

// *actions
type AC = ActionContext<StateIncomingOrders, StateIncomingOrders> // as action context
export interface ActionsOrders
  extends ActionTree<StateIncomingOrders, StateIncomingOrders> {
  getIncomingOrders(
    ctx: AC,
    payload: { id: string; params: Meta; cancelToken: CancelToken }
  ): void
  getIncomingOrderDetails(
    ctx: AC,
    payload: { id: string; partnerId: string }
  ): void
  updateStatus(ctx: AC, payload: InputUpdateStatus): void
  updateStatusBulk(ctx: AC, payload: { file: File }): void
}
export const actions: ActionsOrders = {
  async getIncomingOrders({ commit }, { id, params, cancelToken }) {
    const axios = (this as unknown as Store<StateIncomingOrders>).$axios

    try {
      const response = await axios.$get<PaginatedResponse<IncomingOrder[]>>(
        `/api/clients/partners/${id}/orders`,
        {
          params,
          cancelToken,
        }
      )
      const { data, page, totalPage, totalCount } = response

      if (!data) throw response

      commit('SET_CLIENT_CONNECTIONS', data)
      commit('SET_META', {
        page,
        totalPage,
        totalCount,
      })

      return response
    } catch (error) {
      return error
    }
  },
  async getIncomingOrderDetails({ commit }, params) {
    const axios = (this as unknown as Store<StateIncomingOrders>).$axios

    try {
      const response = await axios.$get(
        `/api/clients/partners/${params?.partnerId ?? ''}/orders/${
          params?.id ?? ''
        }`
      )

      commit('SET_INCOMING_ORDER_DETAILS', response)

      return response
    } catch (error) {
      return error
    }
  },
  async updateStatus(_store, data) {
    const axios = (this as unknown as Store<StateIncomingOrders>).$axios

    try {
      return (await axios.post)<AxiosResponse<any>>(
        `/api/clients/update-order-status`,
        data
      )
    } catch (error) {
      return error
    }
  },
  async updateStatusBulk(_store, { file }) {
    const axios = (this as unknown as Store<StateIncomingOrders>).$axios
    const formData = new FormData()

    try {
      formData.append('file', file)
      return await axios.post(
        'api/clients/partners/updates-from-file',
        formData,
        {
          headers: { 'Content-Type': 'multipart/form-data' },
        }
      )
    } catch (error) {
      return error
    }
  },
}
// end actions
