# Nuxt Store Documentation

## Using Pinia Store

Pinia is a store library for Vue, it allows you to share a state across components/pages. It's like what vuex store does but more composition API friendly and type safe.

- [The documentation](https://pinia.vuejs.org).

```ts
// /store/counter.ts
import { defineStore } from 'pinia'
import { ref } from '@nuxtjs/composition-api'

export const useStore = defineStore('store-name', () => {
  // state
  const state = ref({
    count: 1,
  })

  // getters
  const getCount = computed(() => state.value.count)

  // actions
  const increment = () => {
    state.value.count + 1
  }

  return {
    state,
    getCount,
    increment,
  }
})
```

```vue
<!-- /components/Button.vue -->

<script setup>
import { useStore } from '@/stores/counter'

const counter = useStore()

counter.count++
// with autocompletion ✨
counter.$patch({ count: counter.count + 1 })
// or using an action instead
counter.increment()
</script>

<template>
  <!-- Access the state directly from the store -->
  <div>Current Count: {{ counter.count }}</div>
</template>
```

## Using Vuex Store

**This directory is not required, you can delete it if you don't want to use it.**

This directory contains your Vuex Store files.
Vuex Store option is implemented in the Nuxt.js framework.

Creating a file in this directory automatically activates the option in the framework.

More information about the usage of this directory:

- [The documentation](https://nuxtjs.org/guide/vuex-store).
- [Using composition API](https://composition-api.nuxtjs.org/packages/store).
- [Composition helpers](https://www.npmjs.com/package/vuex-composition-helpers)

### Composition Helpers

The store that we created is divided into several modules, the majority of which are divided based on the pages contained in the system (/pages directory). To make it easier to call the store we use [Composition helpers](https://www.npmjs.com/package/vuex-composition-helpers). With this library we can do mapping (for state, mutations, and also actions) easier and improve the readability of the code we create.

```ts
// store/order.ts

// state initializations
export const state = () => ({
  orders: [] as StateTypes,
})
export type StateOrders = ReturnType<typeof state> // state type

type MC = StateOrders // as mutation context
export interface MutationsOrders extends MutationTree<StateOrders> {
  SET_ORDERS(state: MC, value: PayloadMutations): void
} // mutations type
// mutations initializations
export const mutations: MutationsOrders = {
  SET_ORDERS: (state, value) => {
    // ...
  },
}

type AC = ActionContext<StateOrders, StateOrders> // as action context
export interface ActionsOrders extends ActionTree<StateOrders, StateOrders> {
  getOrders(ctx: AC, payload: PayloadActions): void
} // actions type
// actions initializations
export const actions: ActionsOrders = {
  async getOrders({ commit }, payload) {
    // ...
  },
}
```

```ts
// components/Components.vue

import { defineComponent } from '@nuxtjs/composition-api'
import { useState } from 'vuex-composition-helpers'
// types
import { StateOrders } from '~/store/orders'

export default defineComponent({
  setup() {
    /* Return specific state from specific store.
     *
     * @params {string} as specific module.
     * @params {string[]} as specific state from  store.
     */
    const { orders } = useState<StateOrders>('orders', ['orders'])

    return {
      orders,
    }
  },
})
```
