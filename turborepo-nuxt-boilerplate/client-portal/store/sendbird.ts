// Interfaces
import { MutationTree, ActionTree } from 'vuex'
import { ChatUser, ChatChannel, ChatRoom, ActionChat } from '~/types/sendbird'

export const state = () => ({
  chatUser: {} as ChatUser,
  chatChannel: {} as ChatChannel,
  chatChannelIncoming: [] as ChatChannel[],
  chatRoom: {} as ChatRoom[],
  menu: false,
  roomLoading: false,
  roomLoaded: false,
  newRoomLoaded: 0,
})

export type RootStateChat = ReturnType<typeof state>

export const mutations: MutationTree<RootStateChat> = {
  SET_USER: (state, value: ChatUser) => (state.chatUser = value),
  SET_CHANNEL: (state, value: ChatChannel) => (state.chatChannel = value),
  SET_CHANNEL_INCOMING: (state, value: ChatChannel[]) =>
    (state.chatChannelIncoming = value),
  SET_MENU: (state, value: boolean) => (state.menu = value),
  SET_ROOM: (state, value: ChatRoom[]) => (state.chatRoom = value),
  SET_ROOM_LOADING: (state, value: boolean) => (state.roomLoading = value),
  SET_ROOM_LOADED: (state, value: boolean) => (state.roomLoaded = value),
  SET_NEW_ROOM_LOADED: (state, value: number) => (state.newRoomLoaded += value),
}

export const actions: ActionTree<RootStateChat, RootStateChat> = {
  async getUserChat({ commit }) {
    try {
      const response = await this.$axios.$get(`/api/clients/chat/user`)

      commit('SET_USER', response)

      return response
    } catch (error) {
      return error
    }
  },
  async getChatChannel({ commit }, partnerID: string) {
    try {
      const response = await this.$axios.$get(
        `/api/clients/chat/channel/${partnerID}`
      )

      commit('SET_CHANNEL', response)

      return response
    } catch (error) {
      return error
    }
  },
  async getChatRoom({ commit }, { params, isCommit }) {
    try {
      const response = await this.$axios.$get(`/api/clients/thread`, {
        params,
      })

      if (isCommit) {
        commit('SET_ROOM', response.data)
        commit('SET_NEW_ROOM_LOADED', 1)
      }

      commit('SET_ROOM_LOADING', true)

      return response
    } catch (error) {
      return error
    }
  },
  async storeChatRoom(_state, { body }: { body: ActionChat }) {
    try {
      return await this.$axios.$post(`api/clients/thread`, body)
    } catch (error) {
      return error
    }
  },
  async rejoinThread(_state, { threadId }) {
    try {
      return await this.$axios.$post(`api/clients/thread/${threadId}/join`)
    } catch (error) {
      return error
    }
  },
  async getChatIncoming({ commit }) {
    try {
      const response = await this.$axios.$get(
        `/api/clients/chat/channel/incoming`
      )

      let custRes = response
      if (response && response.length > 0) {
        custRes = [...response].map((x: ChatChannel) => {
          return {
            ...x,
            logo: x.logo ?? '',
            name: x.client_name,
          }
        })
      }

      commit('SET_CHANNEL_INCOMING', custRes)

      return custRes
    } catch (error) {
      return error
    }
  },
  setMenu({ commit }, value: boolean) {
    try {
      commit('SET_MENU', value)

      return value
    } catch (error) {
      return error
    }
  },
  setRoomLoaded({ commit }, value: boolean) {
    try {
      commit('SET_ROOM_LOADED', value)

      return value
    } catch (error) {
      return error
    }
  },
}
