import * as LDClient from 'launchdarkly-js-client-sdk'
import Vue from 'vue'

// types - start
import type { MutationTree, ActionTree, ActionContext } from 'vuex'
import type { Pagination, Alert, Meta } from '~/types/applications'
import type {
  SuspensionAllowedActions,
  User,
} from '~/types/base/applications/login'
// types - end

// *init
const initPagination = {
  page: 1,
  itemsPerPage: 10,
  sortBy: [''],
  sortDesc: [true],
} as Pagination
export const initSimplePagination = {
  page: 1,
  itemsPerPage: 10,
} as Pagination
export const initUnlimitedPagination = {
  page: 1,
  itemsPerPage: 1000,
} as Pagination
export const initModalType = {
  confirm: false,
  form: false,
}
export const initDialogSettings = {
  loading: false,
  title: '',
  content: '',
  cancelText: '',
  submitText: '',
  submitColor: '',
  type: '',
}
export const initMeta = {
  page: 1,
  totalPage: 1,
  totalCount: 10,
} as Meta
// init

// *state
export const state = () => ({
  alert: {
    isShow: false,
    type: '',
    message: '',
  } as Alert,
  pagination: initPagination as Pagination,
  unlimitedPagination: initUnlimitedPagination as Pagination,
  suspensionAllowedActions: {} as SuspensionAllowedActions,
})
export type RootStateApplications = ReturnType<typeof state>
// end state

// *mutations
type MC = RootStateApplications // as mutation context
export interface MutationsApplications
  extends MutationTree<RootStateApplications> {
  SET_ALERT(state: MC, value: Alert): void
  RESET_ALERT(state: MC): void
  SET_PAGINATION(state: MC, value: Pagination): void
  RESET_PAGINATION(state: MC): void
  SET_SUSPENSION_ALLOWED(state: MC, value: SuspensionAllowedActions): void
}
export const mutations: MutationsApplications = {
  SET_ALERT: (state, value) => (state.alert = value),
  RESET_ALERT: (state) =>
    (state.alert = {
      isShow: false,
      type: '',
      message: '',
    }),
  SET_PAGINATION: (state, value) => (state.pagination = value),
  RESET_PAGINATION: (state) => (state.pagination = initPagination),
  SET_SUSPENSION_ALLOWED: (state, value) =>
    (state.suspensionAllowedActions = value),
}
// end mutations

// *actions
type AC = ActionContext<RootStateApplications, RootStateApplications> // as action context
export interface ActionsApplications
  extends ActionTree<RootStateApplications, RootStateApplications> {
  getFeatureFlags(ctx: AC, payload: string): Promise<boolean>
  getSuspensionFeature(ctx: AC): void
}
export const actions: ActionsApplications = {
  async getFeatureFlags(_store, flags: string) {
    const config = (this as unknown as Vue).$config
    const { launchDarklyKey } = config
    const user = { anonymous: true } // identified the users (if you don't want to define user can give anonymous value)
    const client = await LDClient.initialize(launchDarklyKey as string, user) // initialize the launch darkly client with env and user value

    try {
      await client.waitForInitialization() // waiting the inizialization until ready, for get the features-flage information later
      return await client.variation(flags, false) // returns the desired flag
    } catch (err) {
      // eslint-disable-next-line no-console
      console.log(err)
      return false
    }
  },
  getSuspensionFeature({ commit }) {
    const user = (this as unknown as Vue).$auth.$storage.getLocalStorage(
      'user'
    ) as User

    if (!user && Object.keys(user)?.length !== 0) {
      commit('SET_SUSPENSION_ALLOWED', {} as SuspensionAllowedActions)

      return {} as SuspensionAllowedActions
    }

    commit('SET_SUSPENSION_ALLOWED', user?.suspensionAllowedActions || {})
    return user?.suspensionAllowedActions || {}
  },
}
// end actions
