// Interfaces
import { MutationTree, ActionTree, ActionContext, Store } from 'vuex'
import { CancelToken } from 'axios'
import { StatusMapping, StatusChange } from '~/types/statusMapping'

export const statusChangeInit = {
  clientStatus: '',
  luwjistikStatusCode: '',
} as StatusChange

// *state
export const state = () => ({
  statusMappings: [] as StatusMapping[] | [],
  omsStatusMapping: false,
})
export type StateStatusMappings = ReturnType<typeof state>
// end state

// *mutations
type MC = StateStatusMappings // as mutation context
export interface MutationsStatusMappings
  extends MutationTree<StateStatusMappings> {
  SET_STATUS(state: MC, Ivalue: StatusMapping[]): void
  SET_OMS_STATUS(state: MC, value: boolean): void
}
export const mutations: MutationsStatusMappings = {
  SET_STATUS: (state, value) => (state.statusMappings = value),
  SET_OMS_STATUS: (state, value) => (state.omsStatusMapping = value),
}
// end mutations

// *actions
type AC = ActionContext<StateStatusMappings, StateStatusMappings> // as action context
export interface ActionsStatusMappings
  extends ActionTree<StateStatusMappings, StateStatusMappings> {
  getStatusMappings(ctx: AC, payload: { cancelToken: CancelToken }): void
}

export const actions: ActionsStatusMappings = {
  async getStatusMappings({ commit }, { cancelToken }) {
    const axios = (this as unknown as Store<StateStatusMappings>).$axios

    commit('SET_STATUS', [])
    try {
      const response = await axios.$get(`/api/clients/status-mappings`, {
        cancelToken,
      })

      if (!response) throw response

      const mapping = []

      // convert API response for easier use in baseTable component
      for (let i = 0; i < response.clientStatusMappings.length; i++) {
        const clientStatusMappings = response.clientStatusMappings[i]
        for (let j = 0; j < clientStatusMappings.mapping.length; j++) {
          if (clientStatusMappings.mapping[j].code !== '') {
            mapping.push({
              serviceTypeStatusMapping: clientStatusMappings.serviceType,
              code: clientStatusMappings.mapping[j].code,
              luwjistikStatus: clientStatusMappings.mapping[j].luwjistikStatus,
              clientStatusMapping: {
                id: clientStatusMappings.mapping[j].clientStatusMapping.id,
                clientStatus:
                  clientStatusMappings.mapping[j].clientStatusMapping
                    .clientStatus,
              },
            })
          }
        }
      }

      commit('SET_STATUS', mapping)
      commit('SET_OMS_STATUS', response.enableOMSStatusMapping)

      return mapping
    } catch (error) {
      return error
    }
  },
  async updateStatusMappings(_state, { body }) {
    try {
      return await this.$axios.$post(`api/clients/status-mappings`, body)
    } catch (error) {
      return error
    }
  },
  async setOmsEnabler(_state, { body }) {
    try {
      return await this.$axios.$post(
        `api/clients/status-mappings/oms-enabler`,
        body
      )
    } catch (error) {
      return error
    }
  },
}
// end actions
