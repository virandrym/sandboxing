import { defineStore } from 'pinia'
import { useFetchGuard } from '~/composables/base/fetch-guard'

// types - start
import type { CustomerOrderCodePrefixed } from '~/types/customers'
// types - end

export const useCustomersStore = defineStore('customers', () => {
  // init actions - start
  const getOrderCodePrefixed = async () =>
    await useFetchGuard
      .clients('/orders/set-code-format')
      .get()
      .json<{ orderCodeFormat: CustomerOrderCodePrefixed }>()
  const toggleOrderCodePrefixed = async ({
    path,
  }: {
    path: CustomerOrderCodePrefixed
  }) =>
    await useFetchGuard
      .clients(`/orders/set-code-format/${path}`)
      .post()
      .json<{ message: string }>()
  const actions = {
    getOrderCodePrefixed,
    toggleOrderCodePrefixed,
  }
  // init actions - end

  return {
    ...actions,
  }
})
