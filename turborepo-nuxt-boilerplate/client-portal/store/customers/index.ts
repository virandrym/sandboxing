import type { ActionContext, ActionTree, MutationTree, Store } from 'vuex'
import type { CancelToken } from 'axios'
import { initMeta } from '~/store/applications'

// types - start
import type { Meta, PaginatedResponse } from '~/types/applications'
import type { ActionCustomer, Customer } from '~/types/customers'
// types - end

// *state
export const state = () => ({
  customers: [] as Customer[] | [],
  customerDetails: {
    name: '',
  } as Customer,
  meta: { ...initMeta } as Meta,
})
export type StateCustomers = ReturnType<typeof state>
// end state

// *mutations
type MC = StateCustomers // as mutation context
export interface MutationsCustomers extends MutationTree<StateCustomers> {
  SET_CUSTOMERS(state: MC, Ivalue: Customer[]): void
  SET_CUSTOMER_DETAILS(state: MC, value: Customer): void
  SET_META(state: MC, value: Meta): void
}
export const mutations: MutationsCustomers = {
  SET_CUSTOMERS: (state, value) => (state.customers = value),
  SET_CUSTOMER_DETAILS: (state, value) => (state.customerDetails = value),
  SET_META: (state, value) => (state.meta = value),
}
// end mutations

// *actions
type AC = ActionContext<StateCustomers, StateCustomers> // as action context
export interface ActionsCustomers
  extends ActionTree<StateCustomers, StateCustomers> {
  getCustomers(
    ctx: AC,
    payload: { id: string; params: Meta; cancelToken: CancelToken }
  ): void
}
export const actions: ActionsCustomers = {
  async getCustomers({ commit }, { params, cancelToken }) {
    const axios = (this as unknown as Store<StateCustomers>).$axios

    try {
      const response = await axios.$get<PaginatedResponse<Customer[]>>(
        `/api/clients/customers`,
        {
          params,
          cancelToken,
        }
      )
      const { data, page, totalPage, totalCount } = response

      if (!data) throw response

      commit('SET_CUSTOMERS', data)
      commit('SET_META', {
        page,
        totalPage,
        totalCount,
      })

      return response
    } catch (error) {
      return error
    }
  },
  async getCustomerDetail({ commit }, { id }) {
    const axios = (this as unknown as Store<StateCustomers>).$axios

    try {
      const response = await axios.$get<Customer>(
        `/api/clients/customers/${id}`
      )

      commit('SET_CUSTOMER_DETAILS', response)

      return response
    } catch (error) {
      return error
    }
  },
  async createCustomer(
    _state,
    { body }: { customerId: string; body: ActionCustomer }
  ) {
    try {
      return await this.$axios.$post(`api/clients/customers`, body)
    } catch (error) {
      return error
    }
  },
}
// end actions
