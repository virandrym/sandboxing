// Interfaces
import { MutationTree, ActionTree, ActionContext, Store } from 'vuex'
import { CancelToken } from 'axios'
import { initMeta } from '~/store/applications'
import { Meta, PaginatedResponse } from '~/types/applications'
import { CustomerUser, PayloadCustomerUser } from '~/types/customers/users'

// *state
export const state = () => ({
  customerUsers: [] as CustomerUser[] | [],
  meta: { ...initMeta } as Meta,
})
export type StateCustomerUsers = ReturnType<typeof state>
// end state

// *mutations
type MC = StateCustomerUsers // as mutation context
export interface MutationsCustomers extends MutationTree<StateCustomerUsers> {
  SET_CLIENT_CONNECTIONS(state: MC, Ivalue: CustomerUser[]): void
  SET_META(state: MC, value: Meta): void
}
export const mutations: MutationsCustomers = {
  SET_CLIENT_CONNECTIONS: (state, value) => (state.customerUsers = value),
  SET_META: (state, value) => (state.meta = value),
}
// end mutations

// *actions
type AC = ActionContext<StateCustomerUsers, StateCustomerUsers> // as action context
export interface ActionsCustomers
  extends ActionTree<StateCustomerUsers, StateCustomerUsers> {
  getCustomerUsers(
    ctx: AC,
    payload: { id: string; params: Meta; cancelToken: CancelToken }
  ): void
}
export const actions: ActionsCustomers = {
  async getCustomerUsers({ commit }, { id, params, cancelToken }) {
    const axios = (this as unknown as Store<StateCustomerUsers>).$axios

    try {
      const response = await axios.$get<PaginatedResponse<CustomerUser[]>>(
        `/api/clients/customers/${id}/users`,
        {
          params,
          cancelToken,
        }
      )
      const { data, page, totalPage, totalCount } = response

      if (!data) throw response

      commit('SET_CLIENT_CONNECTIONS', data)
      commit('SET_META', {
        page,
        totalPage,
        totalCount,
      })

      return response
    } catch (error) {
      return error
    }
  },
  async createCustomerUser(
    _state,
    { customerId, body }: { customerId: string; body: PayloadCustomerUser }
  ) {
    try {
      return await this.$axios.$post(
        `api/clients/customers/${customerId}/users`,
        body
      )
    } catch (error) {
      return error
    }
  },
  async editCustomerUser(
    _state,
    {
      customerId,
      userId,
      body,
    }: { customerId: string; userId: string; body: PayloadCustomerUser }
  ) {
    try {
      return await this.$axios.$patch(
        `api/clients/customers/${customerId}/users/${userId}`,
        body
      )
    } catch (error) {
      return error
    }
  },
}
// end actions
