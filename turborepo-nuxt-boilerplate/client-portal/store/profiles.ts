import { MutationTree, ActionTree } from 'vuex'
import { Meta, Pagination, PaginatedResponse } from '~/types/applications'
import {
  Credential,
  GenerateCredential,
  ResetPasswordParams,
  Subscription,
  SubscriptionGetParams,
  SubscriptionPayload,
} from '~/types/profiles'

const initMeta = {
  page: 1,
  totalPage: 1,
  totalCount: 3,
} as Meta

export const state = () => ({
  credentials: {
    data: [] as Credential[],
    meta: { ...initMeta } as Meta,
  },
  subscriptions: {
    data: [] as Subscription[],
    meta: { ...initMeta } as Meta,
  },
})

export type RootStateProfiles = ReturnType<typeof state>

export const mutations: MutationTree<RootStateProfiles> = {
  SET_CREDENTIALS: (
    state,
    { data, meta }: { data: Credential[]; meta: Meta }
  ) =>
    (state.credentials = {
      data,
      meta,
    }),
  SET_SUBSCRIPTIONS: (
    state,
    { data, meta }: { data: Subscription[]; meta: Meta }
  ) =>
    (state.subscriptions = {
      data,
      meta,
    }),
  // SET_META: (state, value: Meta) => (state.meta = value),
}

export const actions: ActionTree<RootStateProfiles, RootStateProfiles> = {
  // credentials
  async getCredentials(
    { commit },
    {
      params,
    }: {
      params: Pagination
    }
  ) {
    try {
      const response = await this?.$axios?.$get<
        PaginatedResponse<Credential[]>
      >('/api/clients/credentials', {
        params,
      })
      const { data, page, totalPage, totalCount } = response

      commit('SET_CREDENTIALS', {
        data: data || [],
        meta: {
          page: page || 0,
          totalPage: totalPage || 0,
          totalCount: totalCount || 0,
        },
      })

      return response
    } catch (error) {
      return error
    }
  },
  async revokeKey(_store, { id }: { id: string }) {
    try {
      const response = await this?.$axios?.$patch(
        `/api/clients/credentials/${id}`
      )

      return response
    } catch (error) {
      return error
    }
  },
  async generateKey(
    _store,
    {
      body,
    }: {
      body: GenerateCredential
    }
  ) {
    try {
      const response = await this.$axios.$post('api/clients/credentials', body)

      return response
    } catch (error) {
      return error
    }
  },
  // subscriptions
  async getSubscriptions(
    { commit },
    { params }: { params: SubscriptionGetParams }
  ) {
    try {
      const response = await this.$axios.$get<
        PaginatedResponse<Subscription[]>
      >('api/clients/event-subscriptions/list', { params })
      const { data, page, totalPage, totalCount } = response

      commit('SET_SUBSCRIPTIONS', {
        data: data || [],
        meta: {
          page: page || 0,
          totalPage: totalPage || 0,
          totalCount: totalCount || 0,
        },
      })

      return response
    } catch (error) {
      return error
    }
  },
  async createSubscription(_state, { body }: { body: SubscriptionPayload }) {
    try {
      return await this.$axios.$post('api/clients/event-subscriptions', body)
    } catch (error) {
      return error
    }
  },
  async unsubscribeEvent(_store, { id }: { id: string }) {
    try {
      return await this.$axios.$delete(`/api/clients/event-subscriptions/${id}`)
    } catch (error) {
      return error
    }
  },
  // password
  async resetPassword(
    _store,
    {
      body,
    }: {
      body: ResetPasswordParams
    }
  ) {
    try {
      const response = await this.$axios.$patch('api/user/password', body)

      return response
    } catch (error) {
      return error
    }
  },
}
