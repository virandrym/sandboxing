// Interfaces
import { MutationTree, ActionTree } from 'vuex'
import { CancelToken } from 'axios'
import { Meta } from '~/types/applications'
import {
  FlightList,
  PostReservePayload,
  PostRequestPayload,
  ActiveList,
  BookingList,
  ParamsGetClientBookings,
} from '~/types/lFreight/lFreight'

export const initPostFlight = {
  flightId: '',
  weight: null,
} as unknown as PostReservePayload

export const initPostRequest = {
  originPortID: '',
  destinationPortID: '',
  bookingDate: new Date(),
  weight: null,
} as unknown as PostRequestPayload

export const state = () => ({
  lFreight: [] as FlightList[],
  activeList: [] as ActiveList[],
  bookingList: [] as BookingList[],
  meta: {
    page: 1,
    totalPage: 1,
    totalCount: 10,
  } as Meta,
})

export type RootStateLFreight = ReturnType<typeof state>
// mutations

export const mutations: MutationTree<RootStateLFreight> = {
  SET_DATA: (state, value: FlightList[]) => {
    state.lFreight = value ?? []
  },
  SET_META: (state, value: Meta) => (state.meta = { ...state.meta, ...value }),
  SET_ACTIVE_DATA: (state, value: ActiveList[]) => {
    state.activeList = value ?? []
  },
  SET_BOOKING_DATA: (state, value: BookingList[]) => {
    state.bookingList = value ?? []
  },
}

export const actions: ActionTree<RootStateLFreight, RootStateLFreight> = {
  async getLFreights({ commit }) {
    try {
      const response = await this.$axios.$get('/api/clients/lfreight/flights')

      if (!response?.items) throw response

      commit('SET_DATA', response?.items)
      // commit('SET_META', meta);

      return response
    } catch (error) {
      return error
    }
  },
  async postReserveFlight(
    _state,
    {
      data,
      cancelToken,
    }: { data: PostReservePayload; cancelToken: CancelToken }
  ) {
    try {
      return await this.$axios.post(
        'api/clients/lfreight/reserve-flights',
        data,
        {
          params: data,
          cancelToken,
        }
      )
    } catch (error) {
      return error
    }
  },
  async getActiveLanes({ commit }) {
    try {
      const response = await this.$axios.$get(
        '/api/clients/lfreight/active-lanes'
      )

      if (!response?.data) throw response

      commit('SET_ACTIVE_DATA', response?.data)

      return response
    } catch (error) {
      return error
    }
  },
  async postRequestFlight(_state, { data }: { data: PostRequestPayload }) {
    try {
      return await this.$axios.post(
        '/api/clients/lfreight/request-flights',
        data
      )
    } catch (error) {
      return error
    }
  },
  async getBookings(
    { commit },
    { params }: { params: ParamsGetClientBookings }
  ) {
    try {
      const response = await this.$axios.$get(
        `/api/clients/lfreight/my-bookings?${params}`
      )

      const { data, page, totalPage, totalCount } = response
      if (!data) throw response

      commit('SET_BOOKING_DATA', data)
      commit('SET_META', {
        page,
        totalPage,
        totalCount,
      })

      return response
    } catch (error) {
      return error
    }
  },
  async cancelBookingRequest(_state, { data }: { data: string }) {
    try {
      return await this.$axios.$patch(`/api/clients/lfreight/${data}/cancel`)
    } catch (error) {
      return error
    }
  },
}
