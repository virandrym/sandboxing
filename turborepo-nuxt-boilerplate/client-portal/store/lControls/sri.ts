//  types
import { ActionContext, ActionTree, MutationTree, Store } from 'vuex'
import { CancelToken, AxiosResponse } from 'axios'
import { SRI, SRIDeletePayload, SRIPostPayload } from '~/types/lControl/sri'
//  end

//  state
export const state = () => ({
  sriLanes: [] as SRI[],
})
export type StateSRILanes = ReturnType<typeof state>
export type StoreSRILanes = Store<StateSRILanes>
//  end

//  mutations
type MC = StateSRILanes // as mutation context
export interface MutationsSRILControl extends MutationTree<MC> {
  SET_SRI_LANES(state: MC, value: SRI[]): void
}
export const mutations: MutationsSRILControl = {
  SET_SRI_LANES: (state, value) => (state.sriLanes = value),
}
//  end

//  actions
type AC = ActionContext<StateSRILanes, StateSRILanes> // as action context
export interface ActionsSRILControl
  extends ActionTree<StateSRILanes, StateSRILanes> {
  getSRILanesAPI(ctx: AC): Promise<void>
  postSRILanesAPI(
    ctx: AC,
    payload: { data: SRIPostPayload; cancelToken: CancelToken }
  ): Promise<AxiosResponse<any>>
  deleteSRILanesAPI(
    ctx: AC,
    payload: { data: SRIDeletePayload; cancelToken: CancelToken }
  ): Promise<AxiosResponse<any>>
}
export const actions: ActionsSRILControl = {
  async getSRILanesAPI({ commit }) {
    const { $axios: axios } = this as unknown as StoreSRILanes
    const data = await axios.$get<SRI[]>('/api/clients/sri/lanes')

    commit('SET_SRI_LANES', data || [])
  },
  async postSRILanesAPI(_store, { data, cancelToken }) {
    const { $axios: axios } = this as unknown as StoreSRILanes

    return await axios.post(`/api/clients/sri/lanes`, data, {
      cancelToken,
    })
  },
  async deleteSRILanesAPI(_store, { data, cancelToken }) {
    const { $axios: axios } = this as unknown as StoreSRILanes

    return await axios.delete(`/api/clients/sri/lanes`, {
      data,
      cancelToken,
    })
  },
}
//  end
