// types
import { ActionContext, ActionTree, MutationTree, Store } from 'vuex'
import { AxiosError, AxiosResponse, CancelToken } from 'axios'
import { Meta } from '~/types/applications'
import { LControl, LTimizer } from '~/types/lControl'

// *state
export const state = () => ({
  lControls: [] as LControl[] | [],
  bobModular: {} as LTimizer,
  meta: {
    page: 1,
    totalPage: 1,
    totalCount: 8,
  } as Meta,
})
export type StateLControls = ReturnType<typeof state>
// end state

// *mutations
type MC = StateLControls // as mutation context
export interface MutationsLControls extends MutationTree<StateLControls> {
  SET_LCONTROLS(state: MC, value: LControl[]): void
  SET_BOB_MODULAR(state: MC, value: LTimizer): void
  SET_META(state: MC, value: Meta): void
}
export const mutations: MutationsLControls = {
  SET_LCONTROLS: (state, value) => (state.lControls = value),
  SET_BOB_MODULAR: (state, value) => (state.bobModular = value),
  SET_META: (state, value) => (state.meta = value),
}
// end mutations

// *actions
type AC = ActionContext<StateLControls, StateLControls> // as action context
export interface ActionsLControls
  extends ActionTree<StateLControls, StateLControls> {
  getAllLControl(
    ctx: AC,
    payload: { country: string; serviceType: string; cancelToken?: CancelToken }
  ): void
  updateLControl(ctx: AC, payload: LControl): any
  addBOB(ctx: AC, payload: string): void
  deleteBOB(ctx: AC, payload: string): void
  doGetBobModular(
    ctx: AC,
    payload: { country: string; cancelToken?: CancelToken }
  ): void
  doPostBobModular(
    ctx: AC,
    payload: { country: string; serviceType: string; cancelToken?: CancelToken }
  ): Promise<AxiosResponse<any>>
  doDeleteBobModular(
    ctx: AC,
    payload: { country: string; serviceType: string; cancelToken?: CancelToken }
  ): Promise<AxiosResponse<any>>
}
export const actions: ActionsLControls = {
  async getAllLControl({ commit }, { country, serviceType, cancelToken }) {
    const axios = (this as unknown as Store<StateLControls>).$axios

    try {
      const data = await axios.$get<LControl>('/api/clients/lcontrol', {
        params: {
          country,
          service_type: serviceType,
        },
        cancelToken,
      })

      commit('SET_LCONTROLS', data || [])

      return data
    } catch (error) {
      return error
    }
  },
  async updateLControl(
    _store,
    data
  ): Promise<AxiosResponse<any, any> | AxiosError<any, any>> {
    const axios = (this as unknown as Store<StateLControls>).$axios

    try {
      return await axios.$put('/api/clients/lcontrol', data)
    } catch (error) {
      return error as AxiosError
    }
  },
  // bob general
  async addBOB(_store, country) {
    const axios = (this as unknown as Store<StateLControls>).$axios

    try {
      const dataUser = JSON.parse(localStorage.getItem('auth.user') as any)
      return await axios?.$post(
        `/api/clients/${dataUser?.clientId}/use-bob?country=${country}`
      )
    } catch (error) {
      return error
    }
  },
  async deleteBOB(_store, country) {
    const axios = (this as unknown as Store<StateLControls>).$axios

    try {
      const dataUser = JSON.parse(localStorage.getItem('auth.user') as any)
      return await axios?.$delete(
        `/api/clients/${dataUser?.clientId}/use-bob?country=${country}`
      )
    } catch (error) {
      return error
    }
  },
  // bob modular
  async doGetBobModular({ commit }, { country, cancelToken }) {
    const axios = (this as unknown as Store<StateLControls>).$axios

    try {
      const data = await axios.$get<LTimizer>('/api/clients/ltimizer', {
        params: {
          country,
        },
        cancelToken,
      })

      commit('SET_BOB_MODULAR', data)

      return data
    } catch (error) {
      return error
    }
  },
  async doPostBobModular(_store, { country, serviceType, cancelToken }) {
    const axios = (this as unknown as Store<StateLControls>).$axios

    return await axios?.post(`/api/clients/ltimizer`, null, {
      params: {
        country,
        serviceType,
      },
      cancelToken,
    })
  },
  async doDeleteBobModular(_store, { country, serviceType, cancelToken }) {
    const axios = (this as unknown as Store<StateLControls>).$axios

    return await axios?.delete(`/api/clients/ltimizer`, {
      params: {
        country,
        serviceType,
      },
      cancelToken,
    })
  },
}
// end actions
