// Interfaces
import { MutationTree, ActionTree, ActionContext, Store } from 'vuex'
import { CancelToken } from 'axios'
import {
  GetPortsPayload,
  ServiceType,
  Zone,
  CountryCode,
  Ports,
  Statuses,
  CustomerPaginated,
  ClientsPaginated,
  GetCountriesPayload,
  GetZonesPayload,
  TerminalStatus,
  GetServiceableLanesPayload,
  ServiceableB2BLanes,
} from '~/types/filters'
import { PaginatedResponse, PaginationPayload } from '~/types/applications'

// *state
export const state = () => ({
  countryCodes: [] as CountryCode[],
  zones: [] as Zone[],
  serviceTypes: [] as ServiceType[],
  ports: {} as Ports,
  statuses: [] as Statuses[],
  loaded: false as Boolean,
  customers: {} as CustomerPaginated,
  clients: {
    onPartner: {} as ClientsPaginated,
  },
  terminalStatus: [] as TerminalStatus[],
  serviceableB2BLanes: [] as ServiceableB2BLanes[],
})
export type StateFilters = ReturnType<typeof state>
// end state

// *mutations
type MC = StateFilters // as mutation context
export interface MutationsFilters extends MutationTree<StateFilters> {
  SET_COUNTRY_CODE(state: MC, value: CountryCode[]): void
  SET_ZONES(state: MC, value: Zone[]): void
  SET_SERVICE_TYPES(state: MC, value: ServiceType[]): void
  SET_PORTS(state: MC, value: Ports): void
  SET_STATUSES(state: MC, value: Statuses[]): void
  SET_LOADED(state: MC, value: Boolean): void
  SET_CUSTOMERS(state: MC, value: CustomerPaginated): void
  SET_CLIENTS_ON_PARTNER(state: MC, value: ClientsPaginated): void
  SET_TERMINAL_STATUS(state: MC, value: TerminalStatus[]): void
  SET_SERVICEABLE_B2B_LANES(state: MC, value: ServiceableB2BLanes[]): void
}
export const mutations: MutationsFilters = {
  SET_COUNTRY_CODE: (state, value) => (state.countryCodes = value),
  SET_ZONES: (state, value) => (state.zones = value),
  SET_SERVICE_TYPES: (state, value) => (state.serviceTypes = value),
  SET_PORTS: (state, value) => (state.ports = value),
  SET_STATUSES: (state, value) => (state.statuses = value),
  SET_LOADED: (state, value) => (state.loaded = value),
  SET_CUSTOMERS: (state, value) => (state.customers = value),
  SET_CLIENTS_ON_PARTNER: (state, value) => (state.clients.onPartner = value),
  SET_TERMINAL_STATUS: (state, value) => (state.terminalStatus = value),
  SET_SERVICEABLE_B2B_LANES: (state, value) =>
    (state.serviceableB2BLanes = value),
}
// end mutations

// *actions
type AC = ActionContext<StateFilters, StateFilters> // as action context
export interface ActionsFilters extends ActionTree<StateFilters, StateFilters> {
  getOnce(ctx: AC): void
  getCountryCodes(
    ctx: AC,
    payload: {
      params: GetCountriesPayload
      cancelToken?: CancelToken
    }
  ): void
  getZones(ctx: AC, payload: { params: GetZonesPayload }): void
  getServiceTypes(ctx: AC): void
  getPorts(ctx: AC, payload: GetPortsPayload): void
  getStatuses(ctx: AC): void
  getCustomers(
    ctx: AC,
    payload: { params: PaginationPayload; cancelToken?: CancelToken }
  ): void
  getClientsOnPartner(
    ctx: AC,
    payload: {
      params: PaginationPayload
      partnerId: string
      cancelToken?: CancelToken
    }
  ): void
  getB2BServiceableLanes(
    ctx: AC,
    payload: {
      params: GetServiceableLanesPayload
      cancelToken?: CancelToken
    }
  ): void
}
export const actions: ActionsFilters = {
  async getOnce({ dispatch }) {
    await dispatch('getZones', { params: {} })
    await dispatch('getServiceTypes')
  },
  async getCountryCodes({ commit }, { params, cancelToken }) {
    const axios = (this as unknown as Store<StateFilters>).$axios
    const customUtils = (this as unknown as Store<StateFilters>).$customUtils

    try {
      const paramsFormatted = customUtils.setURLParams({
        active_only: params?.isActive,
      })
      const response = (await axios.$get(`/api/data/country-codes`, {
        params: paramsFormatted,
        cancelToken,
      })) as Record<string, string>

      let temp = [] as CountryCode[]

      if (response) {
        temp = Object.entries(response).map((el) => {
          return {
            name: el[0],
            value: el[1],
          }
        })
      }

      commit('SET_COUNTRY_CODE', temp)

      return response
    } catch (error) {
      return error
    }
  },
  async getZones({ commit }, { params }) {
    const axios = (this as unknown as Store<StateFilters>).$axios

    try {
      const response = await axios.$get(`/api/clients/zones`, {
        params,
      })
      const { zones } = response

      if (!zones) {
        commit('SET_ZONES', [])
        throw response
      }

      commit('SET_ZONES', zones)

      return response
    } catch (error) {
      return error
    }
  },
  async getServiceTypes({ commit }) {
    const axios = (this as unknown as Store<StateFilters>).$axios

    try {
      const response = await axios.$get('/api/clients/serviceTypes')
      const { serviceTypes } = response

      if (!serviceTypes) throw response

      commit('SET_SERVICE_TYPES', serviceTypes)

      return response
    } catch (error) {
      return error
    }
  },
  async getPorts({ commit }, { params, country, cancelToken }) {
    const axios = (this as unknown as Store<StateFilters>).$axios

    try {
      const uri = country ? `?country=${country ?? ''}` : ''
      const response = await axios.$get(`/api/data/ports${uri}`, {
        params,
        cancelToken,
      })

      commit('SET_PORTS', response)

      return response
    } catch (error) {
      return error
    }
  },

  async getStatuses({ commit }) {
    const axios = (this as unknown as Store<StateFilters>).$axios

    try {
      const response = await axios.$get(`/api/data/statuses`)

      commit('SET_STATUSES', response)

      return response
    } catch (error) {
      return error
    }
  },
  async getCustomers({ commit }, { params, cancelToken }) {
    const axios = (this as unknown as Store<StateFilters>).$axios

    try {
      const response = await axios.$get(`/api/clients/customers`, {
        params,
        cancelToken,
      })
      commit('SET_CUSTOMERS', response)

      return response
    } catch (error) {
      return error
    }
  },
  // get clients that connected to partners and have placed at least one order with the partner
  async getClientsOnPartner({ commit }, { params, partnerId, cancelToken }) {
    const axios = (this as unknown as Store<StateFilters>).$axios

    try {
      const response = await axios.$get<PaginatedResponse<ClientsPaginated>>(
        `/api/clients/partners/${partnerId}/clients`,
        {
          params,
          cancelToken,
        }
      )
      commit('SET_CLIENTS_ON_PARTNER', response)

      return response
    } catch (error) {
      return error
    }
  },
  getTerminalStatus({ commit }) {
    commit('SET_TERMINAL_STATUS', [
      {
        text: 'Yes',
        value: 'true',
      },
      {
        text: 'No',
        value: 'false',
      },
    ])
  },

  // get clients that connected to partners and have placed at least one order with the partner
  async getB2BServiceableLanes({ commit }, { params, cancelToken }) {
    const axios = (this as unknown as Store<StateFilters>).$axios

    try {
      const response = await axios.$get(`api/clients/b2b/lanes`, {
        params,
        cancelToken,
      })

      commit('SET_SERVICEABLE_B2B_LANES', response.data)

      return response
    } catch (error) {
      // if no lanes are found, it returns err status 404 null. set data as empty array so no data is available in the dropdown
      commit('SET_SERVICEABLE_B2B_LANES', [])
      return error
    }
  },
}
// end actions
