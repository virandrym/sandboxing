// types
import { MutationTree, ActionTree, Store, ActionContext } from 'vuex'

// * init state
export const state = () => ({
  clientAnalytic: '',
  partnerAnalytics: '',
})
export type RootStateDashboards = ReturnType<typeof state>
// end init state

// * init mutations
type MC = RootStateDashboards
export interface MutationDashboards extends MutationTree<RootStateDashboards> {
  SET_CLIENT_ANALYTIC(state: MC, value: string): void
  SET_PARTNER_ANALYTIC(state: MC, value: string): void
}
export const mutations: MutationDashboards = {
  SET_CLIENT_ANALYTIC: (state, value) => (state.clientAnalytic = value),
  SET_PARTNER_ANALYTIC: (state, value) => (state.partnerAnalytics = value),
}
// end init mutations

// * init actions
type S = Store<RootStateDashboards>
type AC = ActionContext<RootStateDashboards, RootStateDashboards>
export interface ActionDashboards
  extends ActionTree<RootStateDashboards, RootStateDashboards> {
  getClientAnalytics(ctx: AC): void
  getPartnerAnalytics(ctx: AC): void
}
export const actions: ActionDashboards = {
  async getClientAnalytics({ commit }) {
    const axios = (this as unknown as S).$axios

    try {
      const response = await axios.$get<{ endpoint: string }>(
        '/api/clients/analytics/metabase/clients'
      )
      const { endpoint } = response

      if (!endpoint) throw response

      commit('SET_CLIENT_ANALYTIC', endpoint)
    } catch (error) {
      return error
    }
  },
  async getPartnerAnalytics({ commit }) {
    const axios = (this as unknown as S).$axios

    try {
      const response = await axios.$get<{ endpoint: string }>(
        '/api/clients/analytics/metabase/partners'
      )
      const { endpoint } = response

      if (!endpoint) throw response

      commit('SET_PARTNER_ANALYTIC', endpoint)
    } catch (error) {
      return error
    }
  },
}
// end init actions
