import minifyTheme from 'minify-css-string'

require('dotenv').config()

const development = process.env.NODE_ENV === 'development'
// const production = process.env.NODE_ENV === 'production'

export default {
  // Rendering property : https://nuxtjs.org/docs/features/rendering-modes
  ssr: false,
  // Server property: https://nuxtjs.org/docs/configuration-glossary/configuration-server#the-server-property
  server: {
    port: process.env.PORT,
    host: process.env.HOST,
  },

  // Loading property: https://nuxtjs.org/docs/features/loading
  loading: false,
  // loading: {
  //   color: '#2196F3',
  //   failedColor: '#F44336',
  //   height: '4px',
  // },

  router: {
    middleware: 'resetState',
  },

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Client Portal',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Luwjistik connects the dots across supply chains. Powered by robust APIs, our integrated platform enables direct access to network partners, complete transparency and greater ease of cross border movements.​',
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content:
          'luwjistik, client, client portal, supply chains, connects, integrated',
      },
      { hid: 'author', name: 'author', content: 'Luwjistik' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      {
        rel: 'preconnect',
        href: 'https://fonts.gstatic.com',
        crossorigin: true,
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,700;1,300;1,400;1,500;1,700&display=swap',
      },
    ],
    script: [
      {
        src: 'https://polyfill.io/v3/polyfill.min.js?features=IntersectionObserver',
        body: true,
      },
      // {
      //   src: `https://www.googletagmanager.com/gtag/js?id=${process.env.VUE_APP_GOOGLE_ANALYTICS}`,
      //   async: true,
      // },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/scss/main.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/vee-validate.js',
    '~/plugins/custom_utils.ts',
    { src: '~plugins/vue-carousel-3d', ssr: false },
    // '~/plugins/gtag.js',
    // '~/plugins/vue-gtag.client.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/composition-api/module',
    '@vueuse/nuxt',
    // https://github.com/harlan-zw/nuxt-webpack-optimisations
    'nuxt-webpack-optimisations',
    '@nuxtjs/svg',
    '@nuxtjs/date-fns',
    '@nuxtjs/google-analytics',
    // https://pinia.vuejs.org/ssr/nuxt.html#nuxt-2-without-bridge
    ['@pinia/nuxt', { disableVuex: false }],
  ],

  // GOOGLE ANALYTICS: https://google-analytics.nuxtjs.org
  googleAnalytics: {
    id: process.env.VUE_APP_GOOGLE_ANALYTICS,
    pageTracking: true,
    autoTracking: {
      screenview: true,
      shouldRouterUpdate(to, from) {
        // Here I'm allowing tracking only when
        // next route path is not the same as the previous
        return to.path !== from.path
      },
    },
    fields: {
      user: '',
    },
    debug: {
      // dev mode on
      // enabled: development,
      enabled: false,

      // dev mode on
      // sendHitTask: true,
      sendHitTask: !development,
    },
    // dev mode on
    // dev: development,
    dev: false,
    checkDuplicatedScript: true,
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/auth-next',
    '@nuxt/image',
    '@nuxtjs/sentry',
    ['nuxt-storm', { nested: true, alias: true }],
  ],

  // Auth modules configuration: https://auth.nuxtjs.org
  auth: {
    strategies: {
      local: {
        token: {
          property: 'token',
          required: true,
          type: '',
          maxAge: 86400,
        },
        user: {
          property: false,
          autoFetch: false,
        },
        endpoints: {
          login: { url: '/api/login', method: 'post' },
          user: false,
          logout: false,
        },
      },
    },
  },

  // Nuxt image configuration: https://image.nuxtjs.org
  image: {},

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    options: { minifyTheme },
    customVariables: ['~/assets/scss/variables.scss'],
    treeShake: true,
    optionsPath: './vuetify.config.ts',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['vee-validate/dist/rules', 'vuex-composition-helpers'],
    loaders: {
      vue: {
        prettify: false,
      },
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    proxy: development,
    baseURL: development ? '/' : process.env.API_URL,
    browserBaseURL: '/',
  },

  proxy: {
    '/api/': process.env.API_URL,
  },

  sentry: {
    dsn: process.env.SENTRY_DSN || '',
    publishRealse: true,
    config: {
      release: process.env.RELEASE || '',
    },
    tracesSampleRate: 1.0,
    replaysSessionSampleRate: 0.1,
    replaysOnErrorSampleRate: 1.0,
  },

  webpackOptimisations: {
    // hard source is the riskiest, if you have issues don't enable it
    hardSourcePlugin: development,
    parallelPlugin: development,
  },

  publicRuntimeConfig: {
    sendBirdKey: process.env.VUE_APP_SENDBIRD_KEY,
    sendBirdToken: process.env.VUE_APP_SENDBIRD_TOKEN,
    googleAnalytics: {
      id: process.env.VUE_APP_GOOGLE_ANALYTICS,
    },
    launchDarklyKey: process.env.LAUNCH_DARKLY_KEY_CLIENT,
    zendeskKey: process.env.ZENDESK_KEY,
  },
  env: {
    VUE_APP_GOOGLE_ANALYTICS: process.env.VUE_APP_GOOGLE_ANALYTICS,
  },
}
