FROM node:16.13.2-alpine3.14 as builder

RUN mkdir -p /app

ADD package.json /app/package.json
ADD package-lock.json /app/package-lock.json

WORKDIR /app
RUN npm cache verify
RUN npm ci

