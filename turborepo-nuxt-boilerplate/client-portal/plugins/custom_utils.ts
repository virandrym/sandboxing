import { defineNuxtPlugin } from '@nuxtjs/composition-api'

// types - start
import type { AxiosResponse } from 'axios'
import type { ColorServiceType, ServiceType, Statuses } from '~/types/filters'
import type { ErrorAPI } from '~/types/applications'
import { delay, download } from '~/static/js/multiDownload'
// types - end

interface CustomUtils {
  sortServiceTypes: (data: ServiceType[]) => ServiceType[]
  setServiceType: (data: string) => string
  setColorServiceType: (data: string, type?: string) => ColorServiceType
  sortStatuses: (data: Statuses[]) => Statuses[]
  formattingStatuses: (data: Statuses[]) => Statuses[]
  capitalize: (text: string) => string
  setAddress: (data: string[]) => string
  setRuleType: (data: string) => string
  setURLParams: (data: Object, separateKeys?: boolean) => URLSearchParams
  multiDownload: (urls: string[]) => Promise<void>
  setCurrency: ({
    currency,
    price,
  }: {
    currency: string
    price: number | string
  }) => string
  getErrorMessages: (
    error: { response: AxiosResponse<ErrorAPI> | Error },
    defaultMessage: string
  ) => string[] | string
  getStagingVersion: () => 'dev' | 'sandbox' | 'prod'
  getDimensions: ({
    length,
    width,
    height,
  }: {
    length: number
    width: number
    height: number
  }) => string
  formatBytes: (bytes: number, decimals: number) => string
  previewFileUpload: (file: File) => void
  isFile: (file: File) => boolean
  getPathMeta: (
    path: string
  ) => undefined | { path: string; file: string; name: string; ext: string }
  strToBool: (data: boolean | string) => boolean
}
declare module 'vue/types/vue' {
  // this.$customUtils inside Vue components
  interface Vue {
    $customUtils: CustomUtils
  }
}

declare module '@nuxt/types' {
  // nuxtContext.app.$customUtils inside asyncData, fetch, plugins, middleware, nuxtServerInit
  interface NuxtAppOptions {
    $customUtils: CustomUtils
  }
  // nuxtContext.$customUtils
  interface Context {
    $customUtils: CustomUtils
  }
}

declare module 'vuex/types/index' {
  // this.$customUtils inside Vuex stores
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  interface Store<S> {
    $customUtils: CustomUtils
  }
}

export default defineNuxtPlugin((context, inject) => {
  const { $dateFns } = context

  const customUtils: CustomUtils = {
    /** manage service types
     * @function sortServiceTypes : to sorting the servicetype
     * list of priority of service sorted :
     * 1. First Mile
     * 2. Customs
     * 3. Freight Forwarder
     * 4. Last Mile
     *
     * @function setServiceType : set the service type label, ex: LAST_MILE to LAST MILE
     */
    sortServiceTypes: (data) => {
      const serviceTypes = [...data]

      if (serviceTypes.length === 0) return []

      return [
        ...serviceTypes.filter((service) => service.name === 'FIRST_MILE'),
        ...serviceTypes.filter(
          (service) =>
            service.name !== 'FIRST_MILE' && service.name !== 'LAST_MILE'
        ),
        ...serviceTypes.filter((service) => service.name === 'LAST_MILE'),
      ]
    },
    setServiceType: (data) => {
      if (data === 'SELF_COLLECT_LAST_MILE') return 'PUDO'

      return data.replaceAll('_', ' ')
    },
    setColorServiceType: (data, type = '') => {
      const isDomestic = data === 'DOMESTIC'
      const isFirstMile = data === 'FIRST_MILE'
      const isFreight = data === 'FREIGHT_FORWARDER'
      const isCustoms = data === 'CUSTOMS'
      const isPudo = data === 'SELF_COLLECT_LAST_MILE'
      const isb2b = data === 'B2B'
      const isSRIOrder = data === 'SRI'

      //  set the chip color for service type
      if (type === 'chip') {
        if (isSRIOrder) return 'purple darken-1'
        if (isFirstMile) return 'secondary darken-2'
        if (isCustoms) return 'brown darken-1'
        if (isFreight) return 'warning'
        if (isDomestic) return 'cyan darken-1'
        if (isPudo) return 'pink darken-1'
        if (isb2b) return 'success darken-2'

        return 'primary darken-1'
      }

      //  set the text color for service type
      if (isSRIOrder) return 'purple'
      if (isFirstMile) return 'secondary'
      if (isCustoms) return 'brown'
      if (isFreight) return 'warning'
      if (isDomestic) return 'cyan'
      if (isPudo) return 'pink'
      if (isb2b) return 'success'

      return 'primary'
    },
    // end

    /** manage statuses
     * @function sortStatuses : to sort the statuses
     * @function formattingStatuses : to formatting for selected components
     */
    sortStatuses: (data) => {
      const statuses = [...data]

      if (statuses.length === 0) return []

      return [
        ...statuses.filter((item) => item.ServiceType === 'FIRST_MILE'),
        ...statuses.filter(
          (item) =>
            item.ServiceType !== 'FIRST_MILE' &&
            item.ServiceType !== 'LAST_MILE'
        ),
        ...statuses.filter((item) => item.ServiceType === 'LAST_MILE'),
      ]
    },
    formattingStatuses: (data) => {
      const formattedStatus = [] as Statuses[]
      let currentType = ''

      data.forEach((item, index) => {
        const serviceType = item.ServiceType.replaceAll('_', ' ') || ''

        if (currentType !== serviceType) {
          currentType = serviceType

          if (index !== 0) {
            formattedStatus.push({ divider: true } as any)
          }

          formattedStatus.push({ header: serviceType } as any)
        }

        formattedStatus.push(data[index])
      })

      return formattedStatus
    },
    // end

    capitalize: (text) => {
      return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase() || ''
    },
    // to concatenate the addresses
    setAddress: (data) => {
      const setItem = data.filter((item: string) => item)

      return setItem.join(', ')
    },
    setRuleType: (data) => {
      return data.split('RULE_TYPE_')[1]
    },
    // set the fetch params object to the URLSearchParams and also eliminated the empty value
    setURLParams: (data, separateKeys = false) => {
      const paramsFilterOrders = new URLSearchParams()

      for (const [key, value] of Object.entries(data)) {
        if (!value) continue

        // format createdTo and createdFrom params to ISO datetime
        if (key === 'createdTo' || key === 'createdFrom') {
          const date = $dateFns.formatISO(
            $dateFns.parseISO(value as string) as Date
          ) as unknown as string

          paramsFilterOrders.append(key, date)
          continue
        }

        // format array value to separated params but same key e.g foo=bar&foo=baz
        if (separateKeys || key === 'serviceType' || key === 'clientId') {
          if (typeof value === 'object') {
            const items = value as string[]

            items.forEach((item) => {
              paramsFilterOrders.append(key, item)
            })
            continue
          }

          paramsFilterOrders.append(key, value)
          continue
        }

        paramsFilterOrders.append(key, value as string)
      }

      return paramsFilterOrders
    },
    multiDownload: async (
      urls
      // params: { rename?: Object | Function | {}; }
    ) => {
      if (!urls) {
        throw new Error('`urls` required')
      }

      for (const [index, url] of urls.entries()) {
        // const name = typeof params.rename === 'function' ? params.rename({ url, index, urls }) : '';
        const name = ''

        await delay(index * 1000)
        await download(url, name)
      }
    },
    // set the currency using the Intl.NumberFormat API
    setCurrency: ({ currency, price }) => {
      if (!currency || isNaN(price as number)) return ''

      const formattedPrice = parseFloat(String(price)) || 0

      return new Intl.NumberFormat('en-US', {
        style: 'currency',
        maximumFractionDigits: 2,
        currencyDisplay: 'code',
        currency,
      }).format(formattedPrice)
    },
    getErrorMessages: (error, defaultMessage) => {
      if (!(error?.response as AxiosResponse<ErrorAPI>)?.data)
        return (error as unknown as Error)?.message || defaultMessage

      const { data } = (error as { response: AxiosResponse<ErrorAPI> }).response
      const { error: singleError = '', ErrorDetails: listError = [] } = data
      let message = [] as string[]

      // single error
      if (singleError) {
        message = [singleError]
      }

      // multiple error
      if (listError && listError.length > 1) {
        message = listError
          .filter((item) => item?.note || item.reason)
          .map((item) => item?.note || item.reason)
      }

      return message && message.length ? message : defaultMessage
    },
    getStagingVersion: () => {
      let staging = 'dev' as 'dev' | 'sandbox' | 'prod'
      const originPath = window.location.origin

      if (originPath.includes('sandbox')) {
        staging = 'sandbox'
      }

      if (originPath.includes('v2')) {
        staging = 'prod'
      }

      return staging
    },
    // to concatenate the dimensions (Length X Width X Height)
    getDimensions: ({ length, width, height }) => {
      const computedLength = Number(length)?.toFixed(1) || 0
      const computedWidth = Number(width)?.toFixed(1) || 0
      const computedHeight = Number(height)?.toFixed(1) || 0

      return `${computedLength} (cm) X ${computedWidth} (cm) X ${computedHeight} (cm)`
    },
    formatBytes: (bytes, decimals = 2) => {
      if (bytes === 0) return '0 Bytes'

      const k = 1024
      const dm = decimals < 0 ? 0 : decimals
      const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
      const i = Math.floor(Math.log(bytes) / Math.log(k))

      return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i]
    },
    previewFileUpload: (file) => {
      // create a URL for the selected file
      const fileUrl = URL.createObjectURL(file)

      // open a new tab to preview the file
      window.open(fileUrl, '_blank')

      // release the URL object to free up memory
      URL.revokeObjectURL(fileUrl)
    },

    isFile: (data) => {
      return 'File' in window && data instanceof File
    },

    // get file data based on path
    getPathMeta: (path) => {
      if (!path) {
        Error('path not found!')
        return
      }

      // separate the path into path, file, name, ext
      const item = path.match(/(.*?\/)?(([^/]*?)(\.[^/.]+?)?)(?:[?#].*)?$/)
      if (!item || Object.keys(item)?.length === 0) {
        Error('path not match!')
        return
      }

      return {
        path: item[1],
        file: item[2],
        name: item[3],
        ext: item[4],
      }
    },

    /**
     * To formatting the 'yes' or 'no' string to boolean value
     *
     * @param data {boolean | string}
     *
     * @return {boolean}
     */
    strToBool: (data: boolean | string): boolean => {
      if (!data) return false

      return typeof data === 'boolean' ? data : data.toLowerCase() === 'yes'
    },
  }

  inject('customUtils', customUtils)
})
