import { extend } from 'vee-validate'
import * as rules from 'vee-validate/dist/rules'
import { messages } from 'vee-validate/dist/locale/en.json'

export default () => {
  for (const [rule, validation] of Object.entries(rules)) {
    extend(rule, {
      ...validation,
      message: messages[rule],
    })

    extend('url', {
      validate(value) {
        return /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([-.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(
          value
        )
      },
      message: (field) => `${field} should be a valid URL`,
    })

    extend('minMaxItems', {
      validate(value, [min, max]) {
        return value.length >= min && value.length <= max
      },
      message: (field, params) => {
        return `${field} contains minimum ${params[0]} and maximum ${params[1]}`
      },
    })

    extend('password', {
      validate(value) {
        return /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,16}$/.test(
          value
        )
      },
      message: (field) =>
        `${field} should be contains 8 - 16 characters, uppercase letter, lowercase letter, number and special character!`,
    })
  }
}
